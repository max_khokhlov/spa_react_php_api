export const GET_PEOPLE_LOAD = 'GET_PEOPLE_LOAD'
export const GET_PEOPLE_ADD_SUCCESS = 'GET_PEOPLE_ADD_SUCCESS'
export const GET_PEOPLE_SUCCESS = 'GET_PEOPLE_ADD_SUCCESS'
export const GET_PEOPLE_REMOVE_SUCCESS = 'GET_PEOPLE_REMOVE_SUCCESS'
export const GET_PEOPLE_ERROR = 'GET_PEOPLE_ERROR'