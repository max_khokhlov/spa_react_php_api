import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'

import {Icon, Button, Spin} from 'antd';

//form
import {formInfo} from '../page-element/form/templates/formInfo'
import {touristForm} from '../page-element/form/templates/touristForm'
import {addPeople} from '../page-element/form/templates/addPeople'

//hotelAction
import * as hotelAction from '../../action/hotel/hotelAction'

//addHotel
import AddHotelToOrder from '../page-element/form/addHotelToOrder'

//orderAction
import * as orderAction from '../../action/order/orderAction'

const dateFormatInfo = 'DD MMMM YYYY, dddd';


class DaysTour extends Component{

    constructor(props){
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(hotelAction, dispatch);
        this.state = {
            loading: false,
            hotelId: 0,
            hotels: {},
            values: {}
        }
    }

    declOfNum(number, titles) {
        let cases = [2, 0, 1, 1, 1, 2];
        return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
    }

    isEmpty(object) {
        return JSON.stringify(object) == "{}";
    }

    deleteHotel = (data, reserve) => {

        this.setState({loading: true});
        const {dispatch, updateList} = this.props;

        if (reserve){
            let action = hotelAction.deleteReserveHotelforDay(data);
            dispatch(action);
        }else{
            let action = hotelAction.deleteHotelforDay(data);
            dispatch(action);
        }

        setTimeout(() => {
            updateList();
            this.setState({loading: false});
        }, 2000);

    }

    returnServices = (data, rooms) => {

        if (typeof data !== 'undefined'){

            return data.map(item => {
                return <div className="service-item">
                    <div className="name">
                        {rooms[item.service_id].name}
                    </div>
                    <div className="comment">
                        {item.comment}
                    </div>
                </div>
            })

        }else{

            return <div className="service-item">
                Нет доп. услуг к резерву
            </div>

        }

    }

    returnHotels = (data, city) => {

        const {hotel} = this.props.tour_info;

        if (typeof data !== 'undefined' && data.length > 0){

            return data.map(item => {

                let hotelItem = hotel[city][item.hotel_id]
                let roomItem = hotelItem.rooms[item.room_id]

                let dateStartReserve = moment(item.date_reserve_start);
                let dateFinishReserve = moment(item.date_reserve_finish);
                moment.lang('ru');

                return <div className="hotel">
                        <div className="name">
                            <span><b>Отель:</b> {hotelItem.name} | {hotelItem.stars} </span>
                            { item.reserve? <div className="status">Резерв</div> : '' }
                            <button onClick={() => {this.deleteHotel(item, item.reserve)}} className="delete"><Icon type="close-circle-o" /></button>
                        </div>
                        <div className="room">
                            <div className="name"><b>Номер:</b><span>{roomItem.name}</span></div>
                            <div className="dates">
                                <b>Даты брони номера:</b>
                                <div className="date">
                                    {dateStartReserve.format(dateFormatInfo)} - {dateFinishReserve.format(dateFormatInfo)}
                                </div>
                            </div>
                            <div className="description">
                                <b>Описание:</b>
                                <div className="text">
                                    {roomItem.type.description}
                                </div>
                            </div>

                            <div className="capacity">
                                <b>Вместимость:</b>
                                <span>{roomItem.type.capacity} {this.declOfNum(roomItem.type.capacity, ['человек', 'человека', 'человек'])}</span>
                            </div>
                            <div className="peoples">
                                <b>Гости:</b>
                                <div className="people-block">
                                    {this.returnPeople(item.peoples)}
                                </div>
                            </div>
                            <div className="services-list">
                                <b>Дополнительные услуги к номеру:</b>
                                {this.returnServices(item.services, roomItem.services)}
                            </div>
                        </div>
                </div>


            })

        }else{

            return <div>Нет отелей</div>

        }

    }

    returnPeople = (data) => {

        const {people} = this.props;

        // console.log(data);

        if (typeof data !== 'undefined'){
            return data.map(item => {

                let name, phone;

                if (typeof people[item] !== 'undefined'){
                    name = people[item].name;
                    phone = people[item].phone;
                }else{
                    name = 'УДАЛЕННЫЙ УЧАСТНИК ТУРА';
                    phone = 'Нет номера';
                }

                return <div className="people-item">
                    <div className="name">
                        {name} <b> {phone} </b>
                    </div>
                </div>
            })

        }else{
            return <p>Нет людей</p>
        }

    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }

    updateList = () => {

        const {updateList} = this.props;
        updateList();

    }

    deleteDay = (day_id) => {

        const {dispatch, order_id} = this.props;
        bindActionCreators(orderAction, dispatch);

        let data = {
            'order_day_id': day_id,
            'orderId': order_id
        }

        let action = orderAction.deleteDay(data);
        dispatch(action);

    }

    render () {

        const {loading} = this.state;

        if (loading){

          return <Spin />

        }else{

            const {days, tour_info, people} = this.props;

            if (!this.isEmpty(days)){

                return(days.map((day, i) => {
                        return <div className="col-50">
                            <div className="day-info">
                                <div className="title">Период <b>{++i}</b><div className="btn"><Button type="danger" ghost onClick={() => this.deleteDay(day.order_day_id) }>Удалить период</Button></div></div>
                                <div className="date-start">
                                    <b>Дата въезда:</b>
                                    <div className="date">
                                        {this.formatDateInfo(day.date_start)}
                                    </div>
                                </div>
                                <div className="date-finish">
                                    <b>Дата выезда:</b>
                                    <div className="date">
                                        {this.formatDateInfo(day.date_finish)}
                                    </div>
                                </div>
                                <div className="city-block">
                                    <b>Город:</b>
                                    <div className="city">
                                        {tour_info.city[day.city_id].name}
                                    </div>
                                </div>
                                <div className="hotel-block">
                                    {this.returnHotels(day.hotels, day.city_id)}
                                </div>
                                <AddHotelToOrder people={people} tour_info={tour_info} city_id={day.city_id} day_id={day.order_day_id} day_start={day.date_start} day_finish={day.date_finish} updateList={this.updateList}/>
                            </div>
                        </div>
                    })

                )

            }else{

                return <div>Нет заполненых дней в туре</div>

            }

        }

    }
}


DaysTour.PropTypes = {
    days: PropTypes.array.isRequired,
    people: PropTypes.array.isRequired,
    tour_info: PropTypes.array.isRequired,
    updateList: PropTypes.func.isRequired
}

export default connect()(DaysTour)