import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import moment from 'moment'

import { Layout, Form, Spin, Icon, Input, Button, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Radio, notification } from 'antd';


//regular
import * as r from '../../constants/Regular'

//action
import * as orderAction from '../../action/order/orderAction'
import * as orderInfoAction from '../../action/order/orderInfoAction'

//component
import GetPeople from '../people/getPeople'

const { Header, Content } = Layout;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;


let now = moment();
let formated_date = now.format("YYYY-MM-DD");


const dateFormat = 'DD.MM.YYYY';

const reserveTransportDefault = {
    "order_name": "",
    "tour_id": "",
    "date_start": formated_date,
}


notification.config({
    placement: "topLeft",
});


class NormalAddOrder extends Component{

    constructor(props){
        super(props);
        const {dispatch} = this.props;

        bindActionCreators(orderAction, dispatch)
        bindActionCreators(orderInfoAction, dispatch)

        this.state = {
            modalVisible: false,
            submit: false,
            loadingBtn: false,
            mode: 'tour',
            values: {
                date_start: formated_date,
                manager_id: this.props.manager.request.result.manager_id,
                group: this.props.manager.request.result.group
            }
        }
    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].number_place == value) return i;
        }

        return -1;
    }

    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

    }

    onChangeDatePicker (e, name) {
        let returnDate = moment(e);

        let stateValues = this.state.values;
        stateValues[name] = returnDate.format('YYYY-MM-DD');
        this.setState({values:  stateValues});
    }

    onChangeSelect = (e, name) => {

        console.log(e, name);

        let stateValues = this.state.values;

        stateValues[name] = e;

        this.setState({values: stateValues});

    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }


    handleSubmit = () => {

        const {values, mode} = this.state;
        const {dispatch} = this.props;
        let error = false;
        
        this.setState({loadingBtn:true});

        if (mode === "hotel" && typeof values.city === 'undefined'){
            error = true;
            notification['error']({
                message: 'Выберите город!'
            });
        }

        if (typeof values.name === 'undefined'){
            error = true;
            notification['error']({
                message: 'Введите имя заказа!'
            });
        }

        if (mode === "tour" && typeof values.tour === 'undefined'){
            error = true;
            notification['error']({
                message: 'Выберите тур или Конструктор тура!'
            });
        }
        
        if (typeof values.user !== 'undefined'){

            if (typeof values.user.bitrix_id === 'undefined'){

                if (typeof values.user.firstname === 'undefined'){

                    error = true;
                    notification['error']({
                        message: 'Введите имя новому пользователю'
                    });

                }

                if (typeof values.user.firstname === 'undefined'){

                    error = true;
                    notification['error']({
                        message: 'Введите фамилию новому пользователю'
                    });

                }

                if (typeof values.user.firstname === 'undefined'){

                    error = true;
                    notification['error']({
                        message: 'Введите фамилию новому пользователю'
                    });

                }

                if (!r.VALID_EMAIL.test(values.user.email)){
                    error = true;
                    notification['error']({
                        message: 'Некорректный Email нового пользователя'
                    });

                }

                if (!r.VALID_PHONE.test(values.user.phone)){
                    error = true;
                    notification['error']({
                        message: 'Некорректный телефон нового пользователя'
                    });
                }
                
            }
            
        }else{
            
            error = true;
            notification['error']({
              message: 'Необходимо заполнить данные пользователя'
            });
            
        }

        let stateValues = this.state.values;
        stateValues['mode'] = mode;
        
        if (!error){
            
            //Добавление заказа
            
            let action = orderAction.addOrder(stateValues);
            
            dispatch(action);

            //Обновление информации о заказах
            
            let data = {
                manager_id: this.props.manager.request.result.manager_id,
                group: this.props.manager.request.result.group
            }

            action = orderInfoAction.getInfo(data);
            
            dispatch(action);
            
            setTimeout(()=>{
                this.setState({modalVisible : false});
                this.setState({loadingBtn: false});
            },500);
        }else{
            this.setState({loadingBtn: false});
        }

    }

    showDrawer = () => {
        this.setState({
            modalVisible: true,
        });
    }

    onClose = () => {
        this.setState({
            modalVisible: false,
        });
    }

    returnOption = (name) => {

        const {info} = this.props;
        let result;
        let returnOption = [];

        switch (name){
            case 'tour':
                result = info.request.tours;
                break;
            case 'city':
                result = info.request.city;
                break;

        }

        for (let key in result){
            returnOption.push({
                value: key,
                label: result[key].name
            });
        }

        return returnOption.map(option => {
            return <Option value={option.value}>{option.label}</Option>
        });

    }

    returnFormTourAdd = () => {

        return <div className="form-block">
            <div className="tours">
                <FormItem label="Выберите тур:">
                    <Select allowClear={true} className="tour" key="tour" onChange={(e) => { this.onChangeSelect(e, 'tour') }} placeholder="Выберите тур">
                        <Option value={0}>Конструктор тура</Option>
                        {this.returnOption('tour')}
                    </Select>
                </FormItem>
            </div>
            <div className="name">
                <FormItem label="Имя заказа:">
                    <Input className="name" type="text" key="name" id="name" onChange={this.onChange} placeholder="Ввведите имя заказа"></Input>
                </FormItem>
            </div>
            <div className="date-start">
                <FormItem label="Дата начала тура (отправление) :">
                    <DatePicker defaultValue={moment(now, dateFormat)} format={dateFormat} onChange={ (e) => {this.onChangeDatePicker(e, 'date_start')} }/>
                </FormItem>
            </div>
        </div>

    }

    returnFormHotelAdd = () => {

        return <div className="form-block">
            <div className="city">
                <FormItem label="Выберите город проживания:">
                    <Select allowClear={true} className="city"  key="city" onChange={(e) => { this.onChangeSelect(e, 'city') }} placeholder="Выберите город проживания">
                        {this.returnOption('city')}
                    </Select>
                </FormItem>
            </div>
            <div className="name">
                <FormItem label="Имя заказа:">
                    <Input className="name" type="text" key="name" id="name" onChange={this.onChange} placeholder="Ввведите имя заказа"></Input>
                </FormItem>
            </div>
            <div className="date-start">
                <FormItem label="Дата начала брони:">
                    <DatePicker defaultValue={moment(now, dateFormat)} format={dateFormat} onChange={ (e) => {this.onChangeDatePicker(e, 'date_start')} }/>
                </FormItem>
                <FormItem label="Дата окончания брони:">
                    <DatePicker defaultValue={moment(now, dateFormat)} format={dateFormat} onChange={ (e) => {this.onChangeDatePicker(e, 'date_finish')} }/>
                </FormItem>
            </div>
        </div>


    }

    handleModeChange = (e) => {
        const mode = e.target.value;
        const date_start = formated_date;
        const {manager_id} = this.props.manager.request.result;
        const {group} = this.props.manager.request.result;
        this.setState({ mode });
        this.setState({values: {date_start, manager_id, group}});
    }
    
    getPeople = (values) => {
        let stateValues = this.state.values;

        stateValues['user'] = values;

        this.setState({values: stateValues});
    }

    render () {

        const {loading, request} = this.props.info;
        const {mode,loadingBtn} = this.state;

        if (loading || typeof request.tours === 'undefined'){

            if (loading){

                return <div className="loading">
                    <Spin/>
                </div>

            }

            return null;

        }else{

            return <div>
                <div className="add-block">
                    <Button type="primary" icon="plus-square" onClick={ () => {
                        this.showDrawer();
                    } }>Добавить заказ</Button>
                </div>
                <Drawer
                    width={1280}
                    placement="right"
                    closable={false}
                    onClose={this.onClose}
                    visible={this.state.modalVisible}
                >
                    <Form className="form-add-order">

                        <h2>Добавить заказ</h2>

                        <div className="form">
                            <Radio.Group onChange={this.handleModeChange} value={mode} style={{ marginBottom: 8 }}>
                                <Radio.Button value="tour">Добавить заказ тура</Radio.Button>
                                <Radio.Button value="hotel">Добавить заказ отеля</Radio.Button>
                            </Radio.Group>

                            <Form onSubmit={this.handleSubmit}>
                                {mode === 'tour' ? this.returnFormTourAdd() : this.returnFormHotelAdd()}
                            </Form>
                        </div>
                        
                        <GetPeople getPeople={this.getPeople} />

                        <div className="buttons">
                            <Button type="primary" loading={loadingBtn} icon="save" onClick={() => {this.handleSubmit()}} className="save-btn">Добавить заказ</Button>
                        </div>
                    </Form>
                </Drawer>
            </div>

        }

    }

}

function mapStateToProps(state){
    return {
        manager: state.login,
    }
}


const AddOrder = reduxForm({
    form: 'addOrder'
})(NormalAddOrder);


export default connect(mapStateToProps)(AddOrder);