import React, {Component} from "react";
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import InputMask from 'react-input-mask'

import { Form, Icon, Input, Button, Checkbox, Collapse, message, DatePicker, Select, Modal } from 'antd';

import * as r from '../../constants/Regular';

//form
import {formInfo} from '../page-element/form/templates/formInfo'
import {touristForm} from '../page-element/form/templates/touristForm'
import {addPeople} from '../page-element/form/templates/addPeople'

//select
import * as s from '../page-element/form/templates/Select'


const confirm = Modal.confirm;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;
const Panel = Collapse.Panel;

const dateFormat = 'DD.MM.YYYY';
const dateFormatInfo = 'DD MMMM YYYY, dddd';


class PeopleForm extends Component{

    constructor(props){
        super(props);

        this.state = {
            values: {}
        }
    }

    onChange = (e, name ,people_id) => {

        let stateValues = this.state.values;

        if(typeof stateValues[people_id] !== 'undefined'){
            stateValues[people_id] = {
                ...stateValues[people_id],
                [name]: e
            };
        }else{
            stateValues[people_id] = {
                [name]: e
            };
        }

        this.setState({values: stateValues});

    }

    onChangeDatePicker = (e, name, people_id) => {

        let stateValues = this.state.values;

        if(typeof stateValues[people_id] !== 'undefined'){
            stateValues[people_id] = {
                ...stateValues[people_id],
                [name]: e.format('YYYY-MM-DD')
            };
        }else{
            stateValues[people_id] = {
                [name]: e.format('YYYY-MM-DD')
            };
        }

        this.setState({values:  stateValues});
    }

    onChangeSelect = (e, name, people_id) => {

        let stateValues = this.state.values;

        if(typeof stateValues[people_id] !== 'undefined'){
            stateValues[people_id] = {
                ...stateValues[people_id],
                [name]: e
            };
        }else{
            stateValues[people_id] = {
                [name]: e
            };
        }

        this.setState({values:  stateValues});

    }

    isEmpty(object) {
        return JSON.stringify(object) == "{}";
    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }

    showDeleteConfirm = (props, people_order_id) => {
        confirm({
            title: 'Удалить туриста из этого заказа?',
            content: 'Человек не удалится из базы, только из заказа',
            okText: 'Да',
            okType: 'danger',
            cancelText: 'Нет',
            onOk(){

                const {removePeopleToOrder,updateList} = props;

                removePeopleToOrder(people_order_id);

                setTimeout(function () {
                    updateList();
                },1000);

            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    inputReturn = (formData, label, people_order_id) => {

        let returnInput = [];

        for (var key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "className": key,
                                "className": "ant-input",
                                "id": key,
                                "type": "text",
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "className": key,
                                "id": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "className": key,
                                "id": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "className": key,
                                "id": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {
                if (typeof input.Input !== 'undefined'){
                    if (input.Input.id !== 'phone'){
                        return <FormItem {...input.FormItem}>
                            <Input {...input.Input} onChange={(e) => this.onChange(e.target.value, input.Input.id ,people_order_id)}></Input>
                        </FormItem>
                    }else{
                        return <FormItem {...input.FormItem}>
                            <InputMask {...input.Input} mask="+7 (999) 999-99-99" maskChar=" " onChange={(e) => this.onChange(e.target.value, input.Input.id ,people_order_id)}></InputMask>
                        </FormItem>;
                    }

                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={ (e) => {this.onChangeDatePicker(e, input.DatePicker.id ,people_order_id)} }/></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem}><TextArea  {...input.TextArea}  onChange={ (e) => this.onChange(e.target.value, input.TextArea.id ,people_order_id)} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem}><Select defaultValue={input.defaultValue}  {...input.Select} onChange={ (e) => this.onChangeSelect(e, input.Select.id, people_order_id) }  >{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    returnOption = (name, id) => {

        const {people_group} = this.props;

        let result = {};

        switch(name){
            case "citizenship":
                result = s.citizenship
                break;
            case "sex":
                result = s.sex
                break;
            case "people_group_id":
                result = people_group
                break;
        }

         return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
         })

    }

    render() {

        const {peoples, updateForm} = this.props;
        const {values} = this.state;

        if (Object.keys(values).length > 0){

            updateForm(values);

        }

        if (!this.isEmpty(peoples)){

            let arrPeople = [];

            for (let key in peoples){
                arrPeople.push(peoples[key]);
            }

            return (arrPeople.map((people, index) => {
                return <div className="col-50">
                    <div className="title">
                        <span className="tourist-title">Турист {++index} </span>
                        <div className="buttons">
                            <Button onClick={() => {this.showDeleteConfirm(this.props, people.people_order_id)}} type="danger" ghost>Удалить</Button>
                        </div>
                    </div>
                    <br/>
                    {this.inputReturn(people, touristForm, people.people_order_id)}
                </div>
            })
        );

        }else{

            return <div>Нет людей в туре</div>

        }

    }

}

PeopleForm.PropTypes = {
    updateForm: PropTypes.func.isRequired,
    updateList: PropTypes.func.isRequired,
    removePeopleToOrder: PropTypes.func.isRequired,
    peoples: PropTypes.array.isRequired,
    people_group: PropTypes.array.isRequired
}

export default connect()(PeopleForm);