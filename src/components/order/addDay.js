import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import { extendMoment } from 'moment-range';
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet

import { Form, Icon, Input, Button, Checkbox, Collapse, Tabs, message, DatePicker, Select, Drawer, notification, Modal } from 'antd';

//orderAction
import * as orderAction from '../../action/order/orderAction'

const confirm = Modal.confirm;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;
const Panel = Collapse.Panel;

const dateFormat = 'DD.MM.YYYY';
const dateFormatInfo = 'DD MMMM YYYY, dddd';

let now = moment();
let formated_date = now.format("YYYY-MM-DD");

const locale = {
    blank: 'Дата не выбрана',
    headerFormat: 'dddd, D MMM',
    locale: require('date-fns/locale/ru'), // You need to pass in the date-fns locale for the language you want (unless it's EN)
    todayLabel: {
        long: "Сег.",
        short: 'Сег.',
    },
    weekdays: ['Пон', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    weekStartsOn: 1, // Start the week on Monday
};

notification.config({
    placement: "topLeft",
});


class AddDayForm extends Component{

    constructor(props){
        super(props);
        let {last_day} = this.props;

        let numberArr = [0,0];

        if (last_day === false){

            last_day = {
                number: 0,
                date_start: formated_date.toString(),
                date_finish: formated_date.toString()
            }

        }else{
            numberArr = last_day.number_day.split("-");
        }


        const {dispatch} = this.props;
        bindActionCreators(orderAction, dispatch);

        this.state = {
            modalVisible: false,
            submit: false,
            loading: false,
            values: {
                order_id: this.props.order_id,
                date_start: last_day.date_finish,
                date_finish: last_day.date_finish,
                last_number: numberArr[1],
                city: false
            }
        }

    }

    handleSubmit = () => {

        const {values} = this.state;
        const {dispatch} = this.props;
        let stateValues = this.state.values;
        let error = false;

        let dateA = moment(values.date_start);
        let dateB = moment(values.date_finish);

        let diff = dateB.diff(dateA, 'day');

        stateValues['diff'] = diff+1;

        this.setState({values: stateValues});

        if (!values.city){
            error = true;
            notification['error']({
                message: 'Выберите город!'
            });

        }

        if (!error){
            let action = orderAction.addDay(values);
            dispatch(action);
        }

    }

    returnOption = (name) => {
        const {city} = this.props;

        let result = {};


        switch(name){
            case "city":
                result = city
                break;
        }

         if (name === 'city'){

            let returnOption = [];

            for (let key in result){
                returnOption.push({
                    "value": key,
                    "name": result[key].name
                })
            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.name}</Option>
            })

        }else{

            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            });

        }

    }

    onChangeSelect(e, name) {

        let stateValues = this.state.values;

        if (name === 'date_start' || name === 'date_finish'){

            let stateValues = this.state.values;
            let date_start = moment(new Date(e));

            stateValues[name] = date_start.format('YYYY-MM-DD');
            this.setState({values: stateValues});

        }else{
            stateValues[name] = e;
            this.setState({values: stateValues});
        }

    }


    DateFormat(date){

        console.log('date', date);

        let dateArr = date.split('-');

        dateArr[1] = dateArr[1] - 1;

        let dateOut = new Date(dateArr[0], dateArr[1], dateArr[2]);

        return dateOut;

    }

    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
    }

    returnFormAddDay = () => {

        const {values} = this.state;
        let {last_day} = this.props;

        if (last_day === false){
            last_day = {
                number: 0,
                date_start: formated_date.toString(),
                date_finish: formated_date.toString()
            }
        }

        return <div className="form">
            <div className="date">
                <div className="date-start col-50">
                    <label>
                        Дата въезда:
                    </label>
                    <InfiniteCalendar
                        locale={locale}
                        height={300}
                        selected={this.DateFormat(values.date_start)}
                        minDate={this.DateFormat(last_day.date_finish)}
                        // maxDate={this.DateFormat(day_finish)}
                        displayOptions={{
                            showHeader: false
                        }}
                        onSelect={(date) => {this.onChangeSelect(date, 'date_start'); return true;}}
                    />
                </div>
                <div className="date-finish col-50">
                    <label>
                        Дата выезда:
                    </label>
                    <InfiniteCalendar
                        locale={locale}
                        height={300}
                        selected={this.DateFormat(values.date_finish)}
                        minDate={this.DateFormat(last_day.date_finish)}
                        // maxDate={this.DateFormat(day_finish)}
                        displayOptions={{
                            showHeader: false
                        }}
                        onSelect={(date) => {this.onChangeSelect(date, 'date_finish'); return true;}}
                    />
                </div>
            </div>
            <div className="city">
                <FormItem label="Выберите город:" className="transfer-form-item">
                    <Select defaultValue={-1} allowClear={true} showSearch={true} onChange={(e) => { this.onChangeSelect(e, "city") }} >
                        <Option value={-1} disabled>Выберите город отправления</Option>
                        {this.returnOption('city')}
                    </Select>
                </FormItem>
            </div>
        </div>

    }

    render () {

        const {loading} = this.state;

        return <div className="add-day-block">
            <div className="buttons-row">
                <Button type="primary" icon="plus-circle-o" className="add-button" onClick={() => this.setModalVisible(true)}>Добавить период</Button>
            </div>
            <Modal
                width="1024px"
                title="Добавить период к туру"
                wrapClassName="vertical-center-modal"
                visible={this.state.modalVisible}
                onOk={() => this.handleSubmit()}
                onCancel={() => this.setModalVisible(false)}
                footer={[
                    <Button key="back" onClick={() => this.setModalVisible(false)}>Отмена</Button>,
                    <Button key="submit" type="primary" loading={loading} onClick={() => this.handleSubmit()}>
                        Добавить
                    </Button>,
                ]}
            >
                <div className="form-add-people">
                    <Form onSubmit={this.handleSubmit}>
                        {this.returnFormAddDay()}
                    </Form>
                </div>
            </Modal>
        </div>

    }

}

const AddDay = reduxForm({
    form: 'AddDay'
})(AddDayForm);

export default connect()(AddDay);