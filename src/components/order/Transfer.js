import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'

import {Icon, Button, Spin} from 'antd';

//form
import {formInfo} from '../page-element/form/templates/formInfo'
import {touristForm} from '../page-element/form/templates/touristForm'
import {addPeople} from '../page-element/form/templates/addPeople'

//transferAction
import * as transferAction from '../../action/transfer/transferAction'

//addHotel

const dateFormatInfo = 'DD MMMM YYYY, dddd';

class Transfer extends Component{

    constructor(props){
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(transferAction, dispatch);

        this.state = {
            loading: false
        }
    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }

    render () {

        const {loading} = this.state;
        const {transfer, people, tour_info} = this.props;

        if (loading){
            return <div className="loading-block">
                <Spin size="large" />
            </div>
        }else{
            if (transfer && transfer.length > 0){
                return <div className="transfer-block">
                    { transfer.map(item => {
                        return <div className="transfer-item">
                            <div className="delete">
                                <Button
                                    type="danger"
                                    onClick={()=>{

                                        this.setState({loading: true});
                                        const {dispatch, updateList} = this.props;

                                        let data = {
                                            'reserve_transport_people_id': item.reserve_transport_people_id
                                        }

                                        let action = transferAction.deleteReserveTransfer(data);
                                        dispatch(action);

                                        setTimeout(()=>{
                                            this.setState({loading: true});
                                            updateList();
                                        }, 1000)

                                    }}>Удалить</Button>
                            </div>
                            <div className="status">
                                {item.reserve? <div className="reserve">Резерв</div>: ''}
                            </div>
                            <div className="people">
                                {people[item.people_order_id].name}
                            </div>
                            <div className="city">
                                <div className="start">
                                    <b>Город отправления:</b> {tour_info.city[item.city_start].name}
                                </div>
                                <div className="finish">
                                    <b>Город прибытия:</b> {tour_info.city[item.city_finish].name}
                                </div>
                            </div>
                            <div className="transport-name">
                                {item.reserve? item.transport_name : item.transport_type_name}
                            </div>
                            <div className="transport-type">
                                <b>Тип транспорта:</b> <div className="type">{item.transport_type_name}</div>
                            </div>
                            <div className="place">
                                <div className="number">
                                    <b>Место №</b> <span>{item.number_place}</span> <small>{item.place_name}</small>
                                </div>
                            </div>
                            <div className="date-reserve">
                                <b>Дата:</b> {this.formatDateInfo(item.date_reserve)}
                            </div>
                            <div className="price">
                                <b>Цена:</b> {item.price} руб.
                            </div>
                            <div className="comment">
                                <b>Комментарий:</b>
                                <div className="text">
                                    {item.comment}
                                </div>
                            </div>
                        </div>

                    })}
                </div>
            }else{
                return <div className="transfer-block">Нет информации о трансфере</div>;
            }
        }



    }

}

Transfer.PropTypes = {
    order_id: PropTypes.string.isRequired,
    people: PropTypes.array.isRequired,
    updateList: PropTypes.func.isRequired,
    tour_info: PropTypes.array.isRequired,
    transfer: PropTypes.array.isRequired
}

export default connect()(Transfer);