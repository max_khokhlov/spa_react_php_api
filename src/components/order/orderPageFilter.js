import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import {moment} from "moment"
import { Layout, Form, Spin, Icon, Input, Button, Switch, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Radio, notification } from 'antd';

//action
import * as managerAction from '../../action/manager/managerAction'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const statusObj = {
    "new": {
        name: "Новый"
    },
    "register": {
        name: " Зарегистрирован"
    },
    "canceled": {
        name: " Отменен"
    },
    "payed": {
        name: " Оплачен"
    },
    "finish": {
        name: " Завершен"
    }
}


class PageFilter extends Component{
    
    constructor(props){
        super(props);
        const {dispatch} = this.props
        
        bindActionCreators(managerAction, dispatch);
        let action = managerAction.getManagerList();
        dispatch(action);
        
        this.state = {
            visible: false,
            hotel: true,
            tour: true,
            values: {}
        }
        
    }

    visibleFilter = () => {
        
        this.setState({
            visible: !this.state.visible
        })
    }

    returnOption = (name) => {

        const {info, managerList} = this.props;
        let result;
        let returnOption = [];
        let manager_list = {};
        
        if (!managerList.loading || typeof managerList.request !== 'undefined'){
            manager_list = managerList.request
        }
        
        switch (name){
            case 'tour_bitrix_id':
                result = info.request.tours;
                break;
            case 'status':
                result = statusObj;
                break;
            case 'manager':
                result = manager_list;
                break;

        }

        if (name !== 'manager'){
            for (let key in result){
                returnOption.push({
                    value: key,
                    label: result[key].name
                });
            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            });
        }else{

            if (result.length > 0){
                return result.map(item => {

                    let usersArr = [];

                    for (let key in item.users){

                        usersArr.push({
                            value: key,
                            title: item.users[key]
                        })

                    }

                    return <OptGroup label={item.name_group}>

                        {usersArr.map(val => {
                            return <Option value={val.value}>{val.title.name}</Option>
                        })}
                    </OptGroup>

                });
            }else{
                return false;
            }
            
        }

    }

    onChangeDatePicker (e, name) {
        
        if (e.length <= 0){
            return false;
        }
        
        let stateValues = this.state.values;
        stateValues[name] = {
            date_start: e[0].format('YYYY-MM-DD'),
            date_finish: e[1].format('YYYY-MM-DD')
        }
        
        this.setState({values:  stateValues});
    }

    onChangeSelect(e, name){
        
        let stateValues = this.state.values;
        
        if (typeof e !== 'undefined'){
            stateValues[name] = e;
        }else{
            delete stateValues[name];
        }
        
        this.setState({values:  stateValues});

    }

    updateListToFilter = () => {

        const {tour, hotel} = this.state;
        const {updateListToFilter} = this.props;
        let stateValues = this.state.values
        stateValues['is_tour'] = [];
        
        if (tour){
            stateValues['is_tour'].push(1)
        }else{
            delete stateValues['tour_bitrix_id']
        }
        
        if (hotel){
            stateValues['is_tour'].push(0)
        }

        updateListToFilter(stateValues);

    }
    
    render (){
        
        const {info,manager} = this.props;
        const {visible, hotel, tour} = this.state;
        
        if (info.loading || typeof info.request === 'undefined'){
            return null;
        }
        
        console.log(manager);
        
        return <div className="filter-block">
            <div className="btn-block">
                <Button type="primary" icon="filter" onClick={() => {this.visibleFilter()}} className="save-btn">{visible? "Скрыть фильтр" : "Показать фильтр"}</Button>
            </div>
            <div className="filters" style={{display: visible ? "block" : "none" }}>
                <div className="filter-item">
                    <label htmlFor="hotel">Отели:</label>
                    <Switch id="hotel" checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="close" />} checked={hotel} onChange={ () => { this.setState({ hotel: !hotel }) } } />
                </div>
                <div className="filter-item">
                    <label htmlFor="tour">Туры:</label>
                    <Switch id="tour" checkedChildren={<Icon type="check" />} unCheckedChildren={<Icon type="close" />} checked={tour} onChange={ () => { this.setState({ tour: !tour }) } } />
                </div>
                {tour?
                    <div className="filter-item">
                        <label>Тип тура:</label>
                        <Select
                            allowClear
                            style={{ width: 300 }}
                            placeholder="Выберите тур"
                            onChange={(value) => { this.onChangeSelect(value, "tour_bitrix_id") }}
                            
                        >
                            <Option value={0}>Конструктор тура</Option>
                            {this.returnOption('tour_bitrix_id')}
                        </Select>
                    </div> : ""
                }
                <div className="filter-item">
                    <label>Статус заказа:</label>
                    <Select
                        allowClear
                        style={{ width: 300 }}
                        placeholder="Статус заказа"
                        onChange={(value) => { this.onChangeSelect(value, "status") }}

                    >
                        {this.returnOption('status')}
                        {manager.indexOf("1") !== -1 ? <Option value="delete">Удален</Option> : "" }
                    </Select>
                </div>
                <div className="filter-item">
                    <label>Даты заказов:</label>
                    <RangePicker format="DD.MM.YYYY" onChange={(e) => {this.onChangeDatePicker(e,"duration")}}/>
                </div>
                {manager.indexOf("1") !== -1 ?
                    <div className="filter-item">
                        <label>Менеджер заказа:</label>
                        <Select
                            allowClear
                            style={{ width: 300 }}
                            placeholder="Менеджер в заказе"
                            onChange={(value) => { this.onChangeSelect(value, "manager") }}

                        >
                            {this.returnOption('manager')}
                        </Select>
                    </div>
                :""}
                <div className="btn-block">
                    <Button type="primary" icon="filter" onClick={() => {this.updateListToFilter()}} className="save-btn">Отфильтровать</Button>
                </div>
            </div>
        </div>
    }
    
}

function mapStateToProps(state){
    return {
        managerList: state.manager
    }
}

PageFilter.PropTypes = {
    manager: PropTypes.object.isRequired,
    info: PropTypes.object.isRequired,
    updateListToFilter: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(PageFilter)