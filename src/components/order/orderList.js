import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import moment from 'moment'
import { NavLink  } from 'react-router-dom'
import { List, Icon, Button, Spin, message } from 'antd'

//action
import * as orderAction from '../../action/order/orderAction'
import * as orderInfoAction from '../../action/order/orderInfoAction'

const dateFormat = 'DD MMMM YYYY (dd)';

class OrderList extends Component{

    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        
        bindActionCreators(orderAction, dispatch);
        bindActionCreators(orderInfoAction, dispatch);
    }

    ucFirst(str) {
        if (!str) return str;
        return str[0].toUpperCase() + str.slice(1);
    }

    formatName(name){
        return this.ucFirst(name.toLowerCase())
    }

    formatStatus(status){
        switch(status) {
            case 'new':
                return <div className="status new">Новый</div>
                break;
            case 'register':
                return <div className="status register">Зарегистрирован</div>
                break;
            case 'canceled':
                return <div className="status canceled">Отменен</div>
                break;
            case 'payed':
                return <div className="status payed">Оплачен</div>
                break;
            case 'finish':
                return <div className="status finish">Завершен</div>
                break;
            default:
                return <div className="status no-status">Нет статуса</div>
        }
    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }


    deleteOrder = (id) => {

        //Удаление заказа
        
        const {dispatch} = this.props;

        let data = {
            orderId: id,
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group,
        };

        let action = orderAction.deleteOrder(data);
        dispatch(action);


        //Обновление информации о заказах

        data = {
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group
        }

        action = orderInfoAction.getInfo(data);

        dispatch(action);
        
    }

    render() {

        const {loading, request} = this.props.order;

        let result = [];

        if (loading){
            return message.loading('Получаю заказы...', 2000);
        }else{
            setTimeout(message.destroy(), 2000);
            result = request.result
        }

        return <List
             className="order-list"
             itemLayout="horizontal"
             dataSource={result}
             renderItem={item => (

                <List.Item actions={[<NavLink  to={"/manager/order/"+item.order_id} ><Button type="primary" ghost>Изменить</Button></NavLink>,<Button onClick={ () => { this.deleteOrder(item.order_id) } } type="danger" ghost>Удалить</Button>]}>
                    <List.Item.Meta title={'  #'+ item.order_id + '  ' + this.formatName(item.name)} />
                       <div>
                           <div className="total-price">{item.total} руб.</div>
                           {this.formatStatus(item.status)}
                           <div className="date">
                               <div className="date-finish"> <b>Старт:</b> <Icon type="calendar" />{this.formatDate(item.date_start)}</div>
                               <div className="date-start"> <b>Финиш:</b> <Icon type="calendar" />{this.formatDate(item.date_finish)}</div>
                           </div>
                           <div className={item.is_tour === "1"? "type-order tour" : "type-order hotel"}>
                               {item.is_tour === "1"? "Тур" : "Отель"}
                           </div>
                           {item.tour_bitrix_id === "0"? <div className="flag-block">Конструктор</div> : ""}
                       </div>
                     </List.Item>
             )}
         />

    }

}

export default connect()(OrderList)