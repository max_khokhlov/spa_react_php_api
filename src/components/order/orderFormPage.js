import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'

import { Layout, Breadcrumb, Icon, Button, Spin, message } from 'antd'
const { Header, Content } = Layout;

import '../../sass/main.scss';

import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'

import * as orderAction from '../../action/order/orderAction'
import * as peopleAction from '../../action/people/peopleAction'

import FormOrder from '../page-element/form/FormOrder'

class OrderFormPage extends Component{

    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(orderAction, dispatch)


        let orderId = this.props.match.params.orderId;

        let data = {
            'orderId': orderId
        }

        let action = orderAction.getOrderData(data);
        dispatch(action);

    }

    update = () => {

        let orderId = this.props.match.params.orderId;
        const {dispatch} = this.props;

        let data = {
            'orderId': orderId
        }

        let action = orderAction.getOrderData(data)
        dispatch(action);

    }

    removePeopleToOrder = (people_id) => {

        let orderId = this.props.match.params.orderId;
        const {dispatch} = this.props;
        bindActionCreators(peopleAction, dispatch)

        let data = {
            'orderId': orderId,
            'peopleId': people_id
        }

        let action = peopleAction.removePeopleToOrder(data)
        dispatch(action);

    }


    submit = (values) => {
        console.log(values);
    }

    render() {

        let orderId = this.props.match.params.orderId;

        return(
            <div>
                <Layout>
                    <Header className="header" style={{backgroundColor: '#fff', padding: '0 25px'}}>
                        <div className="logo">
                            <img src="/manager/img/logo.png" alt="Logo"/>
                            <span>Раздел Менеджера</span>
                        </div>
                        <div className="menu-block">
                            <MenuHeader keyProps={1} />
                        </div>
                        <div className="user-block">
                            <UserBlock/>
                        </div>
                    </Header>
                    <Layout>
                        <Content className="container" >
                            <FormOrder onSubmit={this.submit} order_id={orderId} orderForm={this.props.orderForm} manager={this.props.manager} updateList={this.update} removePeopleToOrder={this.removePeopleToOrder} />
                        </Content>
                    </Layout>
                </Layout>
            </div>
        );
    }
}


function mapStateToProps(state){
    return {
        orderForm: state.order,
        manager: state.login.request
    }
}

export default connect(mapStateToProps)(OrderFormPage)