import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import { Route, Switch, withRouter } from 'react-router-dom'
import '../sass/main.scss';

import UserBlock from '../components/page-element/menu/UserBlock'
import MenuHeader from '../components/page-element/menu/MenuHeader'

import { Layout, Breadcrumb, Icon, Affix, Pagination } from 'antd';
const { Header, Content, Footer, Sider } = Layout;

import OrderList from './order/orderList'
import AddOrder from './order/addOrder'

//action
import * as orderInfoAction from '../action/order/orderInfoAction'
import * as orderAction from '../action/order/orderAction'

//filter
import PageFilter from './order/orderPageFilter'

class HomePage extends Component {

    constructor(props){
        super(props);
        const {dispatch} = this.props;

        bindActionCreators(orderInfoAction, dispatch);
        bindActionCreators(orderAction, dispatch);

        let data = {
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group
        }

        let action = orderInfoAction.getInfo(data);

        this.state = {
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group,
            page: 1,
            where: {},
            pageSize: 10
        }

        data = {
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group,
            page: 1,
            pageSize: 10
        }

        let actionOrders = orderAction.getOrders(data);

        dispatch(actionOrders);
        dispatch(action);

    }

    onChange = (e, pageSize) => {

       this.setState({page: e, pageSize: pageSize});

       this.updateList(e, pageSize);
       return true;
    }

    updateList = (page, pageSize, where) => {

        let data = {
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group,
            page: page,
            where: where || this.state.where,
            pageSize: pageSize
        }

        const {dispatch} = this.props;
        let action = orderAction.getOrders(data);
        dispatch(action);
    }
    
    updateListToFilter = (where) => {

        const {page, pageSize} = this.state;

        let data = {
            manager_id: this.props.manager.request.result.manager_id,
            group: this.props.manager.request.result.group,
            page: page,
            where: where || this.state.where,
            pageSize: pageSize
        }

        const {dispatch} = this.props;
        let action = orderAction.getOrders(data);
        dispatch(action);
        
        this.setState({where: where})
        
    }

    returnList = () => {

        const {page, pageSize} = this.state;
        const {order, manager} = this.props;

        return <OrderList page={page} pageSize={pageSize} order={order} manager={manager}/>;

    }
    

    render(){

        let pagination = <div></div>;
        const {page,pageSize} = this.state;
        const {manager} = this.props;
        const {loading,request} = this.props.info;

        if (!loading && Object.keys(request).length > 0){
            if (typeof request.count_orders !== 'undefined'){
                pagination = <Pagination
                    onChange={(e, pageSize) => this.onChange(e, pageSize)}
                    total={request.count_orders}
                    showSizeChanger
                    onShowSizeChange = {(e, pageSize) => this.onChange(e, pageSize)}
                    pageSizeOptions = {["1", "2", "10", "20", "30"]}
                    showTotal={(total, range) => `${range[0]}-${range[1]} из ${total} заказов`}
                    pageSize={pageSize}
                    current={page}
                />
            }
        }

        return <div>
            <Layout>
                <Header className="header" style={{ backgroundColor: '#fff', padding: '0 25px' }}>
                    <div className="logo">
                        <img src="/manager/img/logo.png" alt="Logo"/>
                        <span>Раздел Менеджера</span>
                    </div>
                    <div className="menu-block">
                        <MenuHeader keyProps={1} />
                    </div>
                    <div className="user-block">
                        <UserBlock/>
                    </div>
                </Header>
                <Layout>
                   <Content className="container" style={{ height: '100vh' }}>
                      <PageFilter manager={manager.request.result.group} info={this.props.info} updateListToFilter={this.updateListToFilter}/>
                       <div className="add-order-block">
                           <AddOrder info={this.props.info} manager={manager}/>
                       </div>
                       {this.returnList()}
                       {pagination}
                   </Content>
                </Layout>
            </Layout>
        </div>
    }
}

function mapStateToProps(state){
    return {
        info: state.orderInfo,
        order: state.order,
        manager: state.login
    }
}

// export default hot(module)(App)
export default withRouter(connect(mapStateToProps)(HomePage))