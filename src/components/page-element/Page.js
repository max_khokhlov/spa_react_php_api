import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { Button } from 'antd';

export default class Page extends Component {

    onYearBtnClick(e){
        this.props.setYear(+e.target.innerText)
    }


    render (){
        const {year,photos} = this.props
        return <div>
            <p>
                <Button type="primary" onClick={::this.onYearBtnClick} >2015</Button>
                <Button type="primary" onClick={::this.onYearBtnClick} >2016</Button>
                <Button type="primary" onClick={::this.onYearBtnClick} >2018</Button>
            </p>
            <p>У тебя {photos.length} фото за {year} год</p>
        </div>
    }

}

Page.propTypes = {
    year: PropTypes.number.isRequired,
    photos: PropTypes.array.isRequired,
    setYear: PropTypes.func.isRequired
}