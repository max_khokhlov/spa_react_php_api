export const formInfo = {
    "comment": {
        "label": "Комментарий",
        "type": "TextArea"
    },
    "date_start": {
        "label": "Дата начала тура",
        "type": "DatePicker"
    },
    "date_finish": {
        "label": "Дата окончания тура",
        "type": "DatePicker"
    }
}