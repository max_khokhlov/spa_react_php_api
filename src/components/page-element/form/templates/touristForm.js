export const touristForm = {
    "name": {
        "label": "Имя",
        "type": "Input"
    },
    "sex": {
        "label": "Пол",
        "type": "Select"
    },
    "email": {
        "label": "Email",
        "type": "Input"
    },
    "phone": {
        "label": "Телефон",
        "type": "Input"
    },
    "bith_date":{
        "label": "Дата рождения",
        "type": "DatePicker"
    },
    "citizenship":{
        "label": "Гражданство",
        "type": "Select"
    },
    "passport": {
        "label": "Серия Номер паспорта",
        "type": "Input"
    },
    "date_of_issue": {
        "label": "Дата выдачи",
        "type": "DatePicker"
    },
    "date_expires": {
        "label": "Действителен до",
        "type": "DatePicker"
    },
    "comment": {
        "label": "Комментарий",
        "type": "TextArea"
    }
}