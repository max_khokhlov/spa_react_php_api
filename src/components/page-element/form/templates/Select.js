export const citizenship = [
    {
        "value": "1",
        "label": "Россия"
    },
    {
        "value": "2",
        "label": "Украина"
    },
    {
        "value": "3",
        "label": "Белорусь"
    }
];

export const  sex = [
    {
        "value": "male",
        "label": "Мужчина"
    },
    {
        "value": "female",
        "label": "Женщина"
    }
];