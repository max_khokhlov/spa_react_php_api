export const reserveTransport = {
    "transport_name": {
      "label": "Название резерва",
      "type": "Input"
    },
    "city_start": {
        "label": "Город отправления",
        "type": "Select"
    },
    "city_finish": {
        "label": "Город прибытия",
        "type": "Select"
    },
    "date_start": {
        "label": "Дата начала резерва",
        "type": "DatePicker"
    },
    "date_finish": {
        "label": "Дата окончания резерва",
        "type": "DatePicker"
    },
    "type": {
        "label": "Тип транспорта",
        "type": "Select"
    }
}