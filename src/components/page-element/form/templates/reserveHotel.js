export const reserveHotel = {
    "name": {
      "label": "Название резерва",
      "type": "Input"
    },
    "date_start": {
        "label": "Дата начала резерва",
        "type": "DatePicker"
    },
    "date_finish": {
        "label": "Дата окончания резерва",
        "type": "DatePicker"
    },
    "hotel_id": {
        "label": "Отель",
        "type": "Select"
    },
    "original_price": {
        "label": "Цена прямого бронирования номера",
        "type": "InputNumber"
    },
    "client_price": {
        "label": "Цена номера для клиента",
        "type": "InputNumber"
    }
}