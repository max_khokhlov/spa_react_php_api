import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'

import { Form, Icon, Input, Button, Checkbox, DatePicker, Select, Modal } from 'antd';
import InputMask from 'react-input-mask'


import * as r from '../../../constants/Regular';

//form
import {addPeople} from './templates/addPeople'

//select
import * as s from './templates/Select'

//action
import * as peopleAction from '../../../action/people/peopleAction'
import * as orderAction from '../../../action/order/orderAction'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;

let now = moment();
let formated_date = now.format("YYYY-MM-DD");


const formData = {
    "name": "",
    "sex": "",
    "email": "",
    "people_group_id": "",
    "phone": "",
    "citizenship": "",
    "bith_date": formated_date,
    "passport": "",
    "date_of_issue": formated_date,
    "date_expires": formated_date,
    "comment": ""
}

const errValid = {
    hasFeedback: true,
    validateStatus: "error",
    help: ''
}

const errEmailValid = {
    hasFeedback: true,
    validateStatus: "error",
    help: 'Некорректный Email'
}

const success = {
    hasFeedback: true,
    validateStatus:"success"
}

const dateFormat = 'DD.MM.YYYY';

class NormalAddPeopleForm extends Component{

    constructor(props){
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(peopleAction, dispatch);

        let order_id = 0;

        if (typeof this.props.order_id !== 'undefined'){
            formData.order_id = this.props.order_id
        }

        this.state = {
            modalVisible: false,
            submit: false,
            loading: false,
            values: formData
        }

        for (let key in formData){
            let objectError = "Error"+key;
            this.state[objectError] =  {};
        }

    }

    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    returnOption = (name) => {
        const {people_group} = this.props;
        let result;

        switch(name){
            case "citizenship":
                result = s.citizenship
                break;
            case "sex":
                result = s.sex
                break;
            case "people_group_id":
                result = people_group
                break;
        }

        return result.map(option => {
            return <Option value={option.value}>{option.label}</Option>
        })

    }

    onChangeDatePicker (e, name) {
        let returnDate = moment(e);
        // console.log(returnDate.format('YYYY-MM-DD'), name);

        let stateValues = this.state.values;
        stateValues[name] = returnDate.format('YYYY-MM-DD');
        this.setState({values:  stateValues});
    }

    onChangeSelect(e, name){

        let stateValues = this.state.values;
        stateValues[name] = e;
        this.setState({values:  stateValues});

    }

    onChange = (e) => {
        let stateValues = this.state.values;
        stateValues[e.target.id] = e.target.value;

        this.setState({values:  stateValues});
    }

    inputReturn = (formData, label) => {

        let returnInput = [];

        for (let key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "id": key,
                                "className": "ant-input",
                                "type": "text",
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "id": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "id": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "id": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {

                let ErrorState = "Error"+input.FormItem.className;

                if (typeof input.Input !== 'undefined'){
                    if (input.Input.id !== 'phone'){
                        return <FormItem {...input.FormItem} {...this.state[ErrorState]}>
                            <Input {...input.Input}  onChange={this.onChange}></Input>
                        </FormItem>
                    }else{
                        return <FormItem {...input.FormItem} {...this.state[ErrorState]}>
                            <InputMask {...input.Input}  mask="+7 (999) 999-99-99" maskChar=" " onChange={this.onChange}></InputMask>
                        </FormItem>;
                    }
                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem} {...this.state[ErrorState]}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={(e) => {this.onChangeDatePicker(e,input.DatePicker.id)}} /></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem} {...this.state[ErrorState]}><TextArea  {...input.TextArea}  onChange={this.onChange} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem} {...this.state[ErrorState]}><Select defaultValue={input.defaultValue}  {...input.Select}  onChange={(e) => { this.onChangeSelect(e, input.Select.id) }}>{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    handleSubmit = () => {
        this.setState({ loading: true });
        const {dispatch, updateList} = this.props;
        let values = this.state.values;
        this.state.submit = true;

        for (let key in values){
            let errorState = "Error"+key;
            let state = this.state;
            if (values[key].length <= 0){
                this.state.submit = false;
                state[errorState] = errValid;
                this.setState({ loading: false });
            }else{
                if (key == 'email' && !r.VALID_EMAIL.test(values[key])){
                     this.state.submit = false;
                    state[errorState] = errEmailValid
                }else{
                    state[errorState] = success
                }
                this.setState({ loading: false });
            }
        }

        if (this.state.submit){

            let action = peopleAction.addPeople(values);
            dispatch(action);

            setTimeout(function () {
                updateList();
                this.setState({ loading: false });
                this.setModalVisible(false);
            }, 2000);

        }


    }

    render() {

        console.log(this.state);

        const {loading} = this.state;

        return (
            <div>
                <div className="buttons-row">
                    <Button type="primary" icon="plus-circle-o" className="add-button" onClick={() => this.setModalVisible(true)}>Добавить туриста</Button>
                </div>
                <Modal
                    width="1024px"
                    title="Добавить туриста"
                    wrapClassName="vertical-center-modal"
                    visible={this.state.modalVisible}
                    onOk={() => this.handleSubmit()}
                    onCancel={() => this.setModalVisible(false)}
                    footer={[
                        <Button key="back" onClick={() => this.setModalVisible(false)}>Отмена</Button>,
                        <Button key="submit" type="primary" loading={loading} onClick={() => this.handleSubmit()}>
                            Добавить
                        </Button>,
                    ]}
                >
                    <div className="form-add-people">
                        <Form onSubmit={this.handleSubmit}>
                            {this.inputReturn(formData, addPeople)}
                        </Form>
                    </div>
                </Modal>
            </div>


        )

    }

}


NormalAddPeopleForm.PropTypes = {
    order_id: PropTypes.string.isRequired,
    updateList: PropTypes.func.isRequired
}

const AddPeopleForm = reduxForm({
    form: 'addPeople'
})(NormalAddPeopleForm);


export default connect()(AddPeopleForm);