import React, {Component} from "react";
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'

import { Form, Icon, Input, Button, Checkbox, Collapse, message, DatePicker, Select, Modal } from 'antd';

//form
import {formInfo} from './templates/formInfo'

//addPeopleForm
import AddPeopleForm from './addPeople'
import AddTransfer from './addTransfer.js'

//components
import PeopleForm from '../../order/peopleForm'
import DaysTour from '../../order/daysTour'
import Transfer from '../../order/Transfer'
import AddDay from '../../order/addDay'
import ServicesBlock from '../../services/serviceBlock'

//action
import * as managerAction from '../../../action/manager/managerAction'
import * as orderAction from '../../../action/order/orderAction'

const confirm = Modal.confirm;
const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;
const Panel = Collapse.Panel;

const dateFormat = 'DD.MM.YYYY';
const dateFormatInfo = 'DD MMMM YYYY, dddd';

const errValid = {
    hasFeedback: true,
    validateStatus: "error",
    help: ''
}

const errEmailValid = {
    hasFeedback: true,
    validateStatus: "error",
    help: 'Некорректный Email'
}

const success = {
    hasFeedback: true,
    validateStatus:"success"
}

class NormalFormOrder extends Component{

    constructor(props){
        super(props);

        const {dispatch} = this.props;
        bindActionCreators(managerAction, dispatch);
        bindActionCreators(orderAction, dispatch);

        let action = managerAction.getManagerList();
        dispatch(action);

        this.state = {
            loadingBtn: false,
            hotelId: 0,
            hotels: {},
            values: {
                order_id: this.props.order_id
            }
        }


    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }


    formatStatus(status){
        switch(status) {
            case 'new':
                return <div className="status new">Новый</div>
                break;
            case 'register':
                return <div className="status register">Зарегистрирован</div>
                break;
            case 'canceled':
                return <div className="status canceled">Отменен</div>
                break;
            case 'payed':
                return <div className="status payed">Оплачен</div>
                break;
            case 'finish':
                return <div className="status finish">Завершен</div>
                break;
            default:
                return <div className="status no-status">Нет статуса</div>
        }
    }

    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
    }

    inputReturn = (formData, label) => {

        let returnInput = [];

        for (var key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "className": key,
                                "type": "text",
                                "id": key,
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "className": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "className": key,
                                "id": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "className": key,
                                "id": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {
                if (typeof input.Input !== 'undefined'){
                    return <FormItem {...input.FormItem}><Input {...input.Input} onChange={this.onChange} /></FormItem>
                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={(e) => this.onChangeDatePicker(e, input.DatePicker.id)} /></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem}><TextArea  {...input.TextArea}  onChange={this.onChange} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem}><Select defaultValue={input.defaultValue}  {...input.Select}   >{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    updateList = () => {

        const {updateList} = this.props;
        updateList();

    }

    returnOption = (name) => {

        const {manager_info} = this.props;
        let result;


        switch (name){
            case 'manager':
                result = manager_info.request;
                break;

        }


        if (name === 'manager'){

            return result.map(item => {

                let usersArr = [];

                for (let key in item.users){

                    usersArr.push({
                        value: key,
                        title: item.users[key]
                    })

                }

                return <OptGroup label={item.name_group}>

                    {usersArr.map(val => {
                        return <Option value={val.value}>{val.title.name}</Option>
                    })}
                </OptGroup>

            });

        }

    }

    onChangeDatePicker = (e, name) => {
        let stateValues = this.state.values;

        stateValues[name] = e.format('YYYY-MM-DD');;
        this.setState({values:  stateValues});
    }

    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

    }

    onChangeSelect = (e, name) => {

        let stateValues = this.state.values;

        stateValues[name] = e;

    }

    saveTour = () => {

        this.setState({loadingBtn: true});

        const {values} = this.state;
        const {dispatch} = this.props;

        if (Object.keys(values).length > 0){

            let action = orderAction.updateOrder(values);
            dispatch(action);

        }


        this.setState({loadingBtn: false});
        return message.success('Данные сохранены');

    }

    updateForm = (values) => {

        let stateValues = this.state.values;
        stateValues['peoples'] = values;

        this.setState({values: stateValues});

    }

    render() {

        const {removePeopleToOrder} = this.props;
        const {loadingBtn} = this.state;
        const {loading, request} = this.props.orderForm;
        const managerInfo = this.props.manager.result;


        if (loading || typeof request['order_id'] === 'undefined') {

            if (loading) {
                return message.destroy(), message.loading('Получаю данные...', 2000);
            }

            return null;

        }else{

            setTimeout(message.destroy(), 2000);

            const formData = this.props.orderForm.request;
            const {manager_info} = this.props;
            
            let link = "http://pribaltica.blackriver.agency/bitrix/admin/sale_order_view.php?ID=" + formData.order_bitrix_id

            let managerBlock;

            if (typeof manager_info.request !== 'undefined' &&  Object.keys(manager_info.request).length > 0) {

                if(managerInfo.manager_id === "1"){

                    managerBlock =  <div className="manager-selectInfo">
                        <b>Менеджер:</b>
                        <Select className="manager_select" defaultValue={formData.manager_id} style={{width: 200}} onChange={(e) => this.onChangeSelect(e, 'manager_id') }>
                            {this.returnOption('manager')}
                        </Select>
                    </div>

                }else{

                    managerBlock = <div className="managerInfo">
                        <b>Менеджер:</b> {managerInfo.user_name} ({managerInfo.user_login})
                    </div>

                }

            }

            return(
                <div className="form-order-block">
                    <Form onSubmit={this.handleSubmit}>
                        <div className="buttons">
                            <Button type="primary" htmlType="submit" icon="save" className="save-btn" onClick={() => this.saveTour() } loading={loadingBtn}>Сохранить</Button>
                        </div>
                        <Collapse bordered={false} defaultActiveKey={['1', '2', '3', '4', '5', '6']}>
                            <Panel header="Информация о туре" key="1">
                                <div className="info">
                                    <h2>
                                        {formData.name}
                                    </h2>
                                    <div className="col-30">
                                        {managerBlock}

                                        <div className="status">
                                            {this.formatStatus(formData.status)}
                                        </div>
                                    </div>
                                    
                                    <div className="col-50">
                                        <div className="date-order">
                                            Дата заказа: {this.formatDateInfo(formData.date_order)}
                                        </div>
                                        <div className="number-bitrix-order">
                                            <span className="title">Номер заказа в Битриксе:</span>
                                            <a href={link} target="_black">Заказ № {formData.order_bitrix_id}</a>
                                        </div>
                                    </div>
                                    <div className="col-50">
                                        <div className="price_block">
                                            {new Intl.NumberFormat('ru-RU').format(Number(formData.total))} руб.
                                        </div>
                                    </div>
                                </div>
                                {this.inputReturn(formData,formInfo)}
                            </Panel>
                            <Panel header="Туристы" className="tourist-form-block" key="2">
                                <PeopleForm peoples={formData.people} people_group={formData.people_group} updateList={this.updateList} updateForm={this.updateForm} removePeopleToOrder={removePeopleToOrder}/>
                                <div class="row">
                                    <AddPeopleForm people_group={formData.people_group} order_id={formData.order_id} updateList={this.updateList}/>
                                </div>
                            </Panel>
                            <Panel header="Дополнительные услуги туристов" className="services_block_people" key="3">
                                <ServicesBlock order_id={this.props.orderForm.request.order_id} peoples={formData.people} services_list={formData.tour_info.services_list} updateList={this.updateList}/>
                            </Panel>
                            <Panel header="Дни тура" className="day-form-block" key="4">
                                {formData.days !== false?
                                    <div>
                                        <DaysTour days={formData.days} people={formData.people} order_id={this.props.orderForm.request.order_id} tour_info={formData.tour_info} updateList={this.updateList}/>
                                        <AddDay last_day={formData.days[formData.days.length-1]} order_id={this.props.orderForm.request.order_id} city={formData.tour_info.city}/>
                                    </div>
                                    :
                                    <div>
                                        <div>Нет заполненных дней</div>
                                        <AddDay last_day={false} order_id={this.props.orderForm.request.order_id} city={formData.tour_info.city}/>
                                    </div>
                                }
                            </Panel>
                            <Panel header="Транспорт" className="transfer-form-block" key="5">
                                <Transfer order_id={this.props.orderForm.request.order_id} people={formData.people} tour_info={formData.tour_info} updateList={this.updateList} transfer={formData.transfer}></Transfer>
                                <AddTransfer people={formData.people} city={formData.tour_info.city} order_id={this.props.orderForm.request.order_id} date_start={formData.date_start} updateList={this.updateList} date_finish={formData.date_finish} transport_type={formData.tour_info.transport_type} place_type={formData.tour_info.list_place_type}></AddTransfer>
                            </Panel>
                            <Panel header="Конструктор документов" className="documents_block" key="6"></Panel>
                        </Collapse>
                    </Form>

                </div>
            );
        }
    }
}

NormalFormOrder.PropTypes = {
    onSubmit: PropTypes.func.isRequired,
    updateList: PropTypes.func.isRequired,
    removePeopleToOrder: PropTypes.func.isRequired,
    orderForm: PropTypes.array.isRequired,
    manager_id: PropTypes.string.isRequired
}

const FormOrder = reduxForm({
    form: 'order'
})(NormalFormOrder);

function mapStateToProps(state){
    return {
        manager_info: state.manager
    }
}

export default connect(mapStateToProps)(FormOrder);