import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import InfiniteCalendar from 'react-infinite-calendar';
import ReactSVG from 'react-svg'
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet

import { Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

import InputMask from 'react-input-mask'


import * as r from '../../../constants/Regular';

//select
import * as s from './templates/Select'

//action
import * as transferAction from '../../../action/transfer/transferAction'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;

let now = moment();
let formated_date = now.format("YYYY-MM-DD");


const dateFormat = 'DD.MM.YYYY';
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const locale = {
    blank: 'Дата не выбрана',
    headerFormat: 'dddd, D MMM',
    locale: require('date-fns/locale/ru'), // You need to pass in the date-fns locale for the language you want (unless it's EN)
    todayLabel: {
        long: "Сег.",
        short: 'Сег.',
    },
    weekdays: ['Пон', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    weekStartsOn: 1, // Start the week on Monday
};


notification.config({
    placement: "topLeft",
});


class NormalAddTransferForm extends Component{

    constructor(props){
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(transferAction, dispatch);

        this.state = {
            modalVisible: false,
            submit: false,
            loading: false,
            data_place: false,
            type_transport_id: false,
            disableDate: [],
            load_reserve: false,
            selected_number_place: false,
            visible_popconfirm: false,
            date_reserve: this.props.date_start,
            values: {
                order_id: this.props.order_id,
                people: false,
                number_place: false,
                is_reserve: false,
                date_reserve: this.props.date_start,
                city_finish: false,
                city_start: false
            }
        }
    }

    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
    }

    DateFormat(date){

        let dateArr = date.split('-');

        dateArr[1] = dateArr[1] - 1;

        let dateOut = new Date(dateArr[0], dateArr[1], dateArr[2]);

        return dateOut;

    }

    handleSubmit() {

        this.setState({loading: true});
        const {values} = this.state;
        let error = false;

        if (!values.city_start){
            notification['error']({
                message: 'Не выбран город отправления!'
            });
            error = true;
        }

        if (!values.city_finish){
            notification['error']({
                message: 'Не выбран город прибытия!'
            });
            error = true;
        }

        if (!values.people){
            notification['error']({
                message: 'Не выбран турист!'
            });
            error = true;
        }

        if (values.is_reserve){

            if (!values.number_place){
                notification['error']({
                    message: 'Не выбран тип транспорта и место'
                });
                error = true;
            }


        }else{

            if (typeof values.type_transport_id === 'undefined'){
                notification['error']({
                    message: 'Не выбран тип транспорта'
                });
                error = true;
            }

            if (typeof values.type_place_id === 'undefined'){
                notification['error']({
                    message: 'Не выбран тип места'
                });
                error = true;
            }

            if (!values.number_place){
                notification['error']({
                    message: 'Не выбрано место'
                });
                error = true;
            }

        }

        if (typeof values.price === 'undefined'){

             notification['error']({
                 message: 'Введите цену билета',
             });

             error = true;

        }

        if (typeof values.comments === 'undefined'){

             notification['error']({
                 message: 'Введите комментарий',
                 description: 'Впишите в комментарий полезную информацию перезде/перелёте'
             });

             error = true;

        }



        if (!error){
            const {values} =  this.state;
            const {updateList, dispatch} = this.props;


            let action = transferAction.addReserveTransfer(values);
            dispatch(action);

            setTimeout(() => {
                this.defaultValueSelect();
                this.setState({loading: false});
                updateList();
            }, 1000);

        }else{
            this.setState({loading: false});
        }


    }

    returnOption = (name) => {
        const {people, city, transport_type, place_type} = this.props;

        let result = {};

        switch(name){
            case "peoples":
                result = people
                break;
            case "city":
                result = city
                break;
            case "transport_type":
                result = transport_type
                break;
            case "place_type":
                result = place_type
                break;
        }

        if (name === "peoples"){

            let returnOption = [];

            for (let key in result){
                returnOption.push({
                    "value": key,
                    "name": result[key].name+" | "+result[key].phone
                })
            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.name}</Option>
            })

        }else if (name === 'city'){

            let returnOption = [];

            for (let key in result){
                returnOption.push({
                    "value": key,
                    "name": result[key].name
                })
            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.name}</Option>
            })

        }else if (name === 'transport_type'){

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    "value": result[key].transport_type_id,
                    "name": result[key].transport_type_name
                })

            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.name}</Option>
            })

        }else if (name === 'place_type'){

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    "value": result[key].reserve_type_place_id,
                    "name": result[key].place_name
                })

            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.name}</Option>
            })

        }else{
            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })
        }

    }

    onChangeSelect(e, name) {

        const {dispatch} = this.props;

        if (typeof e !== 'undefined' && e !== -1){

            if (name === 'city-start'){

                this.defaultValueSelect();

                this.setState({load_reserve: true});

                let stateValues = this.state.values;

                stateValues['city_start'] = e;
                this.setState({values: stateValues});


                let values = {
                    city_start: e,
                    city_finish: this.state.values.city_finish,
                    date_reserve: this.state.date_reserve,
                }

                let action = transferAction.getTransferByCityID(values);
                dispatch(action);

            }else if (name==='city-finish'){

                this.defaultValueSelect();

                this.setState({load_reserve: true});

                let stateValues = this.state.values;

                stateValues['city_finish'] = e;
                this.setState({values: stateValues});

                let values = {
                    city_start: this.state.values.city_start,
                    city_finish: e,
                    date_reserve: this.state.date_reserve,
                }

                let action = transferAction.getTransferByCityID(values);
                dispatch(action);

            }else if (name ==='date_departure'){

                this.defaultValueSelect();

                this.setState({load_reserve: true});

                const {values} = this.state;

                let date_departure = moment(new Date(e));
                this.setState({date_departure: date_departure.format('YYYY-MM-DD')});

                let stateValues = this.state.values;

                stateValues['date_reserve'] = date_departure.format('YYYY-MM-DD');
                this.setState({values: stateValues});

                if (values.city_start && values.city_finish){
                    let action = transferAction.getTransferByCityID(values);
                    dispatch(action);
                }

            }else if (name === 'peoples'){

                let stateValues = this.state.values;

                stateValues['people'] = e;
                this.setState({values: stateValues});

            }else if(name === 'transport_type'){

                let stateValues = this.state.values;
                stateValues['type_transport_id'] = e;
                this.setState({values: stateValues});

            }else if('place_type'){

                let stateValues = this.state.values;
                stateValues['type_place_id'] = e;
                this.setState({values: stateValues});

            }

            setTimeout(()=>{
                this.setState({load_reserve: false});
            },1500);

        }else{


        }

    }

    onChange = (e) => {

        let stateValues = this.state.values;
        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});
    }

    onChangePrice = (value) => {

         let stateValues = this.state.values;
        stateValues['price'] = value;

        this.setState({values: stateValues});

    }

    defaultValueSelect = () => {

        this.setState({
            load_reserve: false,
            data_place: false,
            type_transport_id: false,
            selected_number_place: false,
            visible_popconfirm: false,
        });

        let stateValues = this.state.values;

        stateValues['number_place'] = false;
        stateValues['is_reserve'] = false;
        delete stateValues['type_transport_id'];

        this.setState({values: stateValues});

    }

    getTabReserve = (data) => {

        let filterType, typeTransport, placeInfo, comment, price;
        const {selected_number_place, data_place} = this.state;

        if (data.transport_types.length > 0){
            filterType = data.transport_types.map((item, index) => {
              return <Checkbox value={index} onChange={this.onChange} defaultChecked={true} >{item}</Checkbox>
            });
        }

        if (data.reserve_transport.length > 0){
            typeTransport = data.reserve_transport.map((item) => {
               return <div className="transport-item">
                   <div className="name">
                       <b>Имя:</b>  {item.transport_name}
                   </div>
                   <div className="type">
                       <b>Тип:</b>  {item.type_name}
                   </div>
                   <div className="btn">
                       <Button
                           type="primary"
                           onClick={()=>{

                               this.setState({
                                   data_place: item.place,
                                   type_transport_id: item.type
                               });

                               this.addTypeTransport();

                           }}>Выбрать</Button>
                   </div>
               </div>
            });
        }

        if (data_place && data_place.length > 0 && selected_number_place){
            placeInfo = data_place.map(item => {
                if (parseInt(item.number_place) === parseInt(selected_number_place)){
                    return <div className="info-place">
                        <div className="type">
                            <b>Тип: </b> {item.type_place_name}
                        </div>
                    </div>
                }
            })
            price = <FormItem label="Цена билета:">
                          <InputNumber
                              id="price"
                              formatter={value => `${value} руб`}
                              onChange={this.onChangePrice}
                            />
                    </FormItem>
            comment = <FormItem label="Комментарий:">
                <TextArea autosize={{ minRows: 2, maxRows: 6 }} id="comments" onChange={this.onChange}/>
            </FormItem>
        }else{
            placeInfo = '';
            comment = '';
            price = '';
        }

        return <TabPane tab="Зарезервированный транспорт" key="1" >
            <div className="filter">
                {filterType}
            </div>
            <div className="reserve-block">
                {typeTransport}
            </div>
            <div className="schema">
                {this.addTypeTransport()}
            </div>
            <div className="selected_place">
                <h4>{selected_number_place? 'Выбрано место №'+selected_number_place : ' '}</h4>
                {placeInfo}
                {price}
                {comment}
            </div>

        </TabPane>

    }

    returnReverve = () => {

       let reserveTransport;
       let {request, loading} = this.props.transfer_reserve;
       const {place_type, transport_type} = this.props

       if (!loading){
           if (request.reserve_transport){
               reserveTransport = this.getTabReserve(request);
           }else{
               reserveTransport = false;
           }
       }

        return <div className="tabs-reserve">
            <Tabs type="card" onChange={ this.defaultValueSelect }>
                {reserveTransport? reserveTransport : ''}
                <TabPane tab="Транспорт" key="2" >
                    <div className="transport_type">
                        <FormItem label="Выберите тип транспорта:" className="transfer-type">
                            <Select defaultValue={-1} allowClear={true} showSearch={true} onChange={(e) => { this.onChangeSelect(e, "transport_type") }} >
                                <Option value={-1} disabled>Выберите тип транспорта</Option>
                                {this.returnOption('transport_type')}
                            </Select>
                        </FormItem>
                    </div>
                    <div className="place_type">
                        <FormItem label="Тип места:" className="transfer-type">
                            <Select defaultValue={-1} allowClear={true} showSearch={true} onChange={(e) => { this.onChangeSelect(e, "place_type") }} >
                                <Option value={-1} disabled>Выберите тип места:</Option>
                                {this.returnOption('place_type')}
                            </Select>
                        </FormItem>
                    </div>
                    <div className="place_number">
                        {this.schemaTransport()}
                    </div>
                    <div className="price">
                        <FormItem label="Цена билета:">
                              <InputNumber
                                  id="price"
                                  formatter={value => `${value} руб`}
                                  onChange={this.onChangePrice}
                                />
                        </FormItem>
                    </div>
                    <div className="comment">
                        <FormItem label="Комментарий:">
                            <TextArea autosize={{ minRows: 2, maxRows: 6 }} id="comments" onChange={this.onChange}/>
                        </FormItem>
                    </div>
                </TabPane>
            </Tabs>
        </div>
    }

    addTypeTransport = () => {

        const {data_place, type_transport_id} = this.state;
        let stateValues = this.state.values;
        let output;

        if (data_place && type_transport_id){

            let path_svg = "/manager/img/type-transport/type_" + type_transport_id + ".svg?v=1";
            let path_jpg = "/manager/img/type-transport/type_" + type_transport_id + ".jpg?v=1";
            let elem  = document.querySelector(".svg_block svg");
            let src;

            output = <div className="svg-schema">
                <ReactSVG
                    path={path_svg}
                    evalScripts="always"
                    onInjected={svg => {

                        const {selected_number_place} = this.state;

                        data_place.map(item => {
                            let element = document.getElementById("place-"+item.number_place);
                            if (item.reserved){
                                element.classList.add("reserved");
                            }else{
                                element.classList.add("active");

                                if (selected_number_place && parseInt(item.number_place) === parseInt(selected_number_place)){
                                    element.classList.add("selected");
                                }
                            }
                        })
                    }}
                    renumerateIRIElements={false}
                    svgClassName="svg-class-name"
                    svgStyle={{ width: 1200 }}
                    className="svg_block"
                    onClick={(e) => {

                        message.destroy();
                        let elem = document.getElementById(e.target.id);

                        let data = elem.dataset.place;
                        let itemClick = {}
                        let place_not = false;

                        data_place.map(item => {

                            if (parseInt(item.number_place) === parseInt(data)){

                                if (item.reserved){
                                    message.warning("Место №"+item.number_place+" уже зарезервировано")
                                }else{
                                    this.setState({selected_number_place: data})

                                    for (let item of document.querySelectorAll('svg rect')) {
                                        item.classList.remove('selected');
                                    }

                                    let title = "Выбрано место №"+item.number_place+" ";
                                    elem.classList.add("selected");

                                    message.success(title);
                                    place_not = false;
                                    itemClick = item;

                                }
                            }

                        });

                        stateValues['number_place'] = itemClick.number_place;
                        stateValues['is_reserve'] = true;
                        stateValues['reserve_transport_id'] = itemClick.reserve_transport_id;
                        stateValues['reserve_type_place_id'] = itemClick.reserve_type_place_id;
                        stateValues['reserve_place_id'] = itemClick.reserve_place_id;

                        this.setState({values: stateValues});

                    }}
                >
                </ReactSVG>
                <img src={path_jpg} style={{width: 1200 }} />
            </div>


        }else{

            output = "Выберите зарезервированный транспорт"

        }

        return output;

    }

    schemaTransport = () => {

        const {values} = this.state;
        let stateValues = this.state.values;
        let output;

        if (values.type_transport_id && !values.is_reserve){

            let path_svg = "/manager/img/type-transport/type_" + values.type_transport_id + ".svg?v=2";
            let path_jpg = "/manager/img/type-transport/type_" + values.type_transport_id + ".jpg?v=2";
            let elem  = document.querySelector(".svg_block svg");
            let src;

            output = <div className="svg-schema">
                <ReactSVG
                    path={path_svg}
                    evalScripts="always"
                    onInjected={svg => {

                        let element = document.querySelectorAll("svg rect");
                        const {selected_number_place} = this.state;

                        for (let item of element){
                            item.classList.add("active");

                            if (item.dataset.place === selected_number_place){
                                item.classList.add("selected");
                            }

                        }

                    }}
                    renumerateIRIElements={false}
                    svgClassName="svg-class-name"
                    svgStyle={{ width: 1200 }}
                    className="svg_block"
                    onClick={(e) => {

                        message.destroy();
                        let elem = document.getElementById(e.target.id);

                        let data = elem.dataset.place;

                        let title = "Выбрано место №"+data+" ";
                        elem.classList.add("selected");

                        message.success(title);

                        stateValues['number_place'] = data;
                        stateValues['is_reserve'] = false;

                        this.setState({selected_number_place: data});
                        this.setState({values: stateValues});

                    }}
                >
                </ReactSVG>
                <img src={path_jpg} style={{width: 1200 }} />
            </div>


        }else{

            output = "Выберите зарезервированный транспорт"

        }

        return output;

    }

    addTransfer = () => {

        const {disableDate, values, load_reserve, loading} = this.state;
        const {date_start, date_finish} = this.props;

        return <Form onSubmit={this.handleSubmit}>
            <div className="date">
                <div className="date-start col-50">
                    <label>
                        Дата въезда:
                    </label>
                    <InfiniteCalendar
                        locale={locale}
                        height={300}
                        selected={this.DateFormat(values.date_reserve)}
                        minDate={this.DateFormat(date_start)}
                        maxDate={this.DateFormat(date_finish)}
                        disabledDates={disableDate}
                        displayOptions={{
                            showHeader: false
                        }}
                        onSelect={(date) => {this.onChangeSelect(date, 'date_departure'); return true;}}
                    />
                </div>
            </div>
            <div className="people">
                <FormItem label="Выберите туриста:" className="transfer-form-item">
                    <Select defaultValue={-1} allowClear={true} showSearch={true} onChange={(e) => { this.onChangeSelect(e, "peoples") }} >
                        <Option value={-1} disabled>Выберите туриста</Option>
                        {this.returnOption('peoples')}
                    </Select>
                </FormItem>
            </div>
            <div className="city">
                <div className="start col-50">
                    <FormItem label="Выберите город отправления:" className="transfer-form-item">
                        <Select defaultValue={-1} allowClear={true} showSearch={true} onChange={(e) => { this.onChangeSelect(e, "city-start") }} >
                            <Option value={-1} disabled>Выберите город отправления</Option>
                            {this.returnOption('city')}
                        </Select>
                    </FormItem>
                </div>
                <div className="finish col-50">
                    <FormItem label="Выберите город прибытия:" className="transfer-form-item">
                        <Select defaultValue={-1} allowClear={true} showSearch={true} onChange={(e) => { this.onChangeSelect(e, "city-finish") }} >
                            <Option value={-1} disabled>Выберите город прибытия</Option>
                            {this.returnOption('city')}
                        </Select>
                    </FormItem>
                </div>
            </div>
            { load_reserve? <Spin indicator={antIcon} /> : this.returnReverve() }
            <div className="btn-row">
                <Button key="back" onClick={() => this.setModalVisible(false)}>Отмена</Button>
                <Button key="submit" type="primary" loading={loading} onClick={() => this.handleSubmit()}>
                    Добавить
                </Button>
            </div>
        </Form>

    }

    render () {

        return <div className="addtransfer-block">
                <div className="buttons-row">
                    <Button type="primary" icon="plus-circle-o" className="add-button" onClick={() => this.setModalVisible(true)}>Добавить резерв транспорта</Button>
                </div>
                <Drawer
                    width={1280}
                    placement="right"
                    closable={false}
                    onClose={() => this.setModalVisible(false)}
                    visible={this.state.modalVisible}
                >
                    <div className="form-addtransfer">
                        {this.addTransfer()}
                    </div>
                </Drawer>
            </div>

    }

}

function mapStateToProps(state){
    return {
        transfer_reserve: state.transfer
    }
}

const AddTransfer = reduxForm({
    form: 'addPeople'
})(NormalAddTransferForm);


export default connect(mapStateToProps)(AddTransfer);