import React, {Component} from "react";
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import * as r from '../../../constants/Regular';

const FormItem = Form.Item;

const warnPass = {
       hasFeedback: true,
       validateStatus: "warning",
       help: 'Пароль не менее 8 символов'
}

const errLoginLength = {
       hasFeedback: true,
       validateStatus: "error",
       help: 'Заполните поле Email'
}
const errLoginValid = {
       hasFeedback: true,
       validateStatus: "error",
       help: 'Некорректный Email'
}

const success = {
    hasFeedback: true,
    validateStatus:"success"
}

class NormalLoginForm extends Component {
     constructor(props){
         super(props);
         this.state = {
             login: '',
             password: '',
             passwordStatus: {},
             loginStatus: {}
         };
         // this.login = React.createRef();
         // this.password = React.createRef();
     }
     handleSubmit = (e) => {
         e.preventDefault();
         const {password, login} = this.state;
         const {onSubmit} = this.props;
         let errorLogin = true;
         let errorPassword = true;
         //
         // let login = this.login.current.props.value;
         // let password = this.password.current.props.value;

         if (password.length < 8){
            this.setState({passwordStatus: warnPass });
         }else{
            errorPassword = false;
            this.setState({passwordStatus: success});
         }

         if (login.length <= 0) {
             this.setState({loginStatus: errLoginLength});
         }else if (!r.VALID_EMAIL.test(login)){
              this.setState({loginStatus: errLoginValid});
         }else{
             errorLogin = false;
             this.setState({loginStatus: success});
         }

         if (!errorLogin && !errorPassword){
            const submit = {
                login: login,
                password: password
            };
            setTimeout(onSubmit(submit));
         }
     }
     onChangePassword = (e) => {
         this.setState({password: e.target.value });
     }
     onChangelogin = (e) => {
         this.setState({login: e.target.value});
     }
      render() {
        let {login, password, passwordStatus, loginStatus} = this.state;
        return (
          <Form onSubmit={this.handleSubmit} className="simplifier-login-form login-form">
            <FormItem {...loginStatus}>
                <Input value={login} onChange={this.onChangelogin} prefix={<Icon type="mail" />} placeholder="Email" />
            </FormItem>
            <FormItem {...passwordStatus}>
                <Input value={password} onChange={this.onChangePassword} prefix={<Icon type="lock" />} type="password" placeholder="Пароль" />
            </FormItem>
            <FormItem className="simplifier-checkbox">
                <Checkbox>Запомнить меня</Checkbox>
            </FormItem>
            <FormItem>
              <div className="btn_block">
                <Button type="primary" htmlType="submit"  className="login-form-button">Зарегистрировать</Button>
                <div className="login_btn">
                    или <a href="/login/" className="ant-btn btn-login">Войти</a>
                </div>
              </div>
            </FormItem>
          </Form>
        );
      }
}

const LoginForm = Form.create()(NormalLoginForm);

export default LoginForm;