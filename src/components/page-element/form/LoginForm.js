import React, {Component} from "react";
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import { Form, Icon, Input, Button, Checkbox, Alert } from 'antd';
import * as r from '../../../constants/Regular';
;


const FormItem = Form.Item;

const warnPass = {
       hasFeedback: true,
       validateStatus: "error",
       help: 'Введите пароль'
}

const errLoginLength = {
       hasFeedback: true,
       validateStatus: "error",
       help: 'Заполните поле Логин'
}

const errLoginValid = {
       hasFeedback: true,
       validateStatus: "error",
       help: 'Некорректный Email'
}

const errLoginAuth = {
       hasFeedback: true,
       validateStatus: "error",
       help: 'Неправильный логин или пароль'
}

const success = {
    hasFeedback: true,
    validateStatus:"success"
}

class NormalLoginForm extends Component {

     constructor(props){
         super(props);

         this.state = {
             login: '',
             password: '',
             passwordStatus: {},
             loginStatus: {},
             redirectLogin: this.props.pathname !== false ? this.props.pathname : "/manager/login"
         };

     }
     handleSubmit = (e) => {
         e.preventDefault();
         const {password, login} = this.state;
         const {onSubmit} = this.props;
         let errorLogin = true;
         let errorPassword = true;
         //
         // let login = this.login.current.props.value;
         // let password = this.password.current.props.value;

         if (password.length <= 0){
            this.setState({passwordStatus: warnPass });
         }else{
            errorPassword = false;
            this.setState({passwordStatus: success});
         }

         if (login.length <= 0) {
             this.setState({loginStatus: errLoginLength});
         }else{
             errorLogin = false;
             this.setState({loginStatus: success});
         }

         if (!errorLogin && !errorPassword){
            const submit = {
                login: login,
                password: password,
                redirect: this.state.redirectLogin
            };
            onSubmit(submit);
         }
     }
     onChangePassword = (e) => {
         this.setState({password: e.target.value });
     }
     onChangelogin = (e) => {
         this.setState({login: e.target.value});
     }
      render() {

        const {loginState} = this.props

        let errorAuth = '';

        if (loginState.isAuth){

           const {redirect} = loginState.request.result;


           if (typeof redirect === 'undefined' || redirect === '/manager/login'){

               return <Redirect to="/manager" />

           }else{

               return <Redirect to={redirect} />

           }




        }

        if (typeof loginState.error !== 'undefined'){

            errorAuth = <div className="error-block">
                    <Alert
                        message="Ошибка авторизации"
                        description={loginState.error}
                        type="error"
                        closable
                    />
                </div>

        }

        const {login, password, passwordStatus, loginStatus} = this.state;

        return (
          <Form onSubmit={this.handleSubmit} className="simplifier-login-form login-form">
            {errorAuth}
            <FormItem {...loginStatus}>
                <Input value={login} onChange={this.onChangelogin} prefix={<Icon type="user" />} name="email" placeholder="Логин" />
            </FormItem>
            <FormItem {...passwordStatus}>
                <Input value={password} onChange={this.onChangePassword} prefix={<Icon type="lock" />} type="password" placeholder="Пароль" />
            </FormItem>
            <FormItem>
              <div className="btn_block">
                <Button type="primary" htmlType="submit"  className="login-form-button">Войти</Button>
              </div>
            </FormItem>
          </Form>
        );
      }
}

NormalLoginForm.PropTypes = {
    onSubmit: PropTypes.func.isRequired,
    login: PropTypes.array.isRequired
}

const LoginForm = reduxForm({
    form: 'login'
})(NormalLoginForm);

// const LoginForm = Form.create()(NormalLoginForm);

export default LoginForm;