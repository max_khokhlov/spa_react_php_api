import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import { extendMoment } from 'moment-range';
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css'; // Make sure to import the default stylesheet

//hotelAction
import * as hotelAction from '../../../action/hotel/hotelAction'

import { Form, Icon, Input, Button, Checkbox, Collapse, Tabs, message, DatePicker, Select, Drawer, notification } from 'antd';

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const Panel = Collapse.Panel;
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const momentRange = extendMoment(moment);


const locale = {
    blank: 'Дата не выбрана',
    headerFormat: 'dddd, D MMM',
    locale: require('date-fns/locale/ru'), // You need to pass in the date-fns locale for the language you want (unless it's EN)
    todayLabel: {
        long: "Сег.",
        short: 'Сег.',
    },
    weekdays: ['Пон', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
    weekStartsOn: 1, // Start the week on Monday
};

notification.config({
    placement: "topLeft",
});

const dateFormat = 'DD.MM.YYYY';

class NormalAddHotelToOrder extends Component{

    constructor(props){
        super(props);

        const {dispatch} = this.props;
        bindActionCreators(hotelAction, dispatch);

        this.state = {
            hotelId: 0,
            peoples_select: 0,
            loading: false,
            modalVisible: false,
            service_id: false,
            disableDate: [],
            service_list: [],
            selectState: {},
            selectPeople: false,
            values: {
                hotelId: 0,
                reserve: false,
                order_day_id: this.props.day_id,
                date_reserve_start: this.props.day_start,
                date_reserve_finish: this.props.day_finish,
                peoples: [],
                services: {}
            }
        }
    }

    disabledDate  = () => {

        const {reserve_date} = this.state;
        const returnDates = [];
        let dateOut;

        if (typeof reserve_date !== 'undefined'){

            reserve_date.map(date => {

                const start = date.date_reserve_start;

                let dateA = moment(date.date_reserve_start);
                let dateB = moment(date.date_reserve_finish);

                let diff = dateB.diff(dateA, 'day');

                for (let i = 0; i <= diff; i++){

                    let dateArr = start.split('-');

                    dateArr[1] = dateArr[1] - 1;

                    dateOut = new Date(dateArr[0], dateArr[1], dateArr[2]);

                    dateOut.setDate(dateOut.getDate() + i);

                    returnDates.push(dateOut);
                }


            });

            this.state.disableDate = returnDates;

        }else{

            this.state.disableDate = [];

        }

    }

    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
        this.setState({ peoples_select: 0 });
    }

    returnOption = (name, id) => {
        const {people} = this.props;
        const {hotel, reserve_hotel ,city} = this.props.tour_info;

        let result = {};


        switch(name){
            case "peoples":
                result = people
                break;
            case "hotel":
                result = hotel[id]
                break;
            case "reserve-hotel":
                result = reserve_hotel[id]
                break;
        }

        if (name === "hotel"){

            let returnOption = [];
            let hotelArr = [];

            for (let key in result) {
                hotelArr.push(result[key]);
            }

            returnOption.push({
                "id_city": id,
                "name": city[id].name ? city[id].name : "Нет названия",
                "hotels": hotelArr
            });

            if (result) {

                return returnOption.map(op_group => {
                    return <OptGroup label={op_group.name}>
                        {op_group.hotels.map(option => {
                            return <Option value={option.id}>{option.name} {option.stars} | от <b>{Math.round(option.price)} руб.</b></Option>
                        })}
                    </OptGroup>
                })

            }else{

                return <Option value=" ">Нет отелей</Option>

            }

        }else if (name === "reserve-hotel"){

            let returnOption = [];
            let reserveArr = [];

            for (let key in result){
                reserveArr.push(result[key]);
            }

            returnOption.push({
                "id_city": id,
                "name": city[id].name? city[id].name: "Нет названия",
                "hotels": reserveArr
            });

            if (result) {

                return returnOption.map(op_group => {

                    let label = "Резерв"

                    return <OptGroup label={label} >
                        {op_group.hotels.map(option => {
                            return <Option value={option.reserve_hotel_id} >{option.name}</Option>
                        })}
                    </OptGroup>
                })

            }else{

                return <Option value=" ">Нет отелей</Option>

            }

        }else if (name === "peoples"){

            let returnOption = [];

            for (let key in result){
                returnOption.push({
                    "value": key,
                    "name": result[key].name+" | "+result[key].phone
                })
            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.name}</Option>
            })

        }else{
            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })
        }



    }

    returnList = (data) => {
        if (data.length > 0){
            return data.map(item => {
                return <li>{item}</li>
            })
        }else{

            return <li>Пусто</li>

        }
    }

    DateFormat(date){

        let dateArr = date.split('-');

        dateArr[1] = dateArr[1] - 1;

        let dateOut = new Date(dateArr[0], dateArr[1], dateArr[2]);

        return dateOut;

    }

    onChangeSelect(e, name) {

        if (typeof e !== 'undefined' && e !== -1){

            if (name === "hotel"){

                //сброс доп услуг
                this.setState({ service_room : false});
                this.setState({ service_id : false});

                let stateValues = this.state.values;
                stateValues['hotelId'] = e;
                this.state.hotelId = e;
                this.setState({values: stateValues});
                this.state.disableDate = [];
                this.returnRoom();

            }else if (name === "reserve-hotel"){

                //сброс доп услуг
                this.setState({ service_room : false});
                this.setState({ service_id : false});

                const {tour_info, city_id} = this.props;

                let stateValues = this.state.values;
                stateValues['hotelId'] = e;
                stateValues['reserve'] = true;
                this.state.hotelId = e;
                this.state.reserve_date = tour_info.reserve_hotel[city_id][e]['reserve'];
                this.setState({values: stateValues});

                this.returnRoom();
                this.disabledDate();

            }else if (name === "peoples-room"){

                let stateValues = this.state.values;

                if (this.find(stateValues['peoples'], e) === -1){
                    stateValues['peoples'].push(e);
                }

            }else if (name ==='date-start'){

                let stateValues = this.state.values;
                let date_start = moment(new Date(e));

                stateValues['date_reserve_start'] = date_start.format('YYYY-MM-DD');
                this.setState({values: stateValues});

            }else if (name ==='date-finish'){

                let stateValues = this.state.values;
                let date_finish = moment(new Date(e));

                stateValues['date_reserve_finish'] = date_finish.format('YYYY-MM-DD');
                this.setState({values: stateValues});

            }else if(name === 'services'){
                this.setState({service_id: e});
            }

        }else{
            this.setState({hotelId: 0});
            this.setState({peoples_select: 0});
            this.returnRoom(0);
        }

    }

    onChange = (e) => {
        this.setState({[e.target.id]: e.target.value});
    }

    defaultValueSelect = () => {

        this.setState({ disableDate : []});
        this.setState({ service_room : false});
        this.setState({ service_id : false});
        this.setState({hotelId: 0});
        this.setState({peoples_select: 0});
        this.setState({ selectState : { value: -1}});
        this.setState({ selectPeople : false});
        this.setState({values: {
            hotelId: 0,
            reserve: false,
            order_day_id: this.props.day_id,
            date_reserve_start: this.props.day_start,
            date_reserve_finish: this.props.day_finish,
            peoples: [],
            services: {}
        }})

    }

    returnPeoples = () => {
        const {peoples_select} = this.state;
        let select = [];

        if (peoples_select > 0) {

            for (let i = 0; i < peoples_select; i++) {

                let index = i + 1;

                let label = "Место "+index;

                select.push({
                    "FormItem" : {
                        "className" : "people-select-"+i,
                        "label" : label,
                    },
                    "Select" : {
                        "id": "peoples-room"
                    },
                    "Option": {
                        "name": "peoples",
                    }
                })

            }

            return select.map(input => {
                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem} >
                        <Select {...input.Select}  onChange={(e) => { this.onChangeSelect(e, input.Select.id) }}>{this.returnOption(input.Option.name)}</Select>
                    </FormItem>
                }
            })

        }
    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i] == value) return i;
        }

        return -1;
    }

    returnRoom = (e, reserve) => {

        if (e === 0){

           return this.viewRoom();

        }else{

            const {hotel, reserve_hotel} = this.props.tour_info;
            let hotelArr = [];

            if (!reserve){
                for (let key in hotel){
                    hotelArr.push(hotel[key])
                }
            }else{
                for (let key in reserve_hotel){
                    hotelArr.push(reserve_hotel[key])
                }
            }

            return hotelArr.map(item => {
                if (typeof item[e] !== 'undefined'){
                    return this.viewRoom(item[e].rooms);
                }
            })

        }

    }

    addServices = () => {

        const {service_id, service_comment} = this.state;

        if (service_id){
            const list = this.state.service_list;
            list.push(service_id);
            this.setState({service_list: list});

            if (service_comment !== '' && typeof service_comment !== 'undefined'){
                const values = this.state.values;

                values['services'][service_id] = {
                    id: service_id,
                    comments: service_comment
                }

                this.setState({values});
                this.setState({service_comment: ''});
            }

        }

        console.log(this.state);
    }

    returnServices = () => {

        const {service_room, service_list, values, service_comment} = this.state;
        let serviceBlockList;

        if (typeof service_room !== 'undefined' && service_room){

            let arrServices = [];

            for (let key in service_room){
                arrServices.push({
                    id: key,
                    name: service_room[key].name,
                    price: Math.ceil(service_room[key].price)
                });
            }

            if (service_list.length > 0){

                serviceBlockList = service_list.map(list => {
                     return <div className="list-item">
                                <span> {service_room[list].name} <b>Цена: {service_room[list].price} руб</b> <span className="comment">{values.services[list]['comments']}</span> </span> <Button className="delete" onClick={() => {this.deleteService(list)}}><Icon type="close-circle-o" /></Button>
                      </div>
                });

            }

            let option = arrServices.map(service => {
                return <Option value={service.id} >{service.name} Цена: <b>{service.price} руб.</b> </Option>
            });

            return <div className="services-list-select">
                    <FormItem label="Доп. услуги:" >
                        <Select defaultValue={-1} allowClear={true} onChange={(e) => { this.onChangeSelect(e, "services") }} >
                            <Option value={-1} disabled>Выберите услугу</Option>
                            {option}
                        </Select>
                    </FormItem>
                    <FormItem label="Комментарий">
                        <TextArea rows={4} id="service_comment" value={service_comment} onChange={this.onChange} />
                    </FormItem>
                    <div className="btn-block">
                        <Button type="primary" onClick={() => this.addServices()}>
                            <Icon type="plus-square" /> Добавить
                        </Button>
                    </div>
                    <div className="service-list">
                        {serviceBlockList}
                    </div>
                </div>


        }else{

            return " ";

        }

    }

    deleteService = (id) => {

        const {service_list , values} = this.state;
        let index = this.find(service_list, id);

        delete values.services[id];
        delete service_list[index];

        this.setState({service_list, values});

    }

    viewRoom = (data) => {

        if (typeof data !== 'undefined'){

            let elemRoom = [];

            for (let key in data){
                elemRoom.push({
                    'id': key,
                    'name': data[key].name,
                    'price': Math.ceil(data[key].price),
                    'facilities': data[key].facilities,
                    'include': data[key].include,
                    'type': data[key].type,
                    'services': data[key].services
                })
            }

            return elemRoom.map(room => {

                return <div className="room-item">
                    <div className="room-info">
                        <div className="name">
                            <b>{room.type.code}</b>
                            <div className="descrition">
                                {room.type.description}
                            </div>
                        </div>
                        <Collapse bordered={false} >
                            <Panel header="Что входит в стоимость" key="1">
                                <ul>
                                    {this.returnList(room.include)}
                                </ul>
                            </Panel>
                            <Panel header="Услуги в номере" key="2">
                                <ul>
                                    {this.returnList(room.facilities)}
                                </ul>
                            </Panel>
                        </Collapse>
                    </div>
                    <div className="price-room">
                        {room.price} руб. / сутки
                    </div>
                    <Button type="primary" onClick={() => this.addPeopleRoom(room)}>
                        <Icon type="plus-square" /> {room.name}
                    </Button>
                </div>
            })
        }else{

            return "Выберите отель";

        }

    }

    addPeopleRoom = (data) => {

        if (typeof data !== 'undefined'){
            let stateValues = this.state.values;
            stateValues['room_id'] = data.id;
            this.setState({values: stateValues});

            if (typeof data.services !== 'undefined'){
                this.setState({service_room: data.services});
            }

            this.state.peoples_select = parseInt(data.type.capacity)

            return this.returnPeoples(), this.returnServices();
        }

    }

    handleSubmit = () => {

        this.setState({loading: true});

        const {values, peoples_select, disableDate} = this.state;
        let error = false;
        let dateOut;
        let returnDates = [];

        const start = values.date_reserve_start;

        let dateA = moment(values.date_reserve_start);
        let dateB = moment(values.date_reserve_finish);

        let diff = dateB.diff(dateA, 'day');

        for (let i = 0; i <= diff; i++){

            let dateArr = start.split('-');

            dateArr[1] = dateArr[1] - 1;

            dateOut = new Date(dateArr[0], dateArr[1], dateArr[2]);

            dateOut.setDate(dateOut.getDate() + i);

            returnDates.push(dateOut);
        }

        if (values.hotelId <= 0){
            notification['error']({
                message: 'Не выбран отель!',
                description: 'Выберите отель из нужной вкладки и раскрывающегося списка.',
            });
            error = true;
        }

        if (typeof values.room_id === 'undefined'){
            notification['error']({
                message: 'Не выбран номер отеля!'
            });
            error = true;
        }

        if (values.peoples.length !== peoples_select){
            notification['error']({
                message: 'Выбраны не все гости в номер!'
            });
            error = true;
        }


        if (disableDate.length > 0){

            let arrDateDisableFormat = [];

            disableDate.map(d_date => {
                arrDateDisableFormat.push(d_date.getTime())
            })

            returnDates.map(date => {

                let time = date.getTime();

                if (this.find(arrDateDisableFormat, time) !== -1){

                    let day_reserve = moment(new Date(date));
                    moment.lang('ru');

                    notification['error']({
                        message: 'Дата '+day_reserve.format('DD MMMM YYYY')+' зарезервирована!'
                    });
                    error = true;
                }

            })

        }

        if (!error){

            const {values} = this.state;
            const {dispatch, updateList} = this.props;
            let action = hotelAction.addReserveHotel(values);
            dispatch(action);

            setTimeout( () => {
                updateList();
                this.setState({loading: false});
            }, 2000);


        }else{
            setTimeout(() => {
                this.setState({loading: false});
            }, 2000);
        }


    }

    addHotels = (data, city_id) => {

        const { disableDate,hotelId, selectState, values, service_list } = this.state;
        const {day_start, day_finish} = this.props;
        let reserveHotel = false;

        if (typeof data.reserve_hotel[city_id] !== 'undefined'){
            reserveHotel =  <TabPane tab="Зарезервированные отели" key="1">
                <FormItem label="Отель:" className="hotel-item">
                    <Select defaultValue={-1} {...selectState} allowClear={true} showSearch={true} onChange={(e) => { this.setState({ selectState : {value: e}}); this.onChangeSelect(e, "reserve-hotel") }} >
                        <Option value={-1} disabled>Выберите отель</Option>
                        {this.returnOption('reserve-hotel', city_id)}
                    </Select>
                </FormItem>
                <div className="rooms">
                    {hotelId <= 0? this.returnRoom(0) : this.returnRoom(this.state.hotelId, true)}
                </div>
                <div className="services">
                    {this.returnServices()}
                </div>
                <div className="peoples">
                    <b>Гости:</b>
                    {this.returnPeoples()}
                </div>
            </TabPane>
        }

        if (typeof data !== 'undefined'){
            return <div className="hotels-block">
                <div className="date">
                    <div className="date-start col-50">
                        <label>
                            Дата въезда:
                        </label>
                        <InfiniteCalendar
                            locale={locale}
                            height={300}
                            selected={this.DateFormat(values.date_reserve_start)}
                            minDate={this.DateFormat(day_start)}
                            maxDate={this.DateFormat(day_finish)}
                            disabledDates={disableDate}
                            displayOptions={{
                                showHeader: false
                            }}
                            onSelect={(date) => {this.onChangeSelect(date, 'date-start'); return true;}}
                        />
                    </div>
                    <div className="date-finish col-50">
                        <label>
                            Дата выезда:
                        </label>
                        <InfiniteCalendar
                            locale={locale}
                            height={300}
                            selected={this.DateFormat(values.date_reserve_finish)}
                            minDate={this.DateFormat(day_start)}
                            maxDate={this.DateFormat(day_finish)}
                            disabledDates={disableDate}
                            displayOptions={{
                                showHeader: false
                            }}
                            onSelect={(date) => {this.onChangeSelect(date, 'date-finish'); return true;}}
                        />
                    </div>
                </div>
                <Tabs type="card" onChange={ this.defaultValueSelect }>
                    {reserveHotel? reserveHotel : ''}
                    <TabPane tab="Отели" key="2" >
                        <div className="hotel">
                            <FormItem label="Отель:" className="hotel-item">
                                <Select defaultValue={-1} {...selectState} allowClear={true} onChange={(e) => { this.setState({ selectState : {}}); this.onChangeSelect(e, "hotel") }} >
                                    <Option value={-1} disabled>Выберите отель</Option>
                                    {this.returnOption('hotel', city_id)}
                                </Select>
                            </FormItem>
                            <div className="rooms">
                                {hotelId <= 0? this.returnRoom(0) : this.returnRoom(this.state.hotelId, false, city_id)}
                            </div>
                            <div className="services">
                                {this.returnServices()}
                            </div>
                            <div className="peoples">
                                <b>Гости:</b>
                                {this.returnPeoples()}
                            </div>
                        </div>
                    </TabPane>
                </Tabs>
            </div>
        }else{
            return <div>Нет отелей</div>
        }
    }

    render () {

        const {tour_info, city_id} = this.props;
        const {loading} = this.state;

        return <div className="add-block">
            <div className="btn-block">
                <Button type="primary" ghost className="add-hotel" onClick={() => this.setModalVisible(true)}> <Icon type="plus-circle" /> Добавить отель</Button>
            </div>
            <Drawer
                width={1280}
                placement="right"
                closable={false}
                onClose={() => this.setModalVisible(false)}
                visible={this.state.modalVisible}
            >
                <div className="form-add-hotel">
                    <Form onSubmit={this.handleSubmit}>
                        {this.addHotels(tour_info, city_id)}
                        <div className="btn-row">
                            <Button key="back" onClick={() => this.setModalVisible(false)}>Отмена</Button>
                            <Button key="submit" type="primary" loading={loading} onClick={() => this.handleSubmit()}>
                                Добавить резерв
                            </Button>
                        </div>
                    </Form>
                </div>
            </Drawer>
        </div>

    }

}

NormalAddHotelToOrder.PropTypes = {
    people: PropTypes.array.isRequired,
    tour_info: PropTypes.array.isRequired,
    city_id: PropTypes.string.isRequired,
    day_id: PropTypes.string.isRequired,
    day_start: PropTypes.string.isRequired,
    day_finish: PropTypes.string.isRequired,
    updateList: PropTypes.func.isRequired
}


const AddHotelToOrder = reduxForm({
    form: 'addPeople'
})(NormalAddHotelToOrder);

export default connect()(AddHotelToOrder);