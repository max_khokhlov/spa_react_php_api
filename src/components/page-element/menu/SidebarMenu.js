import React, {Component} from 'react'
import {Menu, Icon} from 'antd';
const  {SubMenu} = Menu;
import { NavLink  } from 'react-router-dom'

export default class SidebarMenu extends Component {

    render (){
        return (
        <Menu mode="inline" defaultSelectedKeys={['1']} defaultOpenKeys={['sub1']} style={{ height: '100%' }}>
            <Menu.Item key="1"><Icon type="shopping-cart" /><NavLink  to='/'>Магазин</NavLink></Menu.Item>
            <Menu.Item key="2"><Icon type="api" /><NavLink  to='/admin'>Админ</NavLink></Menu.Item>
            <SubMenu key="sub2" title={<span><Icon type="laptop" />subnav 2</span>}>
              <Menu.Item key="5">option5</Menu.Item>
              <Menu.Item key="6">option6</Menu.Item>
              <Menu.Item key="7">option7</Menu.Item>
              <Menu.Item key="8">option8</Menu.Item>
            </SubMenu>
            <SubMenu key="sub3" title={<span><Icon type="notification" />subnav 3</span>}>
              <Menu.Item key="9">option9</Menu.Item>
              <Menu.Item key="10">option10</Menu.Item>
              <Menu.Item key="11">option11</Menu.Item>
              <Menu.Item key="12">option12</Menu.Item>
            </SubMenu>
        </Menu>
        );
    }

}