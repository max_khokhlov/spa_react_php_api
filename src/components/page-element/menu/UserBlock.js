import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'
import { Button, Popconfirm } from 'antd';

import * as loginAction from '../../../action/login/loginAction'

const text = 'Выйти их менеджерского раздела?';

class UserBlock extends Component{

    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(loginAction, dispatch)
    }

    close = () => {
        let { dispatch } = this.props;
        let action = loginAction.logout();
        dispatch(action);
    }


    render () {
        if (this.props.userInfo.isAuth !== false && this.props.userInfo.loading !== true){
            const {user_name} = this.props.userInfo.request.result;
            return(
                <div className="user_info">
                    <div className="user_name">{user_name}</div>
                    <Popconfirm placement="topLeft" title={text} onConfirm={this.close} okText="Да" cancelText="Нет">
                        <Button type="danger">выйти</Button>
                    </Popconfirm>
                </div>
            );
        }else{
            return <Redirect to="/manager/login" />
        }
    }

}

function mapStateToProps(state) {
    return {
        userInfo: state.login
    }
}

UserBlock.propTypes = {
    userInfo: PropTypes.array.isRequired
}

export default connect(mapStateToProps)(UserBlock)