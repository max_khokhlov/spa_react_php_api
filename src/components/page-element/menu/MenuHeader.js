import React, {Component} from 'react'
import {Menu} from 'antd';
import { NavLink  } from 'react-router-dom'

export default class MenuHeader extends Component {

    constructor(props){
        super(props);
    }

    render (){

        let keyProps = "1";

        if (typeof this.props.keyProps !== 'undefined'){

            keyProps = this.props.keyProps.toString()

        }

        return (
        <Menu mode="horizontal" selectedKeys={[keyProps]}   style={{ lineHeight: '64px', backgroundColor: '#fff' }}>
          <Menu.Item key="1">
              <NavLink to="/manager/">Заявки</NavLink>
          </Menu.Item>
          <Menu.Item key="2">
              <NavLink to="/manager/reserve-hotel/">Резервирование отеля</NavLink>
          </Menu.Item>
          <Menu.Item key="3">
              <NavLink to="/manager/reserve-transport/">Резервирование транспорта</NavLink>
          </Menu.Item>
          <Menu.Item key="5">
              <NavLink to="/manager/statistic/">Отчеты</NavLink>
          </Menu.Item>
          <Menu.Item key="6">
              Настройки
          </Menu.Item>
        </Menu>
        );
    }

}