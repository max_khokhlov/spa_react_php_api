import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { List, Avatar, Button, Icon } from 'antd';


export default class ListElem extends Component{
    render () {
        const list = this.props
        console.log(list);
        return (
          <List
            className="demo-loadmore-list"
            itemLayout="horizontal"
            dataSource={list.list}
            renderItem={item => (
              <List.Item actions={[<a>edit</a>, <a>more</a>]}>
                <List.Item.Meta
                  avatar={<Avatar src={item.picture.medium} />}
                  title={<a href="#">{item.name.first}{item.name.last}</a>}
                  description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                />
                <div>content</div>
              </List.Item>
            )}
          />
        );
    }
}


ListElem.propTypes = {
    list: PropTypes.array.isRequired
}