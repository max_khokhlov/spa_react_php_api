import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import { Route, Switch, withRouter } from 'react-router-dom'
import '../../sass/main.scss';

import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'

import { Layout, Breadcrumb, Icon, Affix, Button, Spin } from 'antd';
const { Header, Content, Footer, Sider } = Layout;

//action
import * as transferInfoAction from '../../action/transfer/transferInfoAction'

import TransportList from './transportList'
import TransportFormAdd from './addTransport'

class TransportPage extends Component {

    constructor(props){

        super(props);
        const {dispatch} = this.props;

        bindActionCreators(transferInfoAction, dispatch);

        let data = {
            "status": "transportInfo"
        }

        let action = transferInfoAction.getTypeTransport(data);
        dispatch(action);

    }

    render(){

        const {transport_info} = this.props;
        let addTransport = '';

        if (transport_info.loading){
            addTransport = <div className="loading">
                <Spin/>
            </div>
        }else{
            addTransport = <TransportFormAdd city={transport_info.request.city} type_place={transport_info.request.type_place} type_transport={transport_info.request.type_transport} />
        }

        return <div>
            <Layout>
                <Header className="header" style={{ backgroundColor: '#fff', padding: '0 25px' }}>
                    <div className="logo">
                        <img src="/manager/img/logo.png" alt="Logo"/>
                        <span>Раздел Менеджера</span>
                    </div>
                    <div className="menu-block">
                        <MenuHeader keyProps={3} />
                    </div>
                    <div className="user-block">
                        <UserBlock/>
                    </div>
                </Header>
                <Layout>
                    <Content className="container page-transport" style={{ height: '100vh' }}>
                        {addTransport}
                        <TransportList  />
                    </Content>
                </Layout>
            </Layout>
        </div>
    }
}

function mapStateToProps(state){
    return {
        transport_info: state.transferInfo
    }
}

// export default hot(module)(App)
export default withRouter(connect(mapStateToProps)(TransportPage))