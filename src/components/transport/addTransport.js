import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import ReactSVG from 'react-svg'

import { Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

import InputMask from 'react-input-mask'

//element
import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'

import * as r from '../../constants/Regular';

//form
import {reserveTransport} from '../page-element/form/templates/addReserveTransport'

//action
import * as transferAction from '../../action/transfer/transferAction'

const { Header, Content } = Layout;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;


let now = moment();
let formated_date = now.format("YYYY-MM-DD");


const dateFormat = 'DD.MM.YYYY';

const reserveTransportDefault = {
    "transport_name": "",
    "city_start": "",
    "city_finish": "",
    "date_start": formated_date,
    "date_finish": formated_date,
    "type": ""
}


notification.config({
    placement: "topLeft",
});


class NormalAddReserveTransport extends Component{

    constructor(props){
        super(props);

        this.state = {
            modalVisible: false,
            submit: false,
            loading: false,
            values: {
                transport_name: false,
                date_start: formated_date,
                date_finish: formated_date,
                city_finish: false,
                city_start: false,
                place: []
            }
        }
    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].number_place == value) return i;
        }

        return -1;
    }

    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

        console.log(this.state.values)
    }

    onChangeDatePicker (e, name) {
        let returnDate = moment(e);
        // console.log(returnDate.format('YYYY-MM-DD'), name);

        let stateValues = this.state.values;
        stateValues[name] = returnDate.format('YYYY-MM-DD');
        this.setState({values:  stateValues});
    }

    onChangeSelect = (e, name) => {

        let stateValues = this.state.values;

        stateValues[name] = e;

        this.setState({values: stateValues});

    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }

    inputReturn = (formData, label) => {

        let returnInput = [];

        for (var key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "className": key,
                                "id": key,
                                "className": "ant-input",
                                "type": "text",
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "id": key,
                                "className": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "id": key,
                                "className": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "id": key,
                                "className": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {
                if (typeof input.Input !== 'undefined'){
                    if (input.Input.id !== 'phone'){
                        return <FormItem {...input.FormItem}>
                            <Input {...input.Input} onChange={this.onChange}></Input>
                        </FormItem>
                    }else{
                        return <FormItem {...input.FormItem}>
                            <InputMask {...input.Input} mask="+7 (999) 999-99-99" maskChar=" " onChange={this.onChange}></InputMask>
                        </FormItem>;
                    }

                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={(e) => {this.onChangeDatePicker(e,input.DatePicker.id)}} /></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem}><TextArea  {...input.TextArea}  onChange={this.onChange} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem}><Select defaultValue={input.defaultValue}  {...input.Select} onChange={(e) => { this.onChangeSelect(e, input.Select.id) }} >{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    returnOption = (name, id) => {

        const {city, type_place, type_transport} = this.props;

        let result = {};

        switch(name){
            case "city_start":
                result = city
                break;
            case "city_finish":
                result = city
                break;
            case "type":
                result = type_transport
                break;
        }

        if (name=== 'city_start' || name === 'city_finish'){

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    label: result[key].name,
                    value: result[key].id
                })

            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }else if (name === 'type') {

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    label: result[key].transport_type_name,
                    value: result[key].transport_type_id
                })

            }

            return returnOption.map(option => {

                return <Option value={option.value}>{option.label}</Option>

            })

        } else {

            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }


    }

    addTypeTransport = () => {

        let stateValues = this.state.values;
        const {type} = this.state.values;
        let output;


        if (type) {

            let path_svg = "/manager/img/type-transport/type_" + type + ".svg?v=1";
            let path_jpg = "/manager/img/type-transport/type_" + type + ".jpg?v=1";
            let elem  = document.querySelector(".svg_block svg");
            let src;

            output = <div className="svg-schema">
                <ReactSVG
                    path={path_svg}
                    evalScripts="always"
                    onInjected={svg => {

                        let flag = true;

                        if (stateValues.place <= 0){
                            flag = false;
                        }

                        if (flag){
                            stateValues.place.map(item => {
                                let element = document.getElementById("place-"+item.number_place);

                                if (typeof item.status !== 'undefined' && item.status !== 'delete'){
                                    element.classList.add("selected");
                                }else{
                                    element.classList.remove("selected");
                                }

                            });
                        }


                    }}

                    renumerateIRIElements={false}
                    svgClassName="svg-class-name"
                    svgStyle={{ width: 1200 }}
                    className="svg_block"
                    onClick={(e) => {

                        message.destroy();

                        let elem = document.getElementById(e.target.id);

                        let data = elem.dataset.place;
                        let dataType = elem.dataset.type;

                        if (elem.classList.contains('selected')){

                            elem.classList.remove("selected");

                            let title = "Удалено место №"+data+" ";
                            message.error(title);

                            let index = this.find(stateValues['place'], data);

                            stateValues["place"].splice(index, 1);

                        }else{

                            elem.classList.add("selected");

                            let title = "Выбрано место №"+data+" ";

                            message.success(title);

                            let dataPlace = {
                                "number_place": data,
                                "reserve_type_place_id": dataType,
                                "status": "active"
                            }

                            stateValues['place'].push(dataPlace);


                        }

                        this.setState({values: stateValues});

                    }}
                >
                </ReactSVG>
                <img src={path_jpg} style={{width: 1200 }} />
            </div>


            return output;


        }



    }

    handleSubmit = () => {

        const {values} = this.state;
        const {dispatch} = this.props;
        let error = false;

        if (!values.transport_name){
            error = true;
            notification['error']({
                message: 'Введите имя резерва'
            });
        }

        if (!values.city_start){
            error = true;
            notification['error']({
                message: 'Выберите город отправления'
            });
        }
        if (!values.city_finish){
            error = true;
            notification['error']({
                message: 'Выберите город прибытия'
            });
        }

        if (typeof values.type === 'undefined'){
            notification['error']({
                message: 'Не выбран тип транспорта'
            });
            error = true;
        }

        if (!error){
            console.log(values);
            let action = transferAction.addReserve(values);
            dispatch(action);
            this.setState({modalVisible: false});
        }

    }

    showDrawer = () => {
        this.setState({
            modalVisible: true,
        });
    }

    onClose = () => {
        this.setState({
            modalVisible: false,
        });
    }

    render () {

        const {transfer} = this.state;

        return <div>
                <div className="add-block">
                    <Button type="primary" icon="plus-square" onClick={ () => {
                        this.showDrawer();
                    } }>Добавить резерв</Button>
                </div>
                <Drawer
                    width={1280}
                    placement="right"
                    closable={false}
                    onClose={this.onClose}
                    visible={this.state.modalVisible}
                >
                    <Form className="form-reserve-transport">

                        <h2>Добавить резерв транспорта</h2>

                        {this.inputReturn(reserveTransportDefault, reserveTransport)}

                        <div className="type">
                            {this.addTypeTransport()}
                        </div>

                        <div className="buttons">
                            <Button type="primary"  icon="save" onClick={() => {this.handleSubmit()}} className="save-btn">Сохранить</Button>
                        </div>
                    </Form>
                </Drawer>
            </div>


    }

}


const TransportFormAdd = reduxForm({
    form: 'addReserveTransport'
})(NormalAddReserveTransport);


export default connect()(TransportFormAdd);