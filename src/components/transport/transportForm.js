import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import ReactSVG from 'react-svg'

import { Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

import InputMask from 'react-input-mask'

//element
import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'


//form
import {reserveTransport} from '../page-element/form/templates/reserveTransport'


//action
import * as transferAction from '../../action/transfer/transferAction'


const { Header, Content } = Layout;
const { Option } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;


const dateFormat = 'DD.MM.YYYY';

class NormalUpdateReserveTransport extends Component{

    constructor(props){
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(transferAction, dispatch);

        let reserveId = this.props.match.params.reserveId;

        let data = {
            'reserve_transport_id' : reserveId
        }

        let action = transferAction.getTransfer(data);
        dispatch(action);

        this.state = {
            modalVisible: false,
            submit: false,
            loading: false,
            data_place: false,
            type_transport_id: false,
            disableDate: [],
            load_reserve: false,
            selected_number_place: false,
            visible_popconfirm: false,
            date_reserve: this.props.date_start,
            values: {
                reserve_transport_id: reserveId,
                transport_name: false,
                date_start: false,
                date_finish: false,
                city_finish: false,
                city_start: false,
                place: []
            }
        }
    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].number_place == value) return i;
        }

        return -1;
    }

    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

        console.log(this.state.values)
    }

    onChangeDatePicker (e, name) {
        let returnDate = moment(e);
        // console.log(returnDate.format('YYYY-MM-DD'), name);

        let stateValues = this.state.values;
        stateValues[name] = returnDate.format('YYYY-MM-DD');
        this.setState({values:  stateValues});
    }

    onChangeSelect = (e, name) => {

        let stateValues = this.state.values;

        stateValues[name] = e;

        this.setState({values: stateValues});

    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }

    inputReturn = (formData, label) => {

        let returnInput = [];

        for (var key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "className": key,
                                "id": key,
                                "className": "ant-input",
                                "type": "text",
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "id": key,
                                "className": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "id": key,
                                "className": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "id": key,
                                "className": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {
                if (typeof input.Input !== 'undefined'){
                    if (input.Input.id !== 'phone'){
                        return <FormItem {...input.FormItem}>
                            <Input {...input.Input} onChange={this.onChange}></Input>
                        </FormItem>
                    }else{
                        return <FormItem {...input.FormItem}>
                            <InputMask {...input.Input} mask="+7 (999) 999-99-99" maskChar=" " onChange={this.onChange}></InputMask>
                        </FormItem>;
                    }

                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={(e) => {this.onChangeDatePicker(e,input.DatePicker.id)}} /></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem}><TextArea  {...input.TextArea}  onChange={this.onChange} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem}><Select defaultValue={input.defaultValue}  {...input.Select} onChange={(e) => { this.onChangeSelect(e, input.Select.id) }} >{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    returnOption = (name, id) => {

        const {city, type_place, type_transport} = this.props.transfer.request;

        let result = {};

        switch(name){
            case "city_start":
                result = city
                break;
            case "city_finish":
                result = city
                break;
        }

        if (name=== 'city_start' || name === 'city_finish'){

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    label: result[key].name,
                    value: result[key].id
                })

            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        } else {

            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }


    }

    addTypeTransport = (data_place, type_transport_id) => {

        let stateValues = this.state.values;

        let reserveId = this.props.match.params.reserveId;
        let output;

        console.log(data_place, type_transport_id);

        let path_svg = "/manager/img/type-transport/type_" + type_transport_id + ".svg?v=1";
        let path_jpg = "/manager/img/type-transport/type_" + type_transport_id + ".jpg?v=1";
        let elem  = document.querySelector(".svg_block svg");
        let src;

            output = <div className="svg-schema">
                <ReactSVG
                    path={path_svg}
                    evalScripts="always"
                    onInjected={svg => {

                        let data_concat;
                        let flag = true;

                        if (stateValues.place.length > 0 && data_place.length > 0){
                            data_concat = data_place.concat(stateValues.place);
                        }else if (!data_place && stateValues.place.length > 0){
                            data_concat = stateValues.place;
                        }else if (data_place.length > 0 && stateValues.place.length <= 0){
                            data_concat = data_place;
                        }else{
                            flag = false;
                        }

                        console.log(this.state);

                        if (flag){
                            data_concat.map(item => {
                                let element = document.getElementById("place-"+item.number_place);

                                if (typeof item.status !== 'undefined' && item.status !== 'delete'){
                                    element.classList.add("selected");
                                }else{
                                    element.classList.remove("selected");
                                }

                            });
                        }


                    }}

                    renumerateIRIElements={false}
                    svgClassName="svg-class-name"
                    svgStyle={{ width: 1200 }}
                    className="svg_block"
                    onClick={(e) => {

                        e.stopPropagation();
                        message.destroy();
                        let elem = document.getElementById(e.target.id);

                        let data = elem.dataset.place;
                        let dataType = elem.dataset.type;

                        if (elem.classList.contains('selected')){

                            elem.classList.remove("selected");

                            let title = "Удалено место №"+data+" ";
                            message.error(title);

                            let index = this.find(stateValues['place'], data);

                            if (index !== -1) {

                                stateValues["place"].splice(index, 1);

                            }else{

                                let dataPlace = {
                                    "number_place": data,
                                    "reserve_transport_id": reserveId,
                                    "reserve_type_place_id": dataType,
                                    "status": "delete"
                                }

                                stateValues['place'].push(dataPlace);

                            }

                        }else{

                            elem.classList.add("selected");

                            let title = "Выбрано место №"+data+" ";
                            message.success(title);

                            let index = this.find(stateValues['place'], data);

                            if (index !== -1) {

                                stateValues["place"].splice(index, 1);

                            }else{

                                let dataPlace = {
                                    "number_place": data,
                                    "reserve_transport_id": reserveId,
                                    "reserve_type_place_id": dataType,
                                    "status": "active"
                                }

                                stateValues['place'].push(dataPlace);

                            }


                        }

                        this.setState({values: stateValues});

                    }}
                >
                </ReactSVG>
                <img src={path_jpg} style={{width: 1200 }} />
            </div>


        return output;

    }

    handleSubmit = () => {

        const {values} = this.state;
        const {dispatch} = this.props;

        let action = transferAction.updateReserveTransfer(values);
        dispatch(action);

    }

    render () {

        const {transfer} = this.props;
        message.destroy();

        if (transfer.loading || typeof transfer.request.reserve === 'undefined'){

            if (transfer.loading) {
                return message.loading('Получаю данные...', 2000);
            }

            return null;

        }else{

            let reserve = transfer.request.reserve;
            let type = transfer.request.type_transport;

            return  <div>
                <Layout>
                    <Header className="header" style={{backgroundColor: '#fff', padding: '0 25px'}}>
                        <div className="logo">
                            <img src="/manager/img/logo.png" alt="Logo"/>
                            <span>Раздел Менеджера</span>
                        </div>
                        <div className="menu-block">
                            <MenuHeader />
                        </div>
                        <div className="user-block">
                            <UserBlock/>
                        </div>
                    </Header>
                    <Layout>
                        <Content className="container" >
                            <Form className="form-reserve-transport" onSubmit={this.handleSubmit}>
                                {this.inputReturn(reserve, reserveTransport)}
                                <div className="info">
                                    <div className="type">
                                        {type.map(item => {

                                            if (item.transport_type_id == reserve.type){
                                                return <div className="type-name">
                                                        {item.transport_type_name}
                                                </div>
                                            }
                                        })}
                                    </div>
                                </div>
                                <div className="place">
                                    {typeof reserve.reserve_place !== 'undefined'? this.addTypeTransport(reserve.reserve_place, reserve.type) : this.addTypeTransport([], reserve.type) }
                                </div>
                                <div className="buttons">
                                    <Button type="primary" htmlType="submit" icon="save" className="save-btn">Сохранить</Button>
                                </div>
                            </Form>
                        </Content>
                    </Layout>
                </Layout>
            </div>
        }

    }

}

function mapStateToProps(state){
    return {
        transfer: state.transfer
    }
}

const TransportFormUpdate = reduxForm({
    form: 'updateReserveTransport'
})(NormalUpdateReserveTransport);


export default connect(mapStateToProps)(TransportFormUpdate);