import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import moment from 'moment'
import { NavLink  } from 'react-router-dom'
import { List, Icon, Button, Spin, message, Pagination } from 'antd'

import * as transferAction from '../../action/transfer/transferAction'

const dateFormat = 'DD MMMM YYYY (dd)';

class TransportList extends Component{

    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(transferAction, dispatch);
        
        this.state = {
            page: 1,
            pageSize: 10
        }

        let data = {
            page:  this.state.page,
            pageSize: this.state.pageSize
        }

        let action = transferAction.getTransfersList(data);
        dispatch(action);
    }

    deleteReserve = (id) => {

        const {dispatch} = this.props;
        bindActionCreators(transferAction, dispatch);

        let data = {
            id: id
        }

        let action = transferAction.deleteReserve(data);
        dispatch(action);

    }

    ucFirst(str) {
        if (!str) return str;
        return str[0].toUpperCase() + str.slice(1);
    }

    formatName(name){
        return this.ucFirst(name.toLowerCase())
    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    onChange = (e, pageSize) => {

        this.setState({page: e, pageSize: pageSize});

        this.updateList(e, pageSize);
        return true;
    }

    updateList = (e, pageSize) => {

        const {dispatch} = this.props;

        let data = {
            page:  e,
            pageSize: pageSize
        }

        let action = transferAction.getTransfersList(data);
        dispatch(action);

    }
    
    render () {
        
        let pagination = <div></div>;
        const {page,pageSize} = this.state;
        const {loading, request} = this.props.transfer;
        let result = [];

        if (loading || !request || typeof request === 'undefined') {

            if (loading) {
                return message.destroy(), message.loading('Получаю резервы...', 2000);
            }

            return null;

        }else{
            setTimeout(message.destroy(), 2000)
            result = request

            if (typeof this.props.transport_info.request !== 'undefined'){

                const {count_reserve} = this.props.transport_info.request;

                pagination = <Pagination
                    onChange={(e, pageSize) => this.onChange(e, pageSize)}
                    total={count_reserve}
                    showSizeChanger
                    onShowSizeChange = {(e, pageSize) => this.onChange(e, pageSize)}
                    pageSizeOptions = {["1", "2", "10", "20", "30"]}
                    showTotal={(total, range) => `${range[0]}-${range[1]} из ${total} резервов`}
                    pageSize={pageSize}
                    current={page}
                />

            }
            
        }

        return <div className="reserve-block">
            <List
                className="pribaltica-list"
                itemLayout="horizontal"
                dataSource={result}
                renderItem={item => (
                    <List.Item actions={[<NavLink  to={"/manager/reserve-transport/"+item.reserve_transport_id} ><Button type="primary" ghost>Изменить</Button></NavLink>,<Button onClick={ () => { this.deleteReserve(item.reserve_transport_id) } } type="danger" ghost>Удалить</Button>]}>
                        <List.Item.Meta title={this.formatName(item.transport_name)} />
                        <div>
                            <div className="type">
                                {item.type.transport_type_name}
                            </div>
                            <div className="date">
                                <div className="date-finish"> <b>Начало резерва:</b> <Icon type="calendar" />{this.formatDate(item.date_start)}</div>
                                <div className="date-start"> <b>Конец резерва:</b> <Icon type="calendar" />{this.formatDate(item.date_finish)}</div>
                            </div>
                        </div>
                    </List.Item>
                )}
            />
            {pagination}
        </div>


    }


}

function mapStateToProps(state){
    return {
        transport_info: state.transferInfo,
        transfer: state.transfer
    }
}

export default connect(mapStateToProps)(TransportList)