import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import moment from 'moment'
import { NavLink  } from 'react-router-dom'
import { List, Icon, Button, Spin, message, Pagination } from 'antd'

//action
import * as hotelAction from '../../action/hotel/hotelAction'

const dateFormat = 'DD MMMM YYYY (dd)';

class HotelsList extends Component {

    constructor(props) {
        super(props);
        const {dispatch} = this.props;
        bindActionCreators(hotelAction, dispatch);

        this.state = {
            page: 1,
            pageSize: 10
        }
        
        let data = {
            page:  this.state.page,
            pageSize: this.state.pageSize
        }
        
        let action = hotelAction.getReserveList(data);
        dispatch(action);
        
        
    }

    deleteReserve = (id) => {

        const {dispatch} = this.props;

        let data = {

            'reserve_hotel_id': id

        }

          let action = hotelAction.deleteReserve(data);
        dispatch(action);

    }

    ucFirst(str) {
        if (!str) return str;
        return str[0].toUpperCase() + str.slice(1);
    }

    formatName(name){
        return this.ucFirst(name.toLowerCase())
    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    onChange = (e, pageSize) => {
       
        this.setState({page: e, pageSize: pageSize});

        this.updateList(e, pageSize);
        return true;
    }
    
    updateList = (e, pageSize) => {
        
        const {dispatch} = this.props;
        
        let data = {
            page:  e,
            pageSize: pageSize
        }

        let action = hotelAction.getReserveList(data);
        dispatch(action);
        
    }
    
    render () {
        
        let pagination = <div></div>;
        const {loading, request} = this.props.hotel;
        const {page,pageSize} = this.state;
        let result = [];

        if (loading || !request || typeof request === 'undefined') {

            if (loading) {
                return message.destroy(), message.loading('Получаю резервы...', 2000);
            }

            return null;

        }else{
            
            setTimeout(message.destroy(), 2000);
            result = request;

            if (typeof this.props.hotelInfo.request !== 'undefined'){
                
                const {count_reserve} = this.props.hotelInfo.request;
                
                pagination = <Pagination
                    onChange={(e, pageSize) => this.onChange(e, pageSize)}
                    total={count_reserve}
                    showSizeChanger
                    onShowSizeChange = {(e, pageSize) => this.onChange(e, pageSize)}
                    pageSizeOptions = {["1", "2", "10", "20", "30"]}
                    showTotal={(total, range) => `${range[0]}-${range[1]} из ${total} резервов`}
                    pageSize={pageSize}
                    current={page}
                />

            }
            
        }



        return <div className="reserve-block">
            <List
                className="pribaltica-list hotels-list"
                itemLayout="horizontal"
                dataSource={result}
                renderItem={item => (
                    <List.Item actions={[<NavLink  to={"/manager/reserve-hotel/"+item.reserve_hotel_id} ><Button type="primary" ghost>Изменить</Button></NavLink>,<Button onClick={ () => { this.deleteReserve(item.reserve_hotel_id) } } type="danger" ghost>Удалить</Button>]}>
                        <List.Item.Meta title={this.formatName(item.name)} />
                        <div className="content-info">
                            <div className="col-50">
                                <div className="info">
                                    <div className="city">
                                        <b>Город: </b> {item.city.name}
                                    </div>
                                    <div className="hotel">
                                        <b>Отель: </b> {item.hotel.name}
                                    </div>
                                    {item.room.length != 0 ? <div className="rooms-info">
                                        <div className="left">
                                            <b>Комната: </b>
                                        </div>
                                        <div className="right">
                                            <div className="name"> {item.room[item.room_id].name} </div>
                                        </div>
                                    </div> : '' }
                                </div>
                            </div>
                            <div className="col-50 flex-center">
                                <div className="date">
                                    <div className="date-finish"> <b>Начало резерва:</b> <Icon type="calendar" />{this.formatDate(item.date_start)}</div>
                                    <div className="date-start"> <b>Конец резерва:</b> <Icon type="calendar" />{this.formatDate(item.date_finish)}</div>
                                </div>
                            </div>
                        </div>
                    </List.Item>
                )}
            />
            {pagination}
        </div>

    }

}

function mapStateToProps(state){
    return {
        hotelInfo: state.hotelInfo,
        hotel: state.hotel
    }
}

export default connect(mapStateToProps)(HotelsList)
