import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import { Route, Switch, withRouter } from 'react-router-dom'
import '../../sass/main.scss';

import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'

import { Layout, Breadcrumb, Icon, Affix, Button, Spin } from 'antd';
const { Header, Content, Footer, Sider } = Layout;

//action
import * as hotelInfoAction from '../../action/hotel/hotelInfoAction'

import HotelsList from './hotelsList'
import AddReserveHotels from './addHotels'


class HotelsPage extends Component {


    constructor(props){

        super(props);
        const {dispatch} =this.props;

        bindActionCreators(hotelInfoAction, dispatch);

        let data = {
            "status": "transportInfo"
        }

        let action = hotelInfoAction.getHotelInfo(data);
        dispatch(action);

    }

    render () {

        let addHotels;

        addHotels = <AddReserveHotels  />

        return <div>
            <Layout>
                <Header className="header" style={{ backgroundColor: '#fff', padding: '0 25px' }}>
                    <div className="logo">
                        <img src="/manager/img/logo.png" alt="Logo"/>
                        <span>Раздел Менеджера</span>
                    </div>
                    <div className="menu-block">
                        <MenuHeader keyProps={2} />
                    </div>
                    <div className="user-block">
                        <UserBlock/>
                    </div>
                </Header>
                <Layout>
                    <Content className="container page-transport" style={{ height: '100vh' }}>
                        {addHotels}
                        <HotelsList  />
                    </Content>
                </Layout>
            </Layout>
        </div>

    }

}

function mapStateToProps(state){
    return {
        hotels_info: state.hotelInfo
    }
}

// export default hot(module)(App)
export default withRouter(connect(mapStateToProps)(HotelsPage))