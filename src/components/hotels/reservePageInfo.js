import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import ReactSVG from 'react-svg'

import { Calendar, Badge, Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

import InputMask from 'react-input-mask'

//element
import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'

//form
import {updateReserveHotel} from '../page-element/form/templates/updateReserveHotel'

//action
import * as hotelAction from '../../action/hotel/hotelAction'
import * as hotelInfoAction from '../../action/hotel/hotelInfoAction'

const { Header, Content } = Layout;
const { Option, OptGroup } = Select;
const Panel = Collapse.Panel;
const { TextArea } = Input;
const FormItem = Form.Item;

const dateFormat = 'DD.MM.YYYY';


class ReservePageInfo extends Component{

    constructor(props){

        super(props);

        const {dispatch} = this.props;
        bindActionCreators(hotelAction, dispatch);
        bindActionCreators(hotelInfoAction, dispatch);

        const reserveId = this.props.match.params.reserveId;

        let data = {
            'status': "getInfo"
        }

        const actionInfo = hotelInfoAction.getHotelInfo(data);
        dispatch(actionInfo);

        data = {
            'reserve_hotel_id': reserveId
        }

        this.state = {

            loading: false,
            hotel_id: false,
            values: {
                reserve_hotel_id: reserveId,
                name: false
            }
        }

        const action = hotelAction.getReserve(data);
        dispatch(action);

    }

    checkDate  = (day, month) => {

        const {hotel} = this.props;

        if (typeof hotel.request.reserve !== 'undefined' && hotel.request.reserve.length > 0){

            let reserve_date = hotel.request.reserve;
            let returnArr = [];

            reserve_date.map(date => {

                const start = date.date_reserve_start;

                let dateA = moment(date.date_reserve_start);
                let dateB = moment(date.date_reserve_finish);

                let diff = dateB.diff(dateA, 'day');

                for (let i = 0; i <= diff; i++){

                    let dateArr = moment(start);
                    let checkDate = dateArr.add(i, 'day');

                    if (day == checkDate.date() && month == checkDate.month()){

                        returnArr.push(date);
                    }

                }

            });

            return returnArr;

        }else{

            return false;

        }

    }


    getListData = (value) => {

        let listData = [];
        const {hotel} = this.props;

        if (typeof hotel.request.reserve !== 'undefined' && hotel.request.reserve.length > 0){

            const month = value.month();
            const day = value.date();

            let reserve_info = this.checkDate(day, month);

            if (reserve_info && reserve_info.length > 0){

                reserve_info.map((info) => {

                    let linkHref = "/manager/order/"+info.people.order_id;

                    switch (info.people.order_status) {

                        case 'new':

                            listData.push({type: 'warning', content: info.people.name, link: <a href={linkHref} className="order-link" target="_blank">Заказ</a>}); break;

                        case 'canceled':

                            listData.push({type: 'error', content: info.people.name, link: <a href={linkHref} className="order-link" target="_blank">Заказ</a>}); break;

                        case 'register':

                            listData.push({type: 'processing', content: info.people.name, link: <a href={linkHref} className="order-link" target="_blank">Заказ</a>}); break;

                        case 'payed':

                            listData.push({type: 'success', content: info.people.name, link: <a href={linkHref} className="order-link" target="_blank">Заказ</a>}); break;

                        case 'finish':

                            listData.push({type: 'default', content: info.people.name, link: <a href={linkHref} className="order-link" target="_blank">Заказ</a>}); break;


                        default:

                    }

                });

            }

        }

        return listData || [];
    }

    dateCellRender = (value) => {

        const listData = this.getListData(value);

        return (
            <ul className="events">
                {
                    listData.map(item => (
                        <li key={item.content}>
                            <Badge status={item.type} text={item.content} >
                                {item.link}
                            </Badge>
                        </li>
                    ))
                }
            </ul>
        );
    }

    getMonthData = (value) => {
        if (value.month() === 8) {
            return 1394;
        }
    }

    monthCellRender = (value) => {
        const num = this.getMonthData(value);
        return num ? (
            <div className="notes-month">
                <section>{num}</section>
                <span>Backlog number</span>
            </div>
        ) : null;
    }


    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

    }

    returnList = (data) => {
        if (data.length > 0){
            return data.map(item => {
                return <li>{item}</li>
            })
        }else{

            return <li>Пусто</li>

        }
    }

    onChangeDatePicker (e, name) {
        let returnDate = moment(e);
        // console.log(returnDate.format('YYYY-MM-DD'), name);

        let stateValues = this.state.values;
        stateValues[name] = returnDate.format('YYYY-MM-DD');
        this.setState({values:  stateValues});
    }

    onChangeSelect = (e, name) => {

        let stateValues = this.state.values;

        stateValues[name] = e;

        this.setState({values: stateValues});

    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].number_place == value) return i;
        }

        return -1;
    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    formatDateInfo(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormatInfo)

    }

    inputReturn = (formData, label) => {

        let returnInput = [];

        for (var key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "className": key,
                                "id": key,
                                "className": "ant-input",
                                "type": "text",
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "InputNumber":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "InputNumber" : {
                                "className": key,
                                "id": key,
                                "className": "ant-input",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "id": key,
                                "className": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "id": key,
                                "className": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "id": key,
                                "className": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {
                if (typeof input.Input !== 'undefined'){
                    if (input.Input.id !== 'phone'){
                        return <FormItem {...input.FormItem}>
                            <Input {...input.Input} onChange={this.onChange}></Input>
                        </FormItem>
                    }else{
                        return <FormItem {...input.FormItem}>
                            <InputMask {...input.Input} mask="+7 (999) 999-99-99" maskChar=" " onChange={this.onChange}></InputMask>
                        </FormItem>;
                    }

                }
                if (typeof input.InputNumber !== 'undefined'){
                    if (input.InputNumber.id.match("price")){
                        return <FormItem {...input.FormItem}><InputNumber {...input.InputNumber} formatter={value => `${value} руб`}  /></FormItem>
                    }else{
                        return <FormItem {...input.FormItem}><InputNumber {...input.InputNumber} /></FormItem>
                    }
                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={(e) => {this.onChangeDatePicker(e,input.DatePicker.id)}} /></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem}><TextArea  {...input.TextArea}  onChange={this.onChange} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem}><Select defaultValue={input.defaultValue}  {...input.Select} onChange={(e) => { this.onChangeSelect(e, input.Select.id) }} >{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    returnOption = (name, id) => {

        const {city, hotels} = this.props.hotelInfo.request;

        console.log(city);

        let result = {};

        switch(name){
            case "city_id":
                result = city
                break;
            case "hotel_id":
                result = hotels
                break;
        }

        if (name=== 'city_id'){

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    label: result[key].name,
                    value: result[key].id
                })

            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }else if (name === 'hotel_id') {

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    'name': city[key].name ?  city[key].name : 'Нет названия',
                    'hotels': this.parseHotel(result[key])
                })

            }

            if (result) {

                return returnOption.map(op_group => {
                    return <OptGroup label={op_group.name}>
                        {op_group.hotels.map(option => {
                            return <Option value={option.id}>{option.name} {option.stars} | от <b>{Math.round(option.price)} руб.</b></Option>
                        })}
                    </OptGroup>
                })

            }else{

                return <Option value=" ">Нет отелей</Option>

            }

        } else {

            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }


    }

    parseHotel = (data) => {

        let returnOption = [];

        for (let key in data){

            returnOption.push({
                'id': data[key].id,
                'name': data[key].name,
                'stars': data[key].stars,
                'price': data[key].price
            });

        }

        return returnOption;

    }

    handleSubmit = () => {

        const {values} = this.state;

        const action = hotelAction.updateReserve(values);
        dispatch(action);



    }

    render () {

        const {hotel, hotelInfo} = this.props;
        message.destroy();


        if (hotelInfo.loading || hotel.loading || typeof hotel.request.reserve_hotel_id === 'undefined' || typeof hotelInfo.request.city === 'undefined' ){

            if (hotel.loading || hotelInfo.loading) {
                return message.loading('Получаю данные...', 2000);
            }

            return null;

        }else{

            let reserve = hotel.request;
            let city = hotelInfo.request.city[reserve.city_id];
            let hotelArr = hotelInfo.request.hotels[city.id][reserve.hotel_id];
            let room = hotelArr.rooms[reserve.room_id];
            let name = this.state.values.name;

            return  <div>
                <Layout>
                    <Header className="header" style={{backgroundColor: '#fff', padding: '0 25px'}}>
                        <div className="logo">
                            <img src="/manager/img/logo.png" alt="Logo"/>
                            <span>Раздел Менеджера</span>
                        </div>
                        <div className="menu-block">
                            <MenuHeader />
                        </div>
                        <div className="user-block">
                            <UserBlock/>
                        </div>
                    </Header>
                    <Layout>
                        <Content className="container info-page" >
                            <div className="info-block">
                                <div className="name">
                                    {this.inputReturn(reserve, updateReserveHotel)}
                                    {name? <Button type="primary" className="update" onClick={ () => { this.handleSubmit() }}>Сохранить изменения</Button>: ''}
                                </div>
                                <div className="city">
                                    <b>Город: </b> {city.name}
                                </div>
                                <div className="hotel">
                                    <b>Отель: </b> {hotelArr.name}
                                </div>
                                <div className="hotel-info">
                                    <div className="stars">
                                        <b>Звездность:</b> {hotelArr.stars}
                                    </div>
                                    <div className="text-info" dangerouslySetInnerHTML={{__html: hotelArr.contacts}}>

                                    </div>
                                </div>
                                <div className="room">
                                    <div className="room-info">
                                        <div className="name">
                                            <b>{room.type.code}</b>
                                            <div className="descrition" >
                                                {room.type.description}
                                            </div>
                                        </div>
                                        <Collapse bordered={false} >
                                            <Panel header="Что входит в стоимость" key="1">
                                                <ul>
                                                    {this.returnList(room.include)}
                                                </ul>
                                            </Panel>
                                            <Panel header="Услуги в номере" key="2">
                                                <ul>
                                                    {this.returnList(room.facilities)}
                                                </ul>
                                            </Panel>
                                        </Collapse>
                                    </div>
                                    <div className="price-room">
                                        {Math.round(room.price)} руб. / сутки
                                    </div>
                                </div>
                            </div>
                            <div className="calendar">
                                <div className="calendar-info-block">
                                    <h2>Зарезервированные даты: </h2>
                                    <div className="status">
                                        <Badge status="warning" text="Новый" />
                                        <br />
                                        <Badge status="processing" text="Ожидается оплата" />
                                        <br />
                                        <Badge status="success" text="Подтвержден" />
                                        <br />
                                        <Badge status="error" text="Не подтвержден" />
                                        <br />
                                        <Badge status="default" text="Завершен" />
                                        <br />
                                    </div>
                                </div>
                                <div className="calendar-block">
                                    <Calendar dateCellRender={this.dateCellRender} />
                                </div>
                            </div>
                        </Content>
                    </Layout>
                </Layout>
            </div>

        }

    }

}


function mapStateToProps(state){
    return {
        hotel: state.hotel,
        hotelInfo: state.hotelInfo
    }
}


export default connect(mapStateToProps)(ReservePageInfo);