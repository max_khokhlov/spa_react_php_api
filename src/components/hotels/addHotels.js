import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import ReactSVG from 'react-svg'

import { Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

import InputMask from 'react-input-mask'

//element
import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'

//form
import {reserveHotel} from '../page-element/form/templates/reserveHotel'

//action
import * as hotelAction from '../../action/hotel/hotelAction'
import * as hotelInfoAction from '../../action/hotel/hotelInfoAction'

const { Header, Content } = Layout;


const Panel = Collapse.Panel;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;

const dateFormat = 'DD.MM.YYYY';
let now = moment();
let formated_date = now.format("YYYY-MM-DD");

const defaultReserve = {
    "name": "",
    "date_start": formated_date,
    "date_finish": formated_date,
    "hotel_id": "",
    "original_price": "",
    "client_price": ""
}

class NormalAddReserveHotel extends Component{

    constructor(props){

        super(props);

        const {dispatch} = this.props;

        bindActionCreators(hotelAction, dispatch);
        bindActionCreators(hotelInfoAction, dispatch);


        let data = {
            'status': "getInfo"
        }

        const actionInfo = hotelInfoAction.getHotelInfo(data);
        dispatch(actionInfo);


        this.state = {
            modalVisible: false,
            loading: false,
            room: false,
            hotel_id: false,
            values: {
                date_start: formated_date,
                date_finish: formated_date,
                city: false,
                hotel_id: false,
                original_price: false,
                client_price: false,
                name: false,
                room_id: false
            }
        }

    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].number_place == value) return i;
        }

        return -1;
    }

    onChangeNumber = (e, name) => {

        let stateValues = this.state.values;

        stateValues[name] = e;

        this.setState({values: stateValues});

    }

    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

    }

    changeRoom = (id) => {

        let stateValues = this.state.values;

        stateValues['room_id'] = id;

        this.setState({values: stateValues});

    }

    onChangeDatePicker (e, name) {
        let returnDate = moment(e);
        // console.log(returnDate.format('YYYY-MM-DD'), name);

        let stateValues = this.state.values;
        stateValues[name] = returnDate.format('YYYY-MM-DD');
        this.setState({values:  stateValues});
    }

    onChangeSelect = (e, name) => {

        let stateValues = this.state.values;

        if (name === 'hotel_id'){

            let arr = e.split('|');

            stateValues['city'] = arr[0];
            stateValues['hotel_id'] = arr[1];
            stateValues['room_id'] = false;


        }

        this.setState({ room: false });
        this.setState({values: stateValues});

    }

    formatDate(date){

        let dateArr = date.split('-');
        moment.locale('ru');

        dateArr[1] = dateArr[1] - 1;

        let returnDate = moment(dateArr);

        return returnDate.format(dateFormat)
    }

    inputReturn = (formData, label) => {

        let returnInput = [];

        for (var key in formData){
            if (typeof label[key] !== "undefined") {
                switch (label[key].type){
                    case "Input":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Input" : {
                                "className": key,
                                "id": key,
                                "className": "ant-input",
                                "type": "text",
                                "placeholder": label[key].label,
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "InputNumber":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "InputNumber" : {
                                "className": key,
                                "id": key,
                                "className": "ant-input",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                    case "Select":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "Select" : {
                                "id": key,
                                "className": key
                            },
                            "Option": {
                                "name": key,
                            },
                            "defaultValue": formData[key]
                        });
                        break;
                    }
                    case "DatePicker":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "DatePicker" : {
                                "id": key,
                                "className": key,
                                "format": dateFormat,
                            },
                            "defaultValue": this.formatDate(formData[key])

                        });
                        break;
                    }
                    case "TextArea":{
                        returnInput.push({
                            "FormItem" : {
                                "className" : key,
                                "label" : label[key].label,
                            },
                            "TextArea" : {
                                "id": key,
                                "className": key,
                                "style": "height: 200px",
                                "defaultValue": formData[key]
                            }
                        });
                        break;
                    }
                }
            }
        }

        return <div className="form">
            {returnInput.map(input => {
                if (typeof input.Input !== 'undefined'){
                    if (input.Input.id !== 'phone'){
                        return <FormItem {...input.FormItem}>
                            <Input {...input.Input} onChange={this.onChange}></Input>
                        </FormItem>
                    }else{
                        return <FormItem {...input.FormItem}>
                            <InputMask {...input.Input} mask="+7 (999) 999-99-99" maskChar=" " onChange={this.onChange}></InputMask>
                        </FormItem>;
                    }

                }
                if (typeof input.InputNumber !== 'undefined'){
                    if (input.InputNumber.id.match("price")){
                        return <FormItem {...input.FormItem}><InputNumber {...input.InputNumber} onChange={ (e) => { this.onChangeNumber(e, input.InputNumber.id)}} formatter={value => `${value} руб`}  /></FormItem>
                    }else{
                        return <FormItem {...input.FormItem}><InputNumber {...input.InputNumber} onChange={ (e) => { this.onChangeNumber(e, input.InputNumber.id)}} /></FormItem>
                    }
                }
                if (typeof input.DatePicker !== 'undefined'){
                    return <FormItem {...input.FormItem}><DatePicker defaultValue={moment(input.defaultValue, dateFormat)} {...input.DatePicker} onChange={(e) => {this.onChangeDatePicker(e,input.DatePicker.id)}} /></FormItem>
                }

                if (typeof input.TextArea !== 'undefined'){
                    return <FormItem {...input.FormItem}><TextArea  {...input.TextArea}  onChange={this.onChange} /></FormItem>
                }

                if (typeof input.Select !== 'undefined'){
                    return <FormItem {...input.FormItem}><Select defaultValue={input.defaultValue}  {...input.Select} onChange={(e) => { this.onChangeSelect(e, input.Select.id) }} >{this.returnOption(input.Option.name)}</Select></FormItem>
                }
            })}
        </div>

    }

    returnOption = (name, id) => {

        const {city, hotels} = this.props.hotelInfo.request;

        let result = {};

        switch(name){
            case "city_id":
                result = city
                break;
            case "hotel_id":
                result = hotels
                break;
        }

        if (name=== 'city_id'){

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    label: result[key].name,
                    value: result[key].id
                })

            }

            return returnOption.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }else if (name === 'hotel_id') {

            let returnOption = [];

            for (let key in result){

                returnOption.push({
                    'name': city[key].name ?  city[key].name : 'Нет названия',
                    'hotels': this.parseHotel(result[key])
                })

            }

            if (result) {

                return returnOption.map(op_group => {
                    return <OptGroup label={op_group.name}>
                        {op_group.hotels.map(option => {
                            let value = option.city+"|"+option.id
                            return <Option value={value}>{option.name} {option.stars} | от <b>{Math.round(option.price)} руб.</b></Option>
                        })}
                    </OptGroup>
                })

            }else{

                return <Option value=" ">Нет отелей</Option>

            }

        } else {

            return result.map(option => {
                return <Option value={option.value}>{option.label}</Option>
            })

        }


    }

    parseHotel = (data) => {

        let returnOption = [];

        for (let key in data){

            returnOption.push({
                'id': data[key].id,
                'name': data[key].name,
                'stars': data[key].stars,
                'price': data[key].price,
                'city': data[key].city
            });

        }

        return returnOption;

    }


    returnList = (data) => {
        if (data.length > 0){
            return data.map(item => {
                return <li>{item}</li>
            })
        }else{

            return <li>Пусто</li>

        }
    }

    viewRoom = () => {

        let {values} = this.state;
        const {hotelInfo} = this.props;



        if (values.hotel_id && values.city){

            let data = hotelInfo.request.hotels[values.city][values.hotel_id].rooms;

            let elemRoom = [];

            for (let key in data){
                elemRoom.push({
                    'id': key,
                    'name': data[key].name,
                    'price': Math.ceil(data[key].price),
                    'facilities': data[key].facilities,
                    'include': data[key].include,
                    'type': data[key].type,
                    'services': data[key].services
                })
            }

            return elemRoom.map(room => {

                return <div className="room-item">
                    <div className="room-info">
                        <div className="name">
                            <b>{room.type.code}</b>
                            <div className="descrition">
                                {room.type.description}
                            </div>
                        </div>
                        <Collapse bordered={false} >
                            <Panel header="Что входит в стоимость" key="1">
                                <ul>
                                    {this.returnList(room.include)}
                                </ul>
                            </Panel>
                            <Panel header="Услуги в номере" key="2">
                                <ul>
                                    {this.returnList(room.facilities)}
                                </ul>
                            </Panel>
                        </Collapse>
                    </div>
                    <div className="price-room">
                        {room.price} руб. / сутки
                    </div>
                    <Button type="primary" onClick={ () => { this.changeRoom(room.id); this.setState({ room: room })  }}>
                        <Icon type="plus-square" /> {room.name}
                    </Button>
                </div>
            })
        }else{

            return "Выберите отель";

        }

    }

    handleSubmit = () => {

        const {values} = this.state;
        const {dispatch} = this.props;
        let error = false;

        if (!values.name){
            error = true;
            notification['error']({
                message: 'Введите имя резерва'
            });
        }

        if (!values.city && !values.hotel_id){
            error = true;
            notification['error']({
                message: 'Выберите отель'
            });
        }

        if (!values.room_id){
            error = true;
            notification['error']({
                message: 'Выберите номер резерва'
            });
        }


        if (!values.client_price){
            error = true;
            notification['error']({
                message: 'Выберите цену номера для клиента'
            });
        }


        if (!values.original_price){
            error = true;
            notification['error']({
                message: 'Выберите цену прямого бронирования номера'
            });
        }

        if (!error){
            console.log(values);
            let action = hotelAction.addReserve(values);
            dispatch(action);
            this.setState({modalVisible: false});
        }

    }

    showDrawer = () => {
        this.setState({
            modalVisible: true,
        });
    }

    onClose = () => {
        this.setState({
            modalVisible: false,
        });
    }

    render () {

        const {hotelInfo} = this.props;
        message.destroy();

        if (hotelInfo.loading ||  hotelInfo.hotels === 'undefined'){

            return <div className="loading">
                <Spin/>
            </div>

        }else{

            const {room} = this.state;

            return  <div>
                    <div className="add-block">
                        <Button type="primary" icon="plus-square" onClick={ () => {
                            this.showDrawer();
                        } }>Добавить резерв</Button>
                    </div>
                    <Drawer
                        width={1280}
                        placement="right"
                        closable={false}
                        onClose={this.onClose}
                        visible={this.state.modalVisible}
                    >
                            <Form className="form-reserve-hotels">

                                <h2>Добавить резерв отеля: </h2>

                                {this.inputReturn(defaultReserve, reserveHotel)}

                                <div className="rooms-block">
                                   {this.viewRoom()}
                                   <div className="room-selected-info">
                                       {room? "Выбран номер: "+room.name+" Тип: "+room.type.code : ""}
                                   </div>
                                </div>

                                <div className="buttons">
                                    <Button type="primary"  onClick={ () => {this.handleSubmit()} } icon="save" className="save-btn">Сохранить</Button>
                                </div>
                            </Form>
                    </Drawer>
            </div>

        }

    }

}


function mapStateToProps(state){
    return {
        hotelInfo: state.hotelInfo
    }
}

const AddReserveHotels = reduxForm({
    form: 'updateReserveHotel'
})(NormalAddReserveHotel);


export default connect(mapStateToProps)(AddReserveHotels);