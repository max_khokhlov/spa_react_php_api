
export const document_order_id = "ID"
export const document_type_id = "Тип"
export const order_id = "ID Заказа"
export const people_order_id = "ID Туриста"
export const status = "Статус"
export const url = "Ссылка"


export const name = "Имя"
export const hotel_active = "Активен у отеля"
export const tour_active = "Активен у тура"
export const url_default = "Ссылка"

export const excursion_order_id = "ID"
export const excursion_id = "ID Экскурсии"
export const date_excursion = "Дата экскурсии"
export const time_excursion = "Время экскурсии"
export const date_created = "Дата создания"
export const date_update = "Дата обновления"
export const price = "Цена"

export const hotel_people_id = "ID"
export const reserve_id = "ID Резерва"
export const order_day_id = "ID Дня"

export const date_reserve_start = "Дата начала резерва"
export const date_reserve_finish = "Дата окончания резерва"
export const room_id = "Номер"
export const hotel_id = "Отель"

export const hotel_service_id = "ID"
export const hotel_reserve_id = "ID Резерва"
export const service_id = "ID Доп. услуги"
export const comment = "Комментарий"

export const order_bitrix_id = "ID Заказа из Битрикса"
export const tour_bitrix_id = "ID тура из Битрикса"
export const date_order = "Дата заказа"
export const date_start = "Дата начала"
export const date_finish = "Дата окончания"
export const manager_id = "Менеджер"
export const total = "Цена итого"

export const number_day = "Номер дня"
export const city_id = "Город"

export const people_data_order_id = "ID"
export const bith_date = "Дата рождения"
export const sex = "Пол"
export const citizenship = "Гражданство"
export const passport = "Паспорт"
export const date_of_issue = "Дата выдачи"
export const date_expires = "Дата окончания"

export const people_group_id = "ID Группы"
export const sale = "Скидка"
export const active = "Активность"

export const email = "Email"
export const phone = "Телефон"

export const reserve_hotel_id = "ID резерва"
export const original_price = "Цена агенства"
export const client_price = "Цена клиента"

export const reserve_hotel_people_id = "ID"

export const reserve_place_id = "ID места"
export const reserve_transport_id = "ID Транспорта"
export const reserve_type_place_id = "ID типа места"
export const number_place = "Номер места"

export const transport_name = "Имя транспорта"
export const type = "Тип"
export const city_start = "Город отправления"
export const city_finish = "Город прибытия"

export const reserve_transport_people_id = "ID"
export const is_reserve = "Резерв"
export const transport_type_id = "Тип транспорта"
export const type_place_id = "ID типа места"
export const date_reserve = "Дата резерва"


export const place_name = "Имя"

export const transport_type_name = "Имя"

