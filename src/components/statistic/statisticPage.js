import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Route, Switch, withRouter} from 'react-router-dom'
import ReactExport from "react-data-export";
import '../../sass/main.scss';

import UserBlock from '../page-element/menu/UserBlock'
import MenuHeader from '../page-element/menu/MenuHeader'
import moment from 'moment'

import { Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification, Table } from 'antd';
const {Header, Content, Footer, Sider} = Layout;


const Panel = Collapse.Panel;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const InputGroup = Input.Group;

//EXPORT

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

//action
import * as statisticAction from '../../action/statistic/statisticAction'

//fields
import * as f from './nameFields'

const dateFormat = 'YYYY-MM-DD HH:mm';
let now = moment();
let formated_date = now.format("YYYY-MM-DD HH:mm");
const dateFormatInfo = 'DD MMMM YYYY, dddd';

const dataStatistic = {
    // "document_order": "Документы заказов",
    // "document_type": "Тип документов",
    // "excursion_order": "Экскурсии к заказу",
    "hotel_people": "Нерезервированная бронь отеля",
    "hotel_service": "Дополнительные услуги к номерам",
    "order": "Заказы",
    "order_day": "Дни заказа",
    "people_data_order": "Данные по туристам",
    // "people_group": "Группы покупателей",
    "people_order": "Туристы в заказах",
    "reserve_hotel": "Резерв отелей",
    "reserve_hotel_people": "Бронь резерва отелей",
    "reserve_place": "Зарезервированные места в транспорте",
    // "reserve_transport": "Резерв транспорта",
    "reserve_transport_people": "Бронь резерва транспорта",
    // "reserve_type_place": "Тип места",
    // "transport_type": "Тип транспорта"
}

function find(array, value) {

    for (var i = 0; i < array.length; i++) {
        if (array[i] == value) return i;
    }

    return -1;

}


class StatisticPage extends Component {

    constructor(props) {

        super(props);
        const {dispatch} =this.props;

        bindActionCreators(statisticAction, dispatch);

        let data = {
            'status': 'getTables'
        }

        const action = statisticAction.getTables(data);

        dispatch(action);

        this.state = {
            modalVisible: false,
            loading: false,
            selected_table: false,
            communication: false,
            values: {}
        }

    }

    find(array, value) {

        for (var i = 0; i < array.length; i++) {
            if (array[i].number_place == value) return i;
        }

        return -1;
    }


    onChangeDatePicker = (e, name) => {


        let stateValues = this.state.values;
        let arrDate = []

        e.map(item => {
            arrDate.push(item.format('X'));
        });

        stateValues[name] = arrDate;
        this.setState({values:  stateValues});

    }

    onChange = (e) => {

        let stateValues = this.state.values;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

    }

    onChangeSelect = (e, name) => {

        let stateValues = this.state.values;
        stateValues[name] = e;

        this.setState({values: stateValues});

    }

    returnButtonFilter = (data) => {

        const {statistic} = this.props;
        let returnData = [];

        if (typeof statistic.request.table_list !== 'undefined') {

            for (let key in data) {
                if (find(statistic.request.table_list, key) != -1) {
                    returnData.push(key);
                }
            }


            return returnData.map(item => {
                return <div className="button-item">
                    <Button type="primary" ghost onClick={ () => {
                        this.returnDataFilter(item)
                    }}>{dataStatistic[item]}</Button>
                </div>
            })

        }


    }

    returnOption = (key) => {

        const {option_filter, field_bitrix} = this.props.statistic.request;

        return option_filter[key].map(item => {

            let text = item;

            if (typeof field_bitrix[key] !== 'undefined'){

                if (typeof field_bitrix[key][item] !== 'undefined' ){

                    text = field_bitrix[key][item].name;
                }

            }

            if (key === "is_reserve" || key === "active"){
                if (item === "1"){
                    text = "Да"
                }else{
                    text = "Нет"
                }
            }

            return <Option value={item}>{text}</Option>

        })

    }

    returnDataFilter = (table) => {

        const {dispatch} = this.props;

        const data = {
            'table_name': table,
            'params': false
        }

        this.setState({selected_table: table});

        const action = statisticAction.getData(data);

        dispatch(action);
    }

    returnDataCommunication = (key, table) => {

        const {values, selected_table} = this.state;
        const {dispatch} = this.props;
        let error = false;

        if (typeof values[key] === 'undefined' && Object.keys(values).length === 0){
            error = true;
            notification['error']({
                message: 'Фильтрация по таблице "' + dataStatistic[table] + '" ',
                description: 'Для фильтрации заполните поле "'+f[key]+'" или другие поля'
            });
        }

        if (!error){

            let data = {
                'params': values,
                'key': key,
                'table_join': table,
                'table': selected_table
            }

            const action = statisticAction.getFilterJoin(data);

            dispatch(action);
        }


    }

    onFilter = () => {

        const {dispatch} = this.props;
        const {selected_table, values} = this.state;

        const data = {
            'table_name': selected_table,
            'params': values
        }

        const action = statisticAction.getData(data);

        dispatch(action);

    }

    returnCommunication = () => {

        const {communication} = this.props.statistic.request;
        const {selected_table} = this.state;
        let arrCommunication = [];

        for (let key in communication){
          arrCommunication.push({
             name: f[key],
             key: key,
             tables: communication[key]
          })
        }

        if (arrCommunication.length > 0){
            return arrCommunication.map(field => {

                return <div className="communication-item">
                    <div className="name">
                        {field.name}
                    </div>
                    <div className="tables">
                        {field.tables.map(item => {
                            if (typeof dataStatistic[item] !== 'undefined' && item !== selected_table) {
                                return <div className="button-item">
                                    <Button type="primary" ghost onClick={ () => {
                                        this.returnDataCommunication(field.key, item)
                                    }}>{dataStatistic[item]}</Button>
                                </div>
                            }
                        })}
                    </div>
                </div>
            })
        }else{

            return "Нет связанных таблиц"

        }


    }

    returnTable = () => {

        const {data, option_filter, type_column, field_bitrix} = this.props.statistic.request;
        const {values} = this.state;
        const columns = [];
        let dataRows = [];

        for (let key in option_filter){

            let filter = [];

            let arr_notFilter = [
                'phone',
                'email',
                'name'
            ]

            if (type_column[key] !== 'date' && type_column[key] !== 'datetime' && type_column[key] !== 'timestamp' && type_column[key] !== 'float' && find(arr_notFilter, key) === -1){

                if (Object.keys(option_filter[key]).length > 0){
                    option_filter[key].map(item => {

                        let text = item;

                        if (typeof field_bitrix[key] !== 'undefined'){

                            if (typeof field_bitrix[key][item] !== 'undefined' ){
                                text = field_bitrix[key][item].name;
                            }

                        }

                        filter.push({
                            text: text.toString(),
                            value: item.toString()
                        })

                    });
                }

            }

            columns.push({
                title: f[key],
                dataIndex: key,
                key: key,
                filters: filter,
                filteredValue: values[key] || null,
                filterDropdown: () => {
                    if (type_column[key] === 'datetime' || type_column[key] === 'timestamp'){
                       return <div className="filter-custom date">
                           <FormItem className={key} label={f[key]}><RangePicker
                               showTime={{ format: 'HH:mm' }}
                               format="YYYY-MM-DD HH:mm"
                               placeholder={['Начало промежутка', 'Конец промежутка']}
                               onChange={(e) => {this.onChangeDatePicker(e, key)}}
                               defaultValue={ [moment(typeof this.state.values[key] !== 'undefined' ? moment.unix(parseInt(this.state.values[key][0])) : now, "YYYY-MM-DD HH:mm"), moment(typeof this.state.values[key] !== 'undefined' ? moment.unix(parseInt(this.state.values[key][1])) : now, "YYYY-MM-DD HH:mm")] }
                               onOk={this.onOk}
                               style={{width: "100%"}}
                           /></FormItem>
                           <div className="btn-block">
                               <Button icon="filter" onClick={() => { this.onFilter();  }} type="primary" >Отфильтровать</Button>
                           </div>
                       </div>
                    }else if (type_column[key] === 'date') {
                        return <div className="filter-custom date">
                                <FormItem className={key} label={f[key]}><RangePicker
                                    format="YYYY-MM-DD"
                                    placeholder={['Начало промежутка', 'Конец промежутка']}
                                    onChange={(e) => {
                                        this.onChangeDatePicker(e, key)
                                    }}
                                    defaultValue={ [moment(typeof this.state.values[key] !== 'undefined' ? moment.unix(parseInt(this.state.values[key][0])) : now, "YYYY-MM-DD"), moment(typeof this.state.values[key] !== 'undefined' ? moment.unix(parseInt(this.state.values[key][1])) : now, "YYYY-MM-DD")] || null }
                                    onOk={this.onOk}
                                    style={{width: "100%"}}
                                /></FormItem>
                                <div className="btn-block">
                                    <Button icon="filter" onClick={() => { this.onFilter();  }} type="primary" >Отфильтровать</Button>
                                </div>
                            </div>
                    }else if (type_column[key] === 'float'){
                        return <div className="filter-custom float">
                            <FormItem className={key} label={f[key]}>
                                <InputGroup compact style={{width: "100%"}}>
                                    <Select defaultValue={this.state.values.simbol || "equally"} onChange={(e) => {this.onChangeSelect(e, 'simbol')}} >
                                        <Option value="equally">Равно</Option>
                                        <Option value="better">Больше</Option>
                                        <Option value="less">Меньше</Option>
                                    </Select>
                                    <Input style={{width: "70%" }} id={key} onChange={this.onChange} value={this.state.values[key]}/>
                                </InputGroup>
                            </FormItem>
                            <div className="btn-block">
                                <Button icon="filter" onClick={() => { this.onFilter();  }} type="primary" >Отфильтровать</Button>
                            </div>
                        </div>
                    }else{
                        if (key == 'phone' || key == 'email' || key == 'name'){
                            return  <div className="filter-custom select">
                                <FormItem className={key} label={f[key]}><Select  mode="multiple" showSearch id={key} className={key} defaultValue={this.state.values[key] || -1} onChange={(e) => {this.onChangeSelect(e, key)}} style={{width: "100%"}}><Option value={-1}>Пусто</Option>{this.returnOption(key)}</Select></FormItem>
                                <div className="btn-block">
                                    <Button icon="filter" onClick={() => { this.onFilter();  }} type="primary" >Отфильтровать</Button>
                                </div>
                            </div>
                        }else{
                            return  <div className="filter-custom select">
                                <FormItem className={key} label={f[key]}><Select mode="multiple" id={key} className={key} defaultValue={this.state.values[key] || -1} onChange={(e) => {this.onChangeSelect(e, key)}} style={{width: "100%"}}><Option value={-1}>Пусто</Option>{this.returnOption(key)}</Select></FormItem>
                                <div className="btn-block">
                                    <Button icon="filter" onClick={() => { this.onFilter();  }} type="primary" >Отфильтровать</Button>
                                </div>
                            </div>
                        }
                    }
                }
            })

        }

        data.map(row => {
            for (let key in row){

                if (typeof field_bitrix[key] !== 'undefined'){

                    if (typeof field_bitrix[key][row[key]] !== 'undefined'){

                        //Если таблица резерв транспорта
                        if (key === "transport_type_id"){
                            if (typeof row["is_reserve"] !== 'undefined'){
                                if (parseInt(row["is_reserve"]) === 1){
                                    row[key] = field_bitrix["reserve_type_place_id"][row[key]].name
                                }else{
                                    row[key] = field_bitrix[key][row[key]].name
                                }
                            }
                        }else{
                            row[key] = field_bitrix[key][row[key]].name
                        }

                    }

                }

                //Подмена полей РЕЗЕРВ и АКТИВНОСТЬ
                if (key === "is_reserve" || key === "active"){

                    if (parseInt(row[key]) === 1){
                        row[key] = "Да"
                    }
                    if (parseInt(row[key]) === 0){
                        row[key] = "Нет"
                    }
                }

            }

        });

        return <Table dataSource={data} columns={columns} onChange={this.handleTableChange} />

    }

    handleTableChange = (pagination, filters, sorter) => {

        let stateValues = this.state.values;

        for (let key in filters){
            stateValues[key] = filters[key]
        }

        this.setState({values: stateValues});

        const {dispatch} = this.props;
        const {selected_table, values} = this.state;

        const data = {
            'table_name': selected_table,
            'params': values
        }

        const action = statisticAction.getData(data);

        dispatch(action);

    }

    returnExport = () => {

        const {data, option_filter, field_bitrix} = this.props.statistic.request;

        const columns = [];

        for (let key in option_filter){

            columns.push({
                title: f[key],
                key: key,
                style: {font: {sz: "24", bold: true}}
            })

        }

        let name = "Отчет "+moment(now, dateFormatInfo);

        data.map(row => {
            for (let key in row){

                if (typeof field_bitrix[key] !== 'undefined'){

                    if (typeof field_bitrix[key][row[key]] !== 'undefined'){

                        if (key === "transport_type_id"){

                            if (parseInt(row[key]) === 1){
                                row[key] = field_bitrix["reserve_type_place_id"][row[key]].name
                            }else{
                                row[key] = field_bitrix[key][row[key]].name
                            }

                        }else{

                            row[key] = field_bitrix[key][row[key]].name

                        }

                    }

                }

                //Подмена полей РЕЗЕРВ и АКТИВНОСТЬ
                if (key === "is_reserve" || key === "active"){
                    if (parseInt(row[key]) === 1){
                        row[key] = "Да"
                    }
                    if (parseInt(row[key]) === 0){
                        row[key] = "Нет"
                    }
                }

            }

        });

        return <ExcelFile name={name} element={ <Button icon="file-excel" type="primary" >Скачать отчет</Button>}>
            <ExcelSheet data={data} name="Document">
                {columns.map(column => {
                    return <ExcelColumn label={column.title} value={column.key} />
                })}
            </ExcelSheet>
        </ExcelFile>

    }

    render() {

        message.destroy();

        const {statistic} = this.props;


        if (statistic.loading || Object.keys(statistic.request).length == 0) {

            if (statistic.loading) {

                return message.loading('Загрузка конструктора отчетов ...', 0);

            }

            return null;

        } else {

            const {request} = this.props.statistic;
            const {communication, selected_table} = this.state;

            return <div>
                <Layout>
                    <Header className="header" style={{backgroundColor: '#fff', padding: '0 25px'}}>
                        <div className="logo">
                            <img src="/manager/img/logo.png" alt="Logo"/>
                            <span>Раздел Менеджера</span>
                        </div>
                        <div className="menu-block">
                            <MenuHeader keyProps={5}/>
                        </div>
                        <div className="user-block">
                            <UserBlock/>
                        </div>
                    </Header>
                    <Layout>
                        <Content className="container page-statistic" style={{height: '100vh'}}>
                            <div className="block-statistic">
                                <div className="button-block">
                                    {this.returnButtonFilter(dataStatistic)}
                                </div>
                                <div className="filter-block">
                                    <Form>
                                        {selected_table? <h2>{dataStatistic[selected_table]}</h2>: ""}
                                        {/*{typeof request.option_filter !== 'undefined' ? this.returnSelectFilter() : "" }*/}

                                        <div className="communication-block">
                                            <b>Связанные таблицы</b><br/>
                                            {typeof request.communication !== 'undefined' ? this.returnCommunication() : "" }
                                        </div>

                                        {/*{typeof request.option_filter !== 'undefined' ?*/}
                                            {/*<div className="btn-block">*/}
                                                {/*<Button icon="filter" onClick={() => { this.onFilter();  }} >Отфильтровать</Button>*/}
                                            {/*</div>*/}
                                        {/*: '' }*/}
                                    </Form>
                                </div>

                                <div className="table-block">
                                    {typeof request.data !== 'undefined' ? this.returnTable() : "" }
                                </div>
                                <div className="download-excel-file">
                                    {typeof request.data !== 'undefined' ? this.returnExport() : "" }
                                </div>
                            </div>
                        </Content>
                    </Layout>
                </Layout>
            </div>

        }

    }

}

function mapStateToProps(state) {
    return {
        statistic: state.statistic
    }
}

// export default hot(module)(App)
export default withRouter(connect(mapStateToProps)(StatisticPage))