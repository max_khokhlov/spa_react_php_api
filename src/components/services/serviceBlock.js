import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'

import { Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

//action
import * as serviceAction from '../../action/services/serviceAction'

//addServices
import AddServices from "./addServices"

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;

class ServicesBlock extends Component{
    
    constructor(props){
        super(props);
        
        const {dispatch} = this.props;
        
        this.state = {
            loading: false,
            values: {
                order_id: false,
                people_id: false,
                service_id: false
            }
        }
        
        bindActionCreators(serviceAction, dispatch);
    }

    returnListService = (data) => {
        
        const {services_list} = this.props;
        
        return data.map(item => {
            let service_data = Object.values(services_list).filter(val => {
                return val.id == item.services_id;
            });
            
            return <li className="list-item">
                <div className="name">
                    {service_data[0].name}
                </div>
                <div className="value">
                    {service_data[0].price} руб.
                </div>
                <div className="delete">
                    <button onClick={() => {this.deleteServices(item.order_id, item.people_order_id, service_data[0].id)}} className="delete"><Icon type="close-circle-o" /></button>
                </div>
            </li>
        });
        
    }

    deleteServices = (order_id, people, service_id) => {

        const {dispatch, updateList} = this.props;
        
        let data = {
            order_id: order_id,
            people: people,
            service_id: service_id
        }

        let action = serviceAction.deleteService(data);
        dispatch(action);

        setTimeout(()=>{
            updateList();
        },300);
        
    }
    
    updateList = () => {
        
        const {updateList} = this.props;
        
        updateList();
        
    }
    
    render(){
        const {peoples,order_id,services_list} = this.props;
        const {loading} = this.state;
        let listPeoples = Object.values(peoples);
        
        return <div className="service_people_list">
            {listPeoples.map(people => {
                return <div className="col-50">
                    <div className="service_item">
                        <div className="name">{people.name} | {people.phone}</div>
                        <ul className="list">
                            {people.services? this.returnListService(people.services) : "Нет дополнительных услуг"}
                        </ul>
                        <AddServices people_id={people.people_order_id} order_id={order_id} services_list={services_list} updateList={this.updateList}/>
                    </div>
                </div>
            })}
        </div>
    }
    
}

ServicesBlock.PropTypes = {
    peoples: PropTypes.array.isRequired,
    services_list: PropTypes.object.isRequired,
    updateList: PropTypes.func.isRequired
}

export default connect()(ServicesBlock);