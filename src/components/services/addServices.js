import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'

import { Layout, Form, Spin, Icon, Input, InputNumber, Button, Checkbox, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Modal, notification } from 'antd';

//action
import * as serviceAction from '../../action/services/serviceAction'

const { MonthPicker, RangePicker, WeekPicker } = DatePicker;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;

class AddServices extends Component {

    constructor(props) {
        super(props);

        const {dispatch} = this.props;

        this.state = {
            loading: false,
            values: {
                order_id: false,
                people_id: false,
                service_id: false
            }
        }

        bindActionCreators(serviceAction, dispatch);
    }

    onChangeSelect = (id, name, people_id) => {

        const {order_id} = this.props;

        this.setState({values:{
            order_id: order_id,
            people_id: people_id,
            service_id: id
        }});

    }

    returnOption = (name) => {

        let result = [];
        const {services_list} = this.props;

        switch (name){
            case "services":
                result = services_list;
                break;
        }

        return result.map(option => {
            return <Option value={option.id}>{option.name}</Option>
        })

    }
    
    addServices = () => {

        this.setState({loading: true});
        const {dispatch, updateList, order_id} = this.props;
        const {values} = this.state;

        let data = {
            order_id: values.order_id,
            people: values.people_id,
            service_id: values.service_id
        }

        if (values.service_id && values.people_id && values.order_id){

            let action = serviceAction.addService(data);
            dispatch(action);

            setTimeout(()=>{
                updateList();
            },300);

        }else{

            notification['error']({
                message: 'Не выбрана услуга'
            });

        }

        setTimeout(()=>{
            this.setState({loading: false});
        },200)

    }
    
    render(){
        
        const {people_id} = this.props;
        const {loading} = this.state;
        
        return <div className="add-service">
            <FormItem label="Добавить услугу">
                <Select defaultValue={-1} onChange={(e) => { this.onChangeSelect(e, "services", people_id) }}>
                    <Option disable value={-1}>
                        Без услуг
                    </Option>
                    {this.returnOption("services")}
                </Select>
            </FormItem>
            <Button type="primary" onClick={() => {this.addServices()}} loading={loading}>Добавить услугу</Button>
        </div>
        
    }

}


AddServices.PropTypes = {
    order_id: PropTypes.string.isRequired,
    people_id: PropTypes.string.isRequired,
    services_list: PropTypes.object.isRequired,
    updateList: PropTypes.func.isRequired
}

export default connect()(AddServices);