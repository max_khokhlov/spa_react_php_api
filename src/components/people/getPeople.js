import React, {Component} from "react";
import { bindActionCreators } from 'redux'
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import {connect} from 'react-redux';
import moment from 'moment'
import InputMask from 'react-input-mask'

import { Layout, Form, Spin, Icon, Input, Button, Collapse, Tabs, Popconfirm, message, DatePicker, Select, Drawer, Radio, notification } from 'antd';

//regular
import * as r from '../../constants/Regular'

//action
import * as peopleAction from '../../action/people/peopleAction'

const { Header, Content } = Layout;
const { Option, OptGroup } = Select;
const { TextArea } = Input;
const FormItem = Form.Item;


let now = moment();
let formated_date = now.format("YYYY-MM-DD");


const dateFormat = 'DD.MM.YYYY';

const reserveTransportDefault = {
    "order_name": "",
    "tour_id": "",
    "date_start": formated_date,
}


notification.config({
    placement: "topLeft",
});

class NormalGetPeople extends Component{
    
    constructor(props){
        super(props);
        const {dispatch} = this.props;
        
        bindActionCreators(peopleAction, dispatch);
        
        this.state = {
            mode: "add",
            values: {
                email: '',
                phone: ''
            }
        }
    }

    onChange = (e) => {

        let stateValues = this.state.values;
        const {getPeople} = this.props;

        stateValues[e.target.id] = e.target.value;

        this.setState({values: stateValues});

        getPeople(this.state.values);
        
    }

    handleModeChange = (e) => {
        const mode = e.target.value;
        this.setState({ mode });
        this.setState({
            values: {
                email: '',
                phone: ''
            }
        })
    }

    resultUser = () => {
        
    }

    returnSearchPeople = () => {
        
        return <div className="search-people">
            <FormItem label="Email:">
                <Input className="email" type="text" key="email" id="email" onChange={this.onChange} placeholder="Ввведите Email"></Input>
            </FormItem>
            <div className="buttons">
                <Button type="primary"  icon="save" onClick={() => {this.searchPeople()}} className="save-btn">Найти пользователя</Button>
            </div>
        </div>
        
    }

    searchPeople = () => {

        const {values} = this.state;
        let error = false;
        
        if (typeof values.email === 'undefined'){
            error = true;
            notification['error']({
                message: 'Введите Email'
            });
        }else{
            if (!r.VALID_EMAIL.test(values.email)){
                error = true;
                notification['error']({
                    message: 'Некорректный Email'
                });   
            }
        }
        if (typeof values.phone === 'undefined'){
            error = true;
            notification['error']({
                message: 'Введите телефон'
            });
        }else{
            if (!r.VALID_PHONE.test(values.phone)){
                error = true;
                notification['error']({
                    message: 'Некорректный телефон'
                });   
            }
        }
        
        if(!error){
            console.log(this.state);
        }
    }

    returnAddPeople = () => {
        return <div className="add-form">
                <FormItem label="Фамилия:">
                    <Input className="surname" type="text" key="surname" id="surname" onChange={this.onChange} placeholder="Ввведите фамилию"></Input>
                </FormItem>
                <FormItem label="Имя:">
                    <Input className="firstname" type="text" key="firstname" id="firstname" onChange={this.onChange} placeholder="Ввведите имя"></Input>
                </FormItem>
                <FormItem label="Email:">
                    <Input className="email" type="text" key="email" id="email" onChange={this.onChange} placeholder="Ввведите Email"></Input>
                </FormItem>
                <FormItem label="Телефон:">
                    <InputMask defaultValue={this.state.phone} className="ant-input" type="text" id="phone" key="phone"  mask="+7 (999) 999-99-99" maskChar=" " onChange={this.onChange} placeholder="Введите телефон"></InputMask>
                </FormItem>
        </div>
    }
    
    render () {
        
        const {mode} = this.state;
        
        return <div className="block-people">
            <Radio.Group className="tabs" onChange={this.handleModeChange} value={mode} style={{ marginBottom: 8 }}>
                <Radio.Button value="add">Добавить пользователя</Radio.Button>
            </Radio.Group>

            <Form className="form-people" onSubmit={this.handleSubmit}>
                {mode === 'search' ? this.returnSearchPeople() : this.returnAddPeople()}
            </Form>
            
            
            {this.resultUser}
            
        </div>;
    }
    
    
}

function mapStateToProps(state){
    return {
        people: state.people,
    }
}

const GetPeople = reduxForm({
    form: 'getPeople'
})(NormalGetPeople);

GetPeople.PropTypes = {
    getPeople: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(GetPeople)