import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import {initialize} from 'redux-form';
import {connect} from 'react-redux'
import { getFormValues } from 'redux-form';


import '../../sass/form-style.scss'

import * as loginAction from '../../action/login/loginAction'

import LoginForm from '../page-element/form/LoginForm'


class Login extends Component{

   constructor(props) {
        super(props);
        const {dispatch} = this.props;

       this.state = {
           pathname: this.props.pathname || false
       }

        bindActionCreators(loginAction, dispatch);
    }

    submit = (values) => {
      let { dispatch } = this.props;
      let action = loginAction.login(values);
      dispatch(action);
    }

    render (){

        return <div className="simplifier-login-block">
            <div className="logo">
               <img src="./img/logo.png" alt="Logo"/>
               <span>Менеджерам</span>
            </div>
            <LoginForm onSubmit={this.submit} pathname={this.state.pathname} {...this.props}/>
        </div>
    }
}

function mapStateToProps(state){
    return {
        loginState: state.login
    }
}

// export default hot(module)(Login)
export default connect(mapStateToProps)(Login)