import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import { LocaleProvider } from 'antd';
import ru_RU from 'antd/lib/locale-provider/ru_RU';

import App from './containers/App'
// import './css/style.css'

import configureStore from './store/configureStore'

import { BrowserRouter, Route, Switch } from 'react-router-dom'

const store = configureStore()

render(
    <Provider store={store}>
       <LocaleProvider locale={ru_RU}>
           <BrowserRouter>
              <App/>
           </BrowserRouter>
       </LocaleProvider>
    </Provider>,
    document.getElementById('root')
)