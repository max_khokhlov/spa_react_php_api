import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import rootReducer from '../reducers'
import {createLogger} from 'redux-logger'
import ReduxThunk from 'redux-thunk'

import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router-dom';


import { reducer as formReducer } from 'redux-form/immutable'

const reducers = {
  form: formReducer,
}

const reducer = combineReducers(reducers)

export default function configureStore(initialState) {

  const logger = createLogger()
  const router = routerMiddleware(browserHistory);
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(ReduxThunk, logger, router)), reducer)

   if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers')
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}