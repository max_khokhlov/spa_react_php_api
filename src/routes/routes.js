import Login from '../components/login/Login'
import Register from '../components/login/Register'
import HomePage from '../components/HomePage'
import OrderFormPage from '../components/order/orderFormPage'
import TransportPage from '../components/transport/transportPage'
import TransportFormUpdate from '../components/transport/transportForm'
import HotelsPage from '../components/hotels/hotelsPage'
import ReservePageInfo from '../components/hotels/reservePageInfo'
import StatisticPage from '../components/statistic/statisticPage'

const base = '/manager/';

export const routes = [
    {
        isExact: true,
        path: base,
        name: 'Home',
        component: HomePage
    },
    {
        isExact: true,
        path: base+'login',
        name: 'Login',
        component: Login
    },
    {
        isExact: false,
        path: base+'order/:orderId',
        name: 'Order Form',
        component: OrderFormPage
    },
    {
        isExact: true,
        path: base+'reserve-transport/',
        name: 'Transort Reserve List',
        component: TransportPage
    },
    {
        isExact: false,
        path: base+'reserve-transport/:reserveId',
        name: 'Transort Page Reserve',
        component: TransportFormUpdate
    },
    {
        isExact: true,
        path: base+'reserve-hotel/',
        name: 'Hotels Reserve List Page ',
        component: HotelsPage
    },
    {
        isExact: true,
        path: base+'reserve-hotel/:reserveId',
        name: 'Hotels Page Reserve',
        component: ReservePageInfo
    },
    {
        isExact: true,
        path: base+'statistic/',
        name: 'Statistic Page',
        component: StatisticPage
    }
]