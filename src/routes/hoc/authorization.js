import React from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import isAuth from '../../action/login/loginAction'
import Cookies from 'universal-cookie';

const cookies = new Cookies();
const AuthHash = cookies.get('isAuthHash');


class Authorization extends React.Component {

    constructor(props) {
        super(props);
        const {dispatch} = props;
        bindActionCreators(isAuth, dispatch);

        let action = isAuth();
        dispatch(action);

        this.state = {
            isAuth: false,
        };
    }

    render() {
        if (!AuthHash) {
            let isAuth = isAuth()
            if (isAuth) {
                this.setState({ isAuth: true });
            }
        } else {
            this.setState({ isAuth: true });
        }

    }
}


export default connect( state => ({ isAuth: state.isAuth }) )(Authorization);