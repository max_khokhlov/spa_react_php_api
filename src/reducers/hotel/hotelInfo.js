import * as c from '../../constants/Hotel'

const initialState = {
    loading: false,
    request: {},
    form_data: {}
};

export default function hotelInfo(state = initialState, action) {
    switch (action.type){
        case c.GET_INFO_ROOM_LOAD:
            return {loading: true}
        case c.GET_INFO_ROOM_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_INFO_ROOM_ERROR:
            return {loading: false, error: action.errors}
        case c.GET_INFO_HOTEL_LOAD:
            return {loading: true}
        case c.GET_INFO_HOTEL_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_INFO_HOTEL_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}