import * as c from '../../constants/Login'

const initialState = {
  loading: false,
  request: {}
};

export default function login(state = initialState, action) {
  switch (action.type){
      case c.GET_LOGIN_LOAD:
        return {loading: true}
      case c.GET_LOGIN_SUCCESS:
        return {loading:false, request: action.payload, isAuth: true,  errors: null}
      case c.GET_IS_AUTH:
        return {loading:false, request: action.payload, isAuth: true}
      case c.GET_LOGIN_ERROR_HASH:
        return {loading: false, isAuth: false, errors: true }
      case c.GET_LOGIN_ERROR:
        return {loading: false, isAuth: false, error: action.errors}
      case c.GET_LOGOUT:
        return {loading: false, isAuth: false}
      default:
        return state;
  }
}