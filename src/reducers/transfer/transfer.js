import * as c from '../../constants/Transfer'

const initialState = {
    loading: false,
    request: false,
    form_data: {}
};

export default function transfer(state = initialState, action) {
    switch (action.type){
        case c.GET_TRANSFER_LOAD:
            return {loading: true}
        case c.GET_TRANSFER_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_TRANSFER_FORM_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_TRANSFER_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}