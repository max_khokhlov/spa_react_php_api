import * as c from '../../constants/People'

const initialState = {
    loading: false,
    request: {}
};

export default function login(state = initialState, action) {
    switch (action.type){
        case c.GET_PEOPLE_LOAD:
            return {loading: true}
        case c.GET_PEOPLE_ADD_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_PEOPLE_REMOVE_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_PEOPLE_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}