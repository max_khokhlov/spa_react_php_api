import * as c from '../../constants/Order'

const initialState = {
    loading: false,
    request: {},
    form_data: {}
};

export default function orderInfo(state = initialState, action) {
    switch (action.type){
        case c.GET_ORDER_INFO_LOAD:
            return {loading: true}
        case c.GET_ORDER_INFO_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_ORDER_INFO_FORM_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_ORDER_INFO_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}