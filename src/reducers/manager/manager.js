import * as c from '../../constants/Manager'

const initialState = {
    loading: false,
    request: {}
};

export default function manager(state = initialState, action) {
    switch (action.type){
        case c.GET_MANAGER_LOAD:
            return {loading: true}
        case c.GET_MANAGER_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_MANAGER_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}