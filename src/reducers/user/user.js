import * as c from '../../constants/User'

const initialState = {
  loading: false,
  user: []
};

export default function user(state = initialState, action) {
  switch (action.type){
      case c.GET_USER_LOAD:
        return {loading: true}
      case c.GET_USER_OK:
        return {loading: false, user: action.payload, errors: null }
      case c.GET_USER_ERROR:
        return {loading: false, user: null, errors: action.errors}
      default:
        return state;
  }
}