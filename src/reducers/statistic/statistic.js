import * as c from '../../constants/Statistic'

const initialState = {
    loading: false,
    request: {},
    info: false
};

export default function statistic(state = initialState, action) {
    switch (action.type){
        case c.GET_STATISTIC_LOAD:
            return {loading: true}
        case c.GET_STATISTIC_SUCCESS:
            return {loading: false, request: action.payload, errors: null}
        case c.GET_STATISTIC_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}