import * as c from '../../constants/Services'

const initialState = {
    loading: false,
    request: {}
};

export default function services(state = initialState, action) {
    switch (action.type){
        case c.GET_SERVICE_LOAD:
            return {loading: true}
        case c.GET_SERVICE_SUCCESS:
            return {loading:false, request: action.payload, errors: null}
        case c.GET_SERVICE_ERROR:
            return {loading: false, error: action.errors}
        default:
            return state;
    }
}