import {combineReducers} from 'redux'

import hotel from './hotel/hotel'

import hotelInfo from './hotel/hotelInfo'

import order from './order/order'

import orderInfo from './order/orderInfo'

import login from './login/login'

import statistic from './statistic/statistic'

import transfer from './transfer/transfer'

import transferInfo from './transfer/transferInfo'

import manager from './manager/manager'

import services from './services/services'



export default combineReducers({
  order, login, hotel, transfer, transferInfo, hotelInfo, statistic, manager, orderInfo, services
})