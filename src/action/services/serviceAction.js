import * as c from '../../constants/Services'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?services=';

export function deleteService(data) {

    return dispatch => {

        dispatch({type: c.GET_SERVICE_LOAD});

        fetch(
            url+'deleteService',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_SERVICE_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_SERVICE_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function addService(data) {

    return dispatch => {

        dispatch({type: c.GET_SERVICE_LOAD});

        fetch(
            url+'addService',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_SERVICE_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_SERVICE_ERROR, errors: responce.statusText})
                }
            })
    }

}
