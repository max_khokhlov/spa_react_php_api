import * as c from '../../constants/User'

export function getUser() {
    return dispatch => {
        dispatch({type: c.GET_USER_LOAD})
        fetch('http://localhost:8000/api/v0/users/?format=json', { method: 'GET'}).then((responce) => {
            if (responce.status == 200){
                responce.json().then(function(data) {
                    console.log(data);
                    dispatch(UserData(data))
                });
            }else{
                dispatch({type: c.GET_USER_ERROR, errors: responce.statusText})
            }
        })
    }
}

function UserData(data) {
    return{
        type: c.GET_USER_OK,
        payload: data
    }
}