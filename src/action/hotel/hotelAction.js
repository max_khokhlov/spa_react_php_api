import * as c from '../../constants/Hotel'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?hotel=';

export function getRoomByHotelID(data) {

    return dispatch => {

        dispatch({type: c.GET_ROOM_LOAD});

        fetch(
            url+'getRoomByHotelID',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ROOM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ROOM_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getReserveList(data) {

    return dispatch => {

        dispatch({type: c.GET_ROOM_LOAD});

        fetch(
            url+'getReserveList',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ROOM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ROOM_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getReserve(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'getReserve',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function deleteHotelforDay(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'deleteHotelforDay',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function deleteReserveHotelforDay(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'deleteReserveHotelforDay',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function addReserveHotel(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'addReserveHotel',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function addReserve(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'addReserve',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }

}
export function updateReserve(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'updateReserve',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function deleteReserve(data) {

    return dispatch => {

        dispatch({type: c.GET_HOTEL_LOAD});

        fetch(
            url+'deleteReserve',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }

}