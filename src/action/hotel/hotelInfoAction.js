import * as c from '../../constants/Hotel'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?hotel=';

export function getHotelInfo(data) {

    return dispatch => {

        dispatch({type: c.GET_INFO_HOTEL_LOAD});

        fetch(
            url+'getHotelInfo',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_INFO_HOTEL_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_INFO_HOTEL_ERROR, errors: responce.statusText})
                }
            })
    }
}
