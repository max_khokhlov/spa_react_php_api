import * as c from '../../constants/Statistic'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?statistic=';

export function getTables(data) {

    return dispatch => {

        dispatch({type: c.GET_STATISTIC_LOAD});

        fetch(
            url+'getTables',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_STATISTIC_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_STATISTIC_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getData(data) {

    return dispatch => {

        dispatch({type: c.GET_STATISTIC_LOAD});

        fetch(
            url+'getData',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_STATISTIC_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_STATISTIC_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getFilterJoin(data) {

    return dispatch => {

        dispatch({type: c.GET_STATISTIC_LOAD});

        fetch(
            url+'getFilterJoin',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_STATISTIC_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_STATISTIC_ERROR, errors: responce.statusText})
                }
            })
    }
}
