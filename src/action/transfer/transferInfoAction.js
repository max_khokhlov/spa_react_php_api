import * as c from '../../constants/Transfer'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?transfer=';


export function getTypeTransport(data) {

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_INFO_LOAD});

        fetch(
            url+'getTypeTransport',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_INFO_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_INFO_ERROR, errors: responce.statusText})
                }
            })
    }
}