import * as c from '../../constants/Transfer'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?transfer=';

export function getTransfersList(data) {

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'getTransfersList',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)

            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function getTransfer(data) {

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'getTransfer',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getTypeTransport(data) {

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'getTypeTransport',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }
}


export function updateReserveTransfer(data) {

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'updateReserveTransfer',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                       dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }
}


export function addReserveTransfer(data){

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'addReserveTransfer',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }

}
export function addReserve(data){

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'addReserve',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }

}



export function deleteReserveTransfer(data){

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'deleteReserveTransfer',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function deleteReserve(data){

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'deleteReserve',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }

}

export function getTransferByCityID(data) {

    return dispatch => {

        dispatch({type: c.GET_TRANSFER_LOAD});

        fetch(
            url+'getTransferByCityID',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_TRANSFER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_TRANSFER_ERROR, errors: responce.statusText})
                }
            })
    }

}