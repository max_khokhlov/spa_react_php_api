import * as c from '../../constants/Login'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?bitrix=';
const cookies = new Cookies();
const AuthHash = cookies.get('isAuthHash');
const userLogin = cookies.get('userLogin');
const date = new Date(Date.now() + 1200 * 1000);

export function login(data) {
    return dispatch => {

        dispatch({type: c.GET_LOGIN_LOAD});

        fetch(
            url+'login',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
            if (responce.status == 200){
                responce.json().then(function(data) {
                    if (data.result.status == 'error'){
                        cookies.remove('isAuthHash', { path: "/" })
                        dispatch({type: c.GET_LOGIN_ERROR, errors: data.result.errorText})
                    }else{
                        cookies.set('isAuthHash', data.result.login_hash, { path: "/", expires: date})
                        cookies.set('userLogin', data.result.user_login, { path: "/", expires: date})
                        dispatch(loginData(data))
                    }
                });
            }else{
                dispatch({type: c.GET_LOGIN_ERROR, errors: responce.statusText})
            }
        })
    }
}

export function isAuth() {

    let data = {
        "login_hash": AuthHash,
        "login": userLogin
    }

    return dispatch => {

        dispatch({type: c.GET_LOGIN_LOAD});

        fetch(
            url+'isAuth',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        if (data.result.status == 'error'){
                            cookies.remove('isAuthHash', { path: "/" })
                            dispatch({type: c.GET_LOGIN_ERROR_HASH})
                        }else{
                            cookies.set('isAuthHash', data.result.login_hash, { path: "/", expires: date})
                            dispatch({type: c.GET_IS_AUTH, payload: data })
                        }
                    });
                }else{
                    return false;
                }
        })

    }

}

export function logout() {

    let data = {
        "login_hash": AuthHash,
        "login": userLogin
    }

    return dispatch => {

        dispatch({type: c.GET_LOGIN_LOAD});

        fetch(
        url+'logout',
        {
          method: 'POST',
          headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
          body: 'fields='+JSON.stringify(data)
        })
        .then((responce) => {
           if (responce.status == 200){
              responce.json().then(function(data) {
                  cookies.remove('isAuthHash', { path: "/" })
                  dispatch({type: c.GET_LOGOUT})
              });
           }else{
             return false;
           }
        })
    }
}


function loginData(data) {
    return{
        type: c.GET_LOGIN_SUCCESS,
        payload: data
    }
}