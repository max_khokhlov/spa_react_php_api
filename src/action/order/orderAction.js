import * as c from '../../constants/Order'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?order=';

export function getOrders(data) {

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'getOrders',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
            if (responce.status == 200){
                responce.json().then(function(data) {
                    dispatch(loginData(data))
                });
            }else{
                dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
            }
        })
    }
}

export function getOrderData(data) {

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'getOrderData',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_FORM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getRoomByHotelID(data) {

    return dispatch => {

        dispatch({type: c.GET_ROOM_LOAD});

        fetch(
            url+'getRoomByHotelID',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ROOM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ROOM_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function updateOrder(data) {

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'updateOrder',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_FORM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
                }
            })

    }

}

export function addDay(data){

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'addDay',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_FORM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
                }
            })


    }

}

export function deleteDay(data){

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'deleteDay',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_FORM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
                }
            })


    }

}

export function addOrder(data){

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'addOrder',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_FORM_SUCCESS, payload: data })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
                }
            })


    }

}

export function deleteOrder(data){

    return dispatch => {

        dispatch({type: c.GET_ORDER_LOAD});

        fetch(
            url+'deleteOrder',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_FORM_SUCCESS, payload: data })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_ERROR, errors: responce.statusText})
                }
            })


    }

}

function loginData(data) {
    return{
        type: c.GET_ORDER_SUCCESS,
        payload: data
    }
}