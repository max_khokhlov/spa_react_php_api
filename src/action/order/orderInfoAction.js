import * as c from '../../constants/Order'
import * as l from '../../constants/Link'

const url = l.BASE_URL+'?order=';

export function getInfo(data) {

    return dispatch => {

        dispatch({type: c.GET_ORDER_INFO_LOAD});

        fetch(
            url+'getInfo',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_ORDER_INFO_FORM_SUCCESS, payload: data.result })
                    });
                }else{
                    dispatch({type: c.GET_ORDER_INFO_ERROR, errors: responce.statusText})
                }
            })
    }
}
