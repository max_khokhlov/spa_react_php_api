import * as c from '../../constants/People'
import * as l from '../../constants/Link'
import Cookies from 'universal-cookie';

const url = l.BASE_URL+'?people=';

export function addPeople(data) {

    return dispatch => {

        dispatch({type: c.GET_PEOPLE_LOAD});

        fetch(
            url+'addPeople',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_PEOPLE_ADD_SUCCESS, payload: data })
                    });
                }else{
                    dispatch({type: c.GET_PEOPLE_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function getPeople(data) {

    return dispatch => {

        dispatch({type: c.GET_PEOPLE_LOAD});

        fetch(
            url+'getPeople',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_PEOPLE_SUCCESS, payload: data })
                    });
                }else{
                    dispatch({type: c.GET_PEOPLE_ERROR, errors: responce.statusText})
                }
            })
    }
}

export function removePeopleToOrder(data) {

    return dispatch => {

        dispatch({type: c.GET_PEOPLE_LOAD});

        fetch(
            url+'deletePeopleToOrder',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({ type: c.GET_PEOPLE_REMOVE_SUCCESS, payload: data })
                    });
                }else{
                    dispatch({type: c.GET_PEOPLE_ERROR, errors: responce.statusText})
                }
            })
    }
}

