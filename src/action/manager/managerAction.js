import * as c from '../../constants/Manager'
import * as l from '../../constants/Link'

const url = l.BASE_URL+'?manager=';

export function getManagerList(data) {

    return dispatch => {

        dispatch({type: c.GET_MANAGER_LOAD});

        fetch(
            url+'getManagerList',
            {
                method: 'POST',
                headers: {"Content-type": "application/x-www-form-urlencoded; charset=UTF-8"},
                body: 'fields='+JSON.stringify(data)
            })
            .then((responce) => {
                if (responce.status == 200){
                    responce.json().then(function(data) {
                        dispatch({type: c.GET_MANAGER_SUCCESS, payload: data.result})
                    });
                }else{
                    dispatch({type: c.GET_MANAGER_ERROR, errors: responce.statusText})
                }
            })

    }

}
