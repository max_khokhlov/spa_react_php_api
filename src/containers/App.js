import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import {connect} from 'react-redux'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import {routes} from '../routes/routes'
import { Layout, Breadcrumb, Icon, Spin, Affix } from 'antd';
import Cookies from 'universal-cookie';
import createHistory from "history/createBrowserHistory"

import * as loginAction from '../action/login/loginAction'

import Login from '../components/login/Login'
import NotFound from '../components/NotFound'

const cookies = new Cookies();
const AuthHash = cookies.get('isAuthHash');

const antIcon = <Icon type="loading" style={{ fontSize: 52 }} spin />;

class App extends Component {


    constructor(props) {

        super(props);
        const {dispatch} = this.props;
        const history = createHistory();

        this.state = {
            pathname: history.location.pathname
        }

        bindActionCreators(loginAction , dispatch);
        let action = loginAction.isAuth();

        dispatch(action);

    }

    render(){


        const {loginState} = this.props;
        let output = "";

        if (typeof loginState.isAuth === 'undefined'){
             output = <div className="loading-home">
                <Spin indicator={antIcon} />
            </div>
        }else{
            if (loginState.isAuth === false){
                output = <div>
                    <Switch>
                        <Route path="/manager/login" exact="true" key="login" render={ () => { return <Login pathname={this.state.pathname} /> }} />
                        <Redirect to="/manager/login" />
                        <Route component={NotFound} />
                    </Switch>
                </div>
            }else{

                output = <div>
                    <Switch>
                        { routes.map(route => {
                            return <Route path={route.path} exact={route.isExact} key={route.path} component={route.component}/>
                        })}
                        <Route component={NotFound} />
                    </Switch>
                </div>
            }

        }

        return output;


    }
}

function mapStateToProps(state){
    return {
        loginState: state.login
    }
}

// export default hot(module)(App)
export default withRouter(connect(mapStateToProps)(App))