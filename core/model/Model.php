<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

abstract class Model{
    
    protected $db;
    
    function __construct()
    {
        try {
            $this->db = new DB_MySQL(DB_HOST, DB_USER, DB_PASS, DB);
        } catch (Exception $e) {
            echo "Ошибка базы данных";
        }
        
    }

}

?>