<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_ReserveTransport extends Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function addTransport($data){

        $this->db->query('INSERT INTO `reserve_transport` SET `transport_name`= "'.$data['transport_name'].'", `type` = "'.$data['type'].'", `date_start` = "'.$data['date_start'].'", `date_finish` = "'.$data['date_finish'].'", `city_start` = "'.$data['city_start'].'", `city_finish` = "'.$data['city_finish'].'", `status` = "active"');

        $transport_id = $this->db->getLastId();

        return $transport_id;

    }

    public function addReservePlace($data){

        $this->db->query('INSERT INTO `reserve_place` SET `reserve_transport_id`= "'.$data['reserve_transport_id'].'", `reserve_type_place_id` = "'.$data['reserve_type_place_id'].'", `number_place` = "'.$data['number_place'].'", `status` = "'.$data['status'].'"');

        $place_id = $this->db->getLastId();

        return $place_id;

    }

    public function addPlaceType($data){

        $this->db->query('INSERT INTO `reserve_type_place` SET `place_name`= "'.$data['place_name'].'", `status` = "'.$data['status'].'"');

        $type_id = $this->db->getLastId();

        return $type_id;

    }

    public function addReserveTransferPeople($data){

        $this->db->query('INSERT INTO `reserve_transport_people` SET `order_id`= "'.$data['order_id'].'", `people_order_id`= "'.$data['people_order_id'].'", `reserve_place_id` = "'.$data['reserve_place_id'].'", `is_reserve` = "'.$data['is_reserve'].'", `city_start` = "'.$data['city_start'].'", `city_finish` = "'.$data['city_finish'].'", `transport_type_id` = "'.$data['transport_type_id'].'",  `date_reserve`= "'.$data['date_reserve'].'", `type_place_id` = "'.$data['type_place_id'].'", `number_place` = "'.$data['number_place'].'",  `comment`= "'.$data['comment'].'", `date_created`= NOW(), `active` = 1, `price` = "'.$data['price'].'"');

        $reserve_id = $this->db->getLastId();

        return $reserve_id;

    }

    public function getListPlaceType(){

        $result = $this->db->query("SELECT * FROM `reserve_type_place` WHERE `status` = 'active' ");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getListPlaceTypeFilter(){

        $output = array();
        $result = $this->db->query("SELECT * FROM `reserve_type_place` WHERE `status` = 'active' ");

        if ($result->rows && count($result->rows) > 0){
            foreach ($result->rows as $row){
                $output[$row['reserve_type_place_id']] = array(
                    'name' => $row['place_name'],
                    'id' => $row['reserve_type_place_id']
                );
            }

            return $output;

        }else{
            return false;
        }

    }

    public function getListTypeTransport(){

        $result = $this->db->query("SELECT * FROM `transport_type` ");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getListTypeTransportFilter(){

        $output = array();
        $result = $this->db->query("SELECT * FROM `transport_type` ");

        if ($result->rows && count($result->rows) > 0){

            foreach ($result->rows as $row){

                $output[$row['transport_type_id']] = array(
                    'name' => $row['transport_type_name'],
                    'id' => $row['transport_type_id']
                );

            }

            return $output;

        }else{
            return false;
        }

    }

    public function getListReserveTransportFilter(){

        $output = array();
        $result = $this->db->query("SELECT * FROM `reserve_transport` ");

        if ($result->rows && count($result->rows) > 0){

            foreach ($result->rows as $row){

                $output[$row['reserve_transport_id']] = array(
                    'name' => $row['transport_name'],
                    'id' => $row['reserve_transport_id']
                );

            }

            return $output;

        }else{
            return false;
        }

    }

    public function getTypeTransportByID($type_id){

        $result = $this->db->query("SELECT * FROM `transport_type` WHERE `transport_type_id` =".$type_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getTransportByID($transport_id){

        $result = $this->db->query("SELECT * FROM `reserve_transport` WHERE `reserve_transport_id` =".$transport_id." ;");

        if ($result->row && count($result->row) > 0){
            return $result->row;
        }else{
            return false;
        }

    }

    public function getReserveTransportList(){

        $result = $this->db->query("SELECT * FROM `reserve_transport` WHERE `status` != 'delete';");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getTransportByCityAndDate($city_start, $city_finish, $date_start){

        $result = $this->db->query("SELECT * FROM `reserve_transport` WHERE `city_start` =".$city_start." AND `city_finish`=".$city_finish." AND `date_start` <= '".$date_start."' AND `date_finish` >= '".$date_start."' ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getPlaceByTransportId($transport_id){

        $result = $this->db->query("SELECT * FROM `reserve_place` WHERE `reserve_transport_id` =".$transport_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getPlaceByType($type_place){

        $result = $this->db->query("SELECT * FROM `reserve_type_place_id` WHERE `reserve_transport_id` =".$type_place." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getPlaceByID($reserve_transport_id){

        $result = $this->db->query("SELECT * FROM `reserve_place` WHERE `reserve_transport_id` =".$reserve_transport_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getTypePlaceByID($type_place_id){

        $result = $this->db->query("SELECT * FROM `reserve_type_place` WHERE `reserve_type_place_id` =".$type_place_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getReserveByCityID($city_id){

        $result = $this->db->query("SELECT * FROM `reserve_transport` WHERE `city_start` =".$city_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getReserveOrderByID($reserve_order_id){

        $result = $this->db->query("SELECT * FROM `reserve_transport_people` WHERE `reserve_transport_people_id` =".$reserve_order_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function checkReservePlace($place, $date){

        $result = $this->db->query("SELECT * FROM `reserve_transport_people` WHERE `reserve_place_id` =".$place." AND `date_reserve` = '".$date."';");

        if ($result->rows && count($result->rows) > 0){
            return true;
        }else{
            return false;
        }

    }

    public function getReserveTransportByOrderID($order_id){

        $result = $this->db->query("SELECT * FROM `reserve_transport_people` WHERE `order_id` =".$order_id." ;");

        if ($result->rows && count($result->rows) > 0){

            $index = 0;
            $output = array();

            foreach ($result->rows as $row){

                if ($row['is_reserve'] == 1){

                    $output[$index] = array(
                        'reserve_transport_people_id' => $row['reserve_transport_people_id'],
                        'order_id' => $row['order_id'],
                        'people_order_id' => $row['people_order_id'],
                        'reserve' => true,
                        'date_reserve' => $row['date_reserve'],
                        'price' => $row['price'],
                        'comment' => $row['comment']
                    );

                    $reserve = $this->db->query("SELECT * FROM `reserve_place` WHERE `reserve_place_id` = ".$row['reserve_place_id']." ;");

                    if ($reserve->row && isset($reserve->row['status']) && $reserve->row['status'] == 'active'){

                        $output[$index]['number_place'] = $reserve->row['number_place'];

                        $reserve_transport = $this->db->query("SELECT * FROM `reserve_transport` WHERE `reserve_transport_id` = ".$reserve->row['reserve_transport_id']." ;");


                        if (isset($reserve_transport->row['reserve_transport_id'])){

                            $output[$index]['transport_name'] = $reserve_transport->row['transport_name'];
                            $output[$index]['city_start'] = $reserve_transport->row['city_start'];
                            $output[$index]['city_finish'] = $reserve_transport->row['city_finish'];

                            $transport_type = $this->db->query("SELECT * FROM `transport_type` WHERE `transport_type_id` = ".$reserve_transport->row['type']." ;");

                            if (isset($transport_type->row['transport_type_name'])){
                                $output[$index]['transport_type_name'] = $transport_type->row['transport_type_name'];
                            }else{
                                return false;
                            }

                        }



                        $place_type = $this->db->query("SELECT * FROM `reserve_type_place` WHERE `reserve_type_place_id` = ".$reserve->row['reserve_type_place_id']." ;");

                        if (isset($place_type->row['place_name'])){
                            $output[$index]['place_name'] = $place_type->row['place_name'];
                        }else{
                            return false;
                        }

                    }else{
                        return false;
                    }

                }else{

                    $output[$index] = array(
                        'reserve_transport_people_id' => $row['reserve_transport_people_id'],
                        'order_id' => $row['order_id'],
                        'people_order_id' => $row['people_order_id'],
                        'reserve' => false,
                        'date_reserve' => $row['date_reserve'],
                        'price' => $row['price'],
                        'number_place' => $row['number_place'],
                        'city_start' => $row['city_start'],
                        'city_finish' => $row['city_finish'],
                        'comment' => $row['comment']
                    );

                    $transport_type = $this->db->query("SELECT * FROM `transport_type` WHERE `transport_type_id` = ".$row['transport_type_id']." ;");

                    if (isset($transport_type->row['transport_type_name'])){
                        $output[$index]['transport_type_name'] = $transport_type->row['transport_type_name'];
                    }else{
                        return false;
                    }

                    $place_type = $this->db->query("SELECT * FROM `reserve_type_place` WHERE `reserve_type_place_id` = ".$row['type_place_id']." ;");

                    if (isset($place_type->row['place_name'])){
                        $output[$index]['place_name'] = $place_type->row['place_name'];
                    }else{
                        return false;
                    }


                }

                $index++;

            }


            return $output;

        }else{
            return false;
        }

    }

    public function getReserveOrderBy($data, $order_by, $page, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
                'reserve_transport_id' => 'DESC'
            );
        }

        $sort = 'reserve_transport_id DESC';

        if (count($order_by) > 0){
            foreach ($order_by as $key => $value){
                $sort = ' '.$key.' '.$value.' ';
            }
        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){

                $not = strpos($value, '!=');

                if (count($data) > $i + 1){
                    if ($not === false){
                        $where .= " `".$key."`= '".$value."' AND";
                    }else{
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' AND";
                    }
                }else{
                    if ($not === false){
                        $where .= " `".$key."`= '".$value."' ";
                    }else{
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' ";
                    }
                }
                $i++;
            }
        }


        $result = $this->db->query("SELECT * FROM `reserve_transport` WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else {
            return false;
        }

    }

    public function getCountReserve(){
        
        $result = $this->db->query("SELECT count(reserve_transport_id) AS count_reserve FROM `reserve_transport` WHERE `status` != 'delete' ");
        
        if (isset($result->row)){
            return $result->row['count_reserve'];
        }else{
            return false;
        }
        
    }
    
    public function updateTransport($transport_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_transport` WHERE `reserve_transport_id` = ".(int)$transport_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){
            
            $set = '';
            $i = 0;

            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set .= " `".$key."` = '".$value."' ,";
                }else{
                    $set .= " `".$key."` = '".$value."' ";
                }
                $i++;
            }


            $this->db->query("UPDATE `reserve_transport` SET ".$set." WHERE `reserve_transport_id`=".(int)$transport_id." ;");

            return true;

        }else{
            return false;
        }

    }

    public function updateTransportPlace($transport_place_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_place` WHERE `reserve_place_id` = ".(int)$transport_place_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `reserve_place` SET ".$set." WHERE `reserve_place_id`=".(int)$transport_place_id." ;");

            return $this->getPlaceByID($transport_place_id);

        }else{
            return false;
        }

    }

    public function updateTypePlace($type_place_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_type_place` WHERE `reserve_type_place_id` = ".(int)$type_place_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `reserve_type_place` SET ".$set." WHERE `reserve_type_place_id`=".(int)$type_place_id." ;");

            return $this->getTypePlaceByID($type_place_id);

        }else{
            return false;
        }

    }

    public function updateReserveOrder($reserve_transport_people_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_transport_people` WHERE `reserve_transport_people_id` = ".(int)$reserve_transport_people_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `reserve_transport_people` SET ".$set." WHERE `reserve_transport_people_id`=".(int)$reserve_transport_people_id." ;");

            return $this->getReserveOrderByID($reserve_transport_people_id);

        }else{
            return false;
        }

    }

    public function deleteReserveTransport($transport_id){

        $data = array(
            'status' => 'delete'
        );


        $this->updateTransport($transport_id, $data);

        return true;

    }

    public function deletePlace($place_id){

        $data = array(
            'status' => 'delete'
        );

        $this->updateTransportPlace($place_id, $data);

    }

    public function deleteTypePlace($type_place_id){

        $data = array(
            'status' => 'delete'
        );

        $this->updateTypePlace($type_place_id, $data);

    }

    public function deletePlaceReserve($data){

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $where .= " `".$key."`= '".$value."' AND";
                }else{
                    $where .= " `".$key."`= '".$value."' ";
                }
                $i++;
            }
        }


        $this->db->query("DELETE FROM reserve_place WHERE ".$where.";");

        return true;

    }

    public function deleteReserveOrder($reserve_order_id){

       $this->db->query("DELETE FROM reserve_transport_people WHERE `reserve_transport_people_id` = ".$reserve_order_id.";");

       return true;

    }

}