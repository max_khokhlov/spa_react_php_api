<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';


class model_Order extends Model{

    public $bitrix; 
    
    function __construct()
    {
        parent::__construct();
        $this->bitrix = new model_Bitrix();
    }

    public function addOrder($data){

        $this->db->query('INSERT INTO `order` SET `order_bitrix_id` = '.$data['orderBitrixId'].' , `tour_bitrix_id` = '.$data['tour_bitrix_id'].' , `is_tour` = "'.$data['is_tour'].'", `name` = "'.$data['orderName'].'" , `date_order` = "'.$data['orderDate'].'" , `date_start` = "'.$data['orderDateStart'].'" , `date_finish` = "'.$data['orderDateFinish'].'" , `manager_id` = "'.$data['managerId'].'" , `status` = "'.$data['orderStatus'].'" , `date_created` = NOW() ,  `total` = "'.$data['orderTotal'].'" , `comment` = "'.$data['comment'].'";');

        $orderId = $this->db->getLastId();

        if (isset($data['days']) && count($data['days']) > 0){

            $this->addOrderDay($orderId, $data);

        }

        return $orderId;

    }

    public function getCountOrders($manager_id = false){

       if ($manager_id){
           $count = $this->db->query("SELECT COUNT(order_id) as count_orders FROM `order` WHERE `manager_id` = {$manager_id} AND `status` != 'delete'");
       }else{
           $count = $this->db->query("SELECT COUNT(order_id) as count_orders FROM `order` WHERE `status` != 'delete'");
       }


       if (isset($count->row['count_orders'])){
           return $count->row['count_orders'];
       }else{
           return false;
       }

    }

    public function addOrderDay($orderId, $data){

        if (isset($data['days']) && count($data['days']) > 0){
            
            foreach ($data['days'] as $day){
                
                $day = (array)$day;
                
                $this->db->query('INSERT INTO `order_day` SET `order_id` = "'.$orderId.'", `number_day` = "'.$day['number'].'", `city_id` = '.$day['city_id'].', `date_start` = "'.$day['date_start'].'", `date_finish` = "'.$day['date_finish'].'" ;');
            }

            return true;

        }else{
            return false;
        }

    }

    public function removeOrderDay($orderDayId){

        $reserve_hotel = $this->db->query("SELECT * FROM `reserve_hotel_people` WHERE `order_day_id` = ".$orderDayId.";");


        if (count($reserve_hotel->rows) > 0){

            foreach ($reserve_hotel->rows as $row){

                $this->db->query("DELETE FROM `hotel_service` WHERE `hotel_reserve_id` = {$row['reserve_id']} AND `status` = 'reserve';");

            }

        }

        $hotel = $this->db->query("SELECT * FROM `hotel_people` WHERE `order_day_id` = ".$orderDayId.";");

        if (count($hotel->rows) > 0){

            foreach ($hotel->rows as $row){

                $this->db->query("DELETE FROM `hotel_service` WHERE `hotel_reserve_id` = {$row['reserve_id']} AND `status` = 'hotel';");

            }

        }

        $this->db->query("DELETE FROM `reserve_hotel_people` WHERE `order_day_id` = ".$orderDayId.";");
        $this->db->query("DELETE FROM `hotel_people` WHERE `order_day_id` = ".$orderDayId.";");
        $this->db->query("DELETE FROM `order_day` WHERE `order_day_id` = ".$orderDayId.";");

        return true;

    }

    public function removeTransfer($order_id){

        $this->db->query("DELETE FROM `reserve_transport_people` WHERE `order_id` = {$order_id}");

    }
    
    public function getOrderIDByBitrixOrderID($bitrix_order_id){

        $data = $this->db->query("SELECT order_id FROM `order` WHERE `order_bitrix_id` = ".(int)$bitrix_order_id.";");
        
        if (isset($data->row['order_id'])){
            
            return $data->row['order_id'];
            
        }
        
    }

    public function getOrder($orderId){

        $data = $this->db->query("SELECT * FROM `order` WHERE `order_id` = ".(int)$orderId.";");

        if (isset($data->row) && count($data->row) > 0){

            $data = $data->row;

            $days = $this->db->query("SELECT * FROM `order_day` WHERE `order_id` = ".$orderId.";");

            if (isset($days->rows) && count($days->rows) > 0){
                $data['days'] = $days->rows;
                $i = 0;

                foreach ($days->rows as $day){

                    $hotelArr = array();
                    $reserveArr = array();
                    $hotelSort = array();
                    $reserveSort = array();

                    $hotel = $this->db->query("SELECT * FROM `hotel_people` WHERE `order_day_id` = ".$day['order_day_id']." AND `status` = 'active';");
                    
                    if (count($hotel->rows) > 0){

                        $id_services = array_column($hotel->rows, 'hotel_people_id');
                        
                        foreach ($hotel->rows as $hotelItem){
                            
                            $service = $this->db->query("SELECT * FROM `hotel_service` WHERE `hotel_reserve_id`= '".$hotelItem['reserve_id']."' AND reserve_hotel_id IN (".implode(",", $id_services).") AND `status` = 'hotel';");
                            
                            if (count($service->rows) > 0){
                                foreach ($service->rows as $service_item){
                                    $hotelItem['services'][] = array(
                                        'service_id' => $service_item['service_id'],
                                        'comment' => $service_item['comment']
                                    );
                                }
                            }
                            
                            $roomInfo = $this->bitrix->getRoomByID($hotelItem['room_id']);
                            $capacity = $roomInfo[$hotelItem['room_id']]['type']['capacity'];
                            
                            $h_key = $hotelItem['hotel_id']."|".$hotelItem['room_id']."|".$hotelItem['date_reserve_start']."|".$hotelItem['date_reserve_finish'];
                            $hotelItem['reserve'] = false;
                            $people = $hotelItem['people_order_id'];
                            unset($hotelItem['people_order_id']);

                            if (in_array($h_key, $hotelSort)){
                                $search = array_search($h_key, $hotelSort);
                                if (count($hotelArr[$search]['peoples']) < $capacity){
                                    $hotelArr[$search]['peoples'][] = $people;
                                }else{
                                    $hotelSort[] = $h_key;
                                    $hotelItem['peoples'][] = $people;
                                    $hotelArr[] = $hotelItem;
                                }
                            }else{
                                $hotelSort[] = $h_key;
                                $hotelItem['peoples'][] = $people;
                                $hotelArr[] = $hotelItem;
                            }

                        }
                    }

                    $reserve_hotel = $this->db->query("SELECT * FROM `reserve_hotel_people` rhp LEFT JOIN `reserve_hotel` rh ON rhp.reserve_hotel_id = rh.reserve_hotel_id WHERE rhp.order_day_id = ".$day['order_day_id']."  AND rhp.status = 'active';");

                    if (count($reserve_hotel->rows) > 0){

                        $id_services = array_column($reserve_hotel->rows, 'reserve_hotel_people_id');
                        
                        foreach ($reserve_hotel->rows as $reserve){

                            $service = $this->db->query("SELECT * FROM `hotel_service` WHERE `hotel_reserve_id`= '".$reserve['reserve_id']."' AND reserve_hotel_id IN (".implode(",", $id_services).") AND `status` = 'reserve';");

                            if (isset($service->rows) && count($service->rows) > 0){

                                foreach ($service->rows as $service_item){
                                    $reserve['services'][] = array(
                                        'service_id' => $service_item['service_id'],
                                        'comment' => $service_item['comment']
                                    );
                                }

                            }

                            $roomInfo = $this->bitrix->getRoomByID($reserve['room_id']);
                            $capacity = $roomInfo[$reserve['room_id']]['type']['capacity'];

                            $h_key = $reserve['hotel_id']."|".$reserve['room_id']."|".$reserve['date_reserve_start']."|".$reserve['date_reserve_finish'];
                            $reserve['reserve'] = true;
                            $people = $reserve['people_order_id'];
                            unset($reserve['people_order_id']);

                            if (in_array($h_key, $reserveSort)){
                                $search = array_search($h_key, $reserveSort);
                                if (count($hotelArr[$search]['peoples']) < $capacity){
                                    $reserveArr[$search]['peoples'][] = $people;
                                }else{
                                    $reserveSort[] = $h_key;
                                    $reserve['peoples'][] = $people;
                                    $reserveArr[] = $reserve;
                                }
                            }else{
                                $reserveSort[] = $h_key;
                                $reserve['peoples'][] = $people;
                                $reserveArr[] = $reserve;
                            }

                        }
                    }
                    
                    $data['days'][$i]['hotels'] = array_merge($hotelArr, $reserveArr);

                    $i++;
                }
            }else{
                $data['days'] = false;
            }


            return $data;

        }else{
            return false;
        }

    }

    public function getListOrderBy($data, $order_by = array(),$page = 0, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
              'order_id' => 'DESC'
            );
        }

        $sort = 'order_id DESC';

        if (count($order_by) > 0){
            foreach ($order_by as $key => $value){
                $sort = ' '.$key.' '.$value.' ';
            }
        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            
            foreach ($data as $key => $value){

                if (!is_array($value)){
                    $not = strpos($value, '!=');
                }
                
                if (count($data) > $i + 1){
                    if ($not === false) {
                        if (is_array($value)) {
                            $value = implode(",", $value);
                            $where .= " (`" . $key . "` IN (" . $value . ") ) AND";
                        }else if ($key == 'date_start'){
                            $where .= " `date_created`>= '".$value." 00:00:00' AND";
                        }else if ($key=='date_finish'){
                            $where .= " `date_created`<= '".$value." 23:59:59' AND";
                        }else if ($key == 'manager_id'){
                            $where .= " (`".$key."`= '".$value."' OR `".$key."`= '0') AND";
                        }else if ($key == 'manager'){
                            $where .= " `manager_id`= '".$value."' AND";
                        }else{
                            $where .= " `".$key."`= '".$value."' AND";
                        }
                    }else{

                        if (is_array($value)) {
                            $value = implode(",", $value);
                            $where .= " (`" . $key . "` IN (" . $value . ") ) AND";
                        }else{
                            $value = str_replace('!=', '', $value);
                            $where .= " `".$key."`!= '".$value."' AND";
                        }
                        
                    }
                }else{
                    if ($not === false){
                        if (is_array($value)) {
                            $value = implode(",", $value);
                            $where .= " (`" . $key . "` IN (" . $value . ") ) ";
                        }else if ($key == 'date_start'){
                            $where .= " `date_created`>= '".$value."' ";
                        }else if ($key=='date_finish'){
                            $where .= " `date_created`<= '".$value."' ";
                        }else if ($key == 'manager_id'){
                            $where .= " (`".$key."`= '".$value."' OR `".$key."`= '0') ";
                        }else if ($key == 'manager'){
                            $where .= " `manager_id`= '".$value."' ";
                        }else{
                            $where .= " `".$key."`= '".$value."' ";
                        }
                    }else{
                        if (is_array($value)) {
                            $value = implode(",", $value);
                            $where .= " (`" . $key . "` IN (" . $value . ") ) ";
                        }else{
                            $value = str_replace('!=', '', $value);
                            $where .= " `".$key."`!= '".$value."' ";
                        }
                    }
                }
                $i++;
            }
        }
        
        $result = $this->db->query("SELECT * FROM `order` WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else { 
            return false;
        }

    }

    public function orderUpdate($orderId, $data){

        $result = $this->db->query("SELECT * FROM `order` WHERE `order_id` = ".(int)$orderId.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            $set = '';


            foreach ($data as $key => $value){

                if (count($data) > ($i + 1)){
                    $set .= " `".$key."` = '".$value."' ,";
                }else{
                    $set .= " `".$key."` = '".$value."' ";
                }
                $i++;
            }


            $this->db->query("UPDATE `order` SET ".$set." WHERE `order_id`=".$orderId." ;");

            return $this->getOrder($orderId);

        }else{
            return false;
        }

    }

    public function getBitrixOrderIDByOrderID($order_id){

        $result = $this->db->query("SELECT `order_bitrix_id` FROM `order` WHERE `order_id` = {$order_id}");
        
        if (count($result->rows) > 0){
            
           return $result->row['order_bitrix_id'];
            
        }else{
            
            return false;
            
        }

    }
    

    public function deleteOrder($orderId){

        $data = array(
            'status' => 'delete'
        );

        $days = $this->db->query("SELECT `order_day_id` FROM `order_day` WHERE `order_id` = {$orderId}");

        if (count($days->rows) > 0){

            foreach ($days->rows as $day){

                $this->removeOrderDay($day['order_day_id']);

            }

        }

        $this->removeTransfer($orderId);
        $this->orderUpdate($orderId, $data);

        return true;

    }


}