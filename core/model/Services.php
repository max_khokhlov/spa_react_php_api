<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_Services extends Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    function addServices($data){
        
        $this->db->query("INSERT INTO order_services SET services_id = {$data['service_id']}, people_order_id = {$data['people']}, order_id = {$data['order_id']}");

        $last_id = $this->db->getLastId();
        
        return $last_id;
        
    }

    function getServices($order_id, $people_order_id){

        $result = $this->db->query("SELECT * FROM order_services WHERE order_id = {$order_id} AND people_order_id = {$people_order_id};");

        if (count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    function deleteServices($order_id, $service_id ,$people_order_id){
        
        $this->db->query("DELETE FROM order_services WHERE order_id = {$order_id} AND people_order_id = {$people_order_id} AND services_id = {$service_id}; ");

        return true;
        
    }
    
}