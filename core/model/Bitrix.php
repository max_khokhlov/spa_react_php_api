<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

//CModule::IncludeModule('main');
//CModule::IncludeModule('sale');

use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

global $USER;

Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");
Bitrix\Main\Loader::includeModule("main");

?>

<?php

class model_Bitrix extends Model{

    private $user;
    private $mail;
    private $mail_template;
    
    public function __construct()
    {
        global $USER;
        global $APPLICATION;

        parent::__construct();
        $this->user = $USER;
        $this->mail_template = __DIR__."/../mail_templates/";
        $this->application = $APPLICATION;
        $this->mail = new model_Mail($this->user->GetEmail());
    }

    public function generatePassword(){
        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        $max=10;
        $size=strlen($chars)-1;
        $password=null;

        while($max--)
            $password.=$chars[rand(0,$size)];
        
        return $password;
    }
    
    public function addOrder($user_id, $basket_prod = false){
        
        $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
        $currencyCode = Option::get('sale', 'default_currency', 'RUB');
        DiscountCouponsManager::init();

        //создание заказа
        $order = Order::create($siteId, $user_id);
        $order->setPersonTypeId(1);
        $order->setField('CURRENCY', $currencyCode);
        $basket = Sale\Basket::create($siteId);

        
        
        //корзина
        if (!$basket_prod) {

            $item = $basket->createItem('catalog', 201);
            $item->setFields(array(
                'QUANTITY' => 1,
                'CURRENCY' => $currencyCode,
                'LID' => SITE_ID,
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
            
        }else{
            
            $product_basket = array();
            foreach ($basket_prod->rooms as $room) {

                $room = (array)$room;
                
                $name = 'Без названия';

                $res = CIBlockElement::GetByID($room['id']);
                if($ar_res = $res->GetNext()) $name = $ar_res['NAME'];

                $product_basket[] = array(
                    'PRODUCT_ID' => $room['id'],
                    'NAME' => $name,
                    'QUANTITY' => $room['quantity']
                );
                
            }

            if (count($basket_prod->services) > 0){
                
                foreach ($basket_prod->services as $service){

                    $service = (array)$service;
                    
                    $name = 'Без названия';

                    $res = CIBlockElement::GetByID($service['id']);
                    if($ar_res = $res->GetNext()) $name = $ar_res['NAME'];

                    $product_basket[] = array(
                        'PRODUCT_ID' => $service['id'],
                        'NAME' => $name,
                        'QUANTITY' => $service['quantity']
                    );
                }
            }
            
            foreach ($product_basket as $product){
                if ($item = $basket->getExistsItem('catalog', $product['PRODUCT_ID'])){
                    $item->setField('QUANTITY', $item->getQuantity() + $product['QUANTITY']);
                }else{
                    $item = $basket->createItem('catalog', $product['PRODUCT_ID']);
                    $item->setFields(array(
                        'QUANTITY' => $product['QUANTITY'],
                        'CURRENCY' => $currencyCode,
                        'NAME' => $product['NAME'],
                        'LID' => $siteId,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    ));
                }
            }
            
        }
        
        $order->setBasket($basket);
        
        // Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());

        // Создаём оплату со способом #1
        $paymentCollection = $order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $payment->setFields(array(
            'PAY_SYSTEM_ID' => 1,
            'PAY_SYSTEM_NAME' => "Внутренний счет",
        ));

        $order->doFinalAction(true);
        $order->save();
        
        return $order->GetId();
       
    }
    
    public function checkUser($data){
        
        $rsUser = CUser::GetByLogin($data['email']);
        
        if ($arUser = $rsUser->Fetch()){

            $output = array(
                'status' => 'success',
                'user_data' => array(
                    'id' => $arUser['ID'],
                    'firstname' => $arUser['NAME'],
                    'surname' => $arUser['LAST_NAME'],
                    'date_register' => $arUser['DATE_REGISTER'],
                    'email' => $arUser['EMAIL'],
                    'phone' => $arUser['PERSONAL_PHONE'],
                    'manager_name' => $this->user->GetFullName(),
                    'manager_email' => $this->user->GetEmail()
                )
            );

            $subject = "Информация о личном кабинете на сайте Прибалтика.ру";
            $message = $this->mail->getTemplates($this->mail_template."check_user.html", $output['user_data']);
            $this->mail->Send(array($output['user_data']['email'], $this->user->GetEmail()), $subject, $message);
            
            
            return $output = array(
                'status' => 'success',
                'user_bitrix_id' => $arUser['ID']
            );
            
        }else{
            
            $output = array(
                'status' => 'error',
            );
            
        }
        
        return $output;
        
    }

    public function addServicesToOrder($data, $order_id){

        $result = $this->db->query("SELECT order_bitrix_id  FROM `order` WHERE order_id  = {$order_id}; ");

        if (count($result->rows) <= 0){
            return false;
        }

        $orderID = $result->row['order_bitrix_id'];
        $product_basket = array();
        $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
        $currencyCode = Option::get('sale', 'default_currency', 'RUB');
        $order = Sale\Order::load($orderID);
        $basket = $order->getBasket();

        if (isset($data['services'])){

            foreach ($data['services'] as $service){

                $service = (array)$service;
                $name = 'Без названия';

                $res = CIBlockElement::GetByID($service['id']);
                if($ar_res = $res->GetNext()) $name = $ar_res['NAME'];

                $product_basket[] = array(
                    'PRODUCT_ID' => $service['id'], 
                    'NAME' => $name,
                    'QUANTITY' => $service['quantity']
                );
            }

            foreach ($product_basket as $product){
                if ($item = $basket->getExistsItem('catalog', $product['PRODUCT_ID'])){
                    $item->setField('QUANTITY', $item->getQuantity() + $product['QUANTITY']);
                }else{
                    $item = $basket->createItem('catalog', $product['PRODUCT_ID']);
                    $item->setFields(array(
                        'QUANTITY' => $product['QUANTITY'],
                        'CURRENCY' => $currencyCode,
                        'NAME' => $product['NAME'],
                        'LID' => $siteId,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    ));
                }
            }

            $basket->save();
            $order->refreshData();
            $order->save();

            return true;

        }

    }
    
    public function deleteServiceToOrder($order_id){

        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID"=> 11, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);

        $services = array();

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $services[] = $arFields['ID'];

        }

        $result = $this->db->query("SELECT order_bitrix_id  FROM `order` WHERE order_id  = {$order_id}; ");

        if (count($result->rows) <= 0){
            return false;
        }

        $orderID = $result->row['order_bitrix_id'];
        
        $order = Sale\Order::load($orderID);
        $basket = $order->getBasket();

        foreach ($services as $service) {

            if ($item = $basket->getExistsItem('catalog', $service)){
                $item->delete();
            }
            
        }
        
        $basket->save();
        $order->refreshData();
        $order->save();
        
    }
    
    public function addProductInOrder($order_id, $product_id, $quantity, $price = false){

        $result = $this->db->query("SELECT order_bitrix_id  FROM `order` WHERE order_id  = {$order_id}; ");

        if (count($result->rows) <= 0){
            return false;
        }

        $orderID = $result->row['order_bitrix_id'];
        $product_basket = array();
        $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
        $currencyCode = Option::get('sale', 'default_currency', 'RUB');
        $order = Sale\Order::load($orderID);
        $basket = $order->getBasket();

        $name = 'Без названия';

        $res = CIBlockElement::GetByID($product_id);
        if($ar_res = $res->GetNext()) $name = $ar_res['NAME'];

        $product_basket[] = array(
            'PRODUCT_ID' => $product_id,
            'NAME' => $name,
            'QUANTITY' => $quantity
        );

        foreach ($product_basket as $product){
            if ($item = $basket->getExistsItem('catalog', $product['PRODUCT_ID'])){
                $item->setField('QUANTITY', $item->getQuantity() + $product['QUANTITY']);
            }else{
                
                if ($price){
                    $item = $basket->createItem('catalog', $product['PRODUCT_ID']);
                    $item->setFields(array(
                        'QUANTITY' => $product['QUANTITY'],
                        'CURRENCY' => $currencyCode,
                        'NAME' => $product['NAME'],
                        'CUSTOM_PRICE' => $price,
                        'LID' => $siteId,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    ));
                }else{
                    $item = $basket->createItem('catalog', $product['PRODUCT_ID']);
                    $item->setFields(array(
                        'QUANTITY' => $product['QUANTITY'],
                        'CURRENCY' => $currencyCode,
                        'NAME' => $product['NAME'],
                        'LID' => $siteId,
                        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                    ));
                }
                
                
            }
        }

        $basket->save();
        $order->refreshData();
        $order->save();

        return true;    
            
        
    }
    
    public function deleteProductInOrder($order_id, $product_id, $quantity){

        $result = $this->db->query("SELECT order_bitrix_id  FROM `order` WHERE order_id  = {$order_id}; ");

        if (count($result->rows) <= 0){
            return false;
        }

        $orderID = $result->row['order_bitrix_id'];

        $order = Sale\Order::load($orderID);
        $basket = $order->getBasket();

        if ($item = $basket->getExistsItem('catalog', $product_id)){
            $item->setField('QUANTITY', $item->getQuantity() - $quantity);
        }

        $basket->save();
        $order->refreshData();
        $order->save();
        
    }
    
    public function updateBasketOrder($day_id, $data){
        
        if (intval($day_id) > 0 && count($data) > 0){
            
           $result = $this->db->query("SELECT ord.order_bitrix_id AS bitrix_id FROM `order_day` ordd LEFT JOIN `order` ord ON ordd.order_id = ord.order_id WHERE ordd.order_day_id  = {$day_id}; ");
           
           if (count($result->rows) > 0){

               $product_basket = array();
               $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
               $currencyCode = Option::get('sale', 'default_currency', 'RUB');
               
               $orderID = $result->row['bitrix_id'];
               $order = Sale\Order::load($orderID);
               $basket =$order->getBasket();
               
               foreach ($data['rooms'] as $room) {

                   $name = 'Без названия';
                   
                   $res = CIBlockElement::GetByID($room['id']);
                   if($ar_res = $res->GetNext()) $name = $ar_res['NAME'];
                   
                   $product_basket[] = array(
                       'PRODUCT_ID' => $room['id'],
                       'NAME' => $name,
                       'QUANTITY' => 1
                   );
               }
               
               if (isset($data['services'])){
                   foreach ($data['services'] as $service){

                       $name = 'Без названия';

                       $res = CIBlockElement::GetByID($service['id']);
                       if($ar_res = $res->GetNext()) $name = $ar_res['NAME'];
                       
                       $product_basket[] = array(
                           'PRODUCT_ID' => $service['id'],
                           'COMMENTS' => $service['comments'],
                           'NAME' => $name,
                           'QUANTITY' => 1
                       );
                   }
               }
               
               if ($item = $basket->getExistsItem('catalog', 201)){
                   $item->delete();
               }
               
               foreach ($product_basket as $product){
                   if ($item = $basket->getExistsItem('catalog', $product['PRODUCT_ID'])){
                       $item->setField('QUANTITY', $item->getQuantity() + $product['QUANTITY']);
                   }else{
                       $item = $basket->createItem('catalog', $product['PRODUCT_ID']);
                       $item->setFields(array(
                           'QUANTITY' => $product['QUANTITY'],
                           'CURRENCY' => $currencyCode,
                           'NAME' => $product['NAME'],
                           'LID' => $siteId,
                           'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
                       ));
                   }
               }
                
               $basket->save();
               $order->refreshData();
               $order->save();

               return true;
               
           }else{
               
               return false;
               
           }
           
        }
        
    }
    
    public function deleteProductBasketToDay($day_id, $data){

        if (intval($day_id) > 0 && count($data) > 0) {
            
            $result = $this->db->query("SELECT ord.order_bitrix_id AS bitrix_id FROM `order_day` ordd LEFT JOIN `order` ord ON ordd.order_id = ord.order_id WHERE ordd.order_day_id  = {$day_id}; ");
            
            if (isset($data['all']) && count($result->rows) > 0){
                
                    $product_data = array();
                    $reserve_id = array();
                
                   $reserve_result = $this->db->query("SELECT rp.reserve_id, rh.room_id FROM `reserve_hotel_people` rp LEFT JOIN `reserve_hotel` rh ON rp.reserve_hotel_id = rh.reserve_hotel_id WHERE rp.order_day_id = {$day_id}");
                   
                   if (count($reserve_result->rows) > 0){
                       
                       foreach ($reserve_result->rows as $row){

                           if (!in_array($row['reserve_id'], $reserve_id)){

                               $reserve_id[] = $row['reserve_id'];

                               $service_reserve = $this->db->query("SELECT service_id FROM hotel_service WHERE hotel_reserve_id = {$row['reserve_id']} AND status = 'reserve'");

                               if (count($service_reserve->rows) > 0){

                                   foreach ($service_reserve->rows as $s){

                                       if (isset($product_data[$s['service_id']])){
                                           $product_data[$s['service_id']] += 1;
                                       }else{
                                           $product_data[$s['service_id']] = 1;
                                       }

                                   }

                               }
                               
                           }
                           
                            if (isset($product_data[$row['room_id']])){
                                $product_data[$row['room_id']] += 1;
                            }else{
                                $product_data[$row['room_id']] = 1;
                            }
                            
                       }
                       
                   }
                   
                   $hotels_result = $this->db->query("SELECT reserve_id, room_id FROM `hotel_people` WHERE order_day_id = {$day_id}");
                   
                   if ($hotels_result->rows){
                       foreach ($hotels_result->rows as $row){
                           if (!in_array($row['reserve_id'], $reserve_id)){

                               $reserve_id[] = $row['reserve_id'];

                               $service_reserve = $this->db->query("SELECT service_id FROM hotel_service WHERE hotel_reserve_id = {$row['reserve_id']} AND status = 'hotel'");

                               if (count($service_reserve->rows) > 0){

                                   foreach ($service_reserve->rows as $s){

                                       if (isset($product_data[$s['service_id']])){
                                           $product_data[$s['service_id']] += 1;
                                       }else{
                                           $product_data[$s['service_id']] = 1;
                                       }

                                   }

                               }

                           }

                           if (isset($product_data[$row['room_id']])){
                               $product_data[$row['room_id']] += 1;
                           }else{
                               $product_data[$row['room_id']] = 1;
                           }
                       }
                       
                   }
                
            }else{

                $product_data = array();
                $reserve_id = array();
                
                if (isset($data['reserve_hotel_id'])){

                    $reserve_result = $this->db->query("SELECT rp.reserve_id, rh.room_id FROM `reserve_hotel_people` rp LEFT JOIN `reserve_hotel` rh ON rp.reserve_hotel_id = rh.reserve_hotel_id WHERE rp.reserve_hotel_id = {$data['reserve_hotel_id']} AND rp.order_day_id = {$data['order_day_id']}");
                    
                    if (count($reserve_result->rows) > 0){

                        foreach ($reserve_result->rows as $row){

                            if (!in_array($row['reserve_id'], $reserve_id)){

                                $reserve_id[] = $row['reserve_id'];

                                $service_reserve = $this->db->query("SELECT service_id FROM hotel_service WHERE hotel_reserve_id = {$row['reserve_id']} AND status = 'reserve'");

                                if (count($service_reserve->rows) > 0){

                                    foreach ($service_reserve->rows as $s){

                                        if (isset($product_data[$s['service_id']])){
                                            $product_data[$s['service_id']] += 1;
                                        }else{
                                            $product_data[$s['service_id']] = 1;
                                        }

                                    }

                                }

                            }

                            if (isset($product_data[$row['room_id']])){
                                $product_data[$row['room_id']] += 1;
                            }else{
                                $product_data[$row['room_id']] = 1;
                            }

                        }

                    }
                    
                }else{
                    $hotels_result = $this->db->query("SELECT reserve_id, room_id FROM `hotel_people` WHERE order_day_id = {$data['order_day_id']} AND room_id = {$data['room_id']}");

                    if ($hotels_result->rows){
                        foreach ($hotels_result->rows as $row){
                            if (!in_array($row['reserve_id'], $reserve_id)){

                                $reserve_id[] = $row['reserve_id'];

                                $service_reserve = $this->db->query("SELECT service_id FROM hotel_service WHERE hotel_reserve_id = {$row['reserve_id']} AND status = 'hotel'");

                                if (count($service_reserve->rows) > 0){

                                    foreach ($service_reserve->rows as $s){

                                        if (isset($product_data[$s['service_id']])){
                                            $product_data[$s['service_id']] += 1;
                                        }else{
                                            $product_data[$s['service_id']] = 1;
                                        }

                                    }

                                }

                            }

                            if (isset($product_data[$row['room_id']])){
                                $product_data[$row['room_id']] += 1;
                            }else{
                                $product_data[$row['room_id']] = 1;
                            }
                        }

                    }
                    
                }
                
            }
            
            $orderID = $result->row['bitrix_id'];
            $order = Sale\Order::load($orderID);
            $basket = $order->getBasket();


            foreach ($product_data as $id => $count){
                if ($item = $basket->getExistsItem('catalog', $id)){
                    $item->setField('QUANTITY', $item->getQuantity() - $count);
                }
            }

            $basket->save();
            $order->refreshData();
            $order->save();

            return true;
            
        }
        
    }
    
    public function updateOrder($order_id = false, $data){
        
        if(is_array($data) && $order_id){

            $arOrder = CSaleOrder::GetByID($order_id);
            
            if ($arOrder){
                
                $data['EMP_STATUS_ID'] = $this->user->GetID();
                
                $res = CSaleOrder::Update($order_id, $data);
                
                echo "<pre>";print_r($res);die();
                
                return true;
                
            }else{
                
                return false;
                
            }
            
        }else{
            
            return false;
            
        }
        
    }
    
    public function canсelOrder($order_id = false, $status_id){
        
        if($status_id && $order_id){
            
            if (!CSaleOrder::StatusOrder($order_id, $status_id)) {
                return false;
            }else{
                CSaleOrder::CancelOrder($order_id, "Y", "Удален из менеджерского раздела");
                return true;
            }
            
        }else{
            return false;
        }
        
    }
    
    public function addUser($user_data){

        $user = new CUser;
        
        $password = $this->generatePassword();
        
        $arFields = Array(
            "NAME"              => $user_data['firstname'],
            "LAST_NAME"         => $user_data['surname'],
            "EMAIL"             => $user_data['email'],
            "LOGIN"             => $user_data['email'],
            "LID"               => "ru",
            "ACTIVE"            => "Y",
            "GROUP_ID"          => array(2),
            "PASSWORD"          => $password,
            "CONFIRM_PASSWORD"  => $password,
            "PERSONAL_PHOTO"    => array(),
            "PERSONAL_PHONE"    => $user_data['phone'],
        );

        
        $user_data['password'] = $password;
        $user_data['manager_name'] = $this->user->GetFullName();
        $user_data['manager_email'] = $this->user->GetEmail();
        
        $subject = "Вы зарегистрированы на сайте Прибалтика.ру";
        $message = $this->mail->getTemplates($this->mail_template."new_user.html", $user_data);
        
        $ID = $user->Add($arFields);
        
        if ($ID && intval($ID) > 0){
            
            $this->mail->Send(array($user_data['email'], $this->user->GetEmail()), $subject, $message);
            
            return $ID;
        }else{
            
            return false;
            
        }
        
    }
    
    public function priceRub($val) {
        //Строка вида 124|EUR выводит конвертированную в рубли 124*курс число
        $price=explode('|', $val);
        $rub_price=CCurrencyRates::ConvertCurrency($price[0],$price[1],'RUB');
        return $rub_price;
    }

    public function getTourByID($tour_id, $days = false){

        if ($tour_id != 0 && $tour_id != -1){

            CModule::IncludeModule('iblock');

            $arSelect = Array("ID", "IBLOCK_ID", "NAME");
            $arFilter = Array("IBLOCK_ID"=>IntVal(15),"ID"=>IntVal($tour_id), "ACTIVE"=>"Y");

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);

            $output = array();

            while($ob = $res->GetNextElement()){

                $arFields = $ob->GetFields();
                $arProps = $ob->GetProperties();

                $output['name'] = $arFields['NAME'];

                if (isset($arProps['TOUR_CATEGORY']) && count($arProps['TOUR_CATEGORY']['VALUE']) > 0){

                    foreach ($arProps['TOUR_CATEGORY']['VALUE'] as $tour_category){

                        $cat_arSelect = Array("ID", "IBLOCK_ID", "NAME");
                        $cat_arFilter = Array("IBLOCK_ID"=>IntVal($arProps['TOUR_CATEGORY']['LINK_IBLOCK_ID']),"ID"=>IntVal($tour_category), "ACTIVE"=>"Y");

                        $res = CIBlockElement::GetList(Array(), $cat_arFilter, false, Array("nPageSize"=>99), $cat_arSelect);

                        while($ob = $res->GetNextElement()){

                            $cat_arFields = $ob->GetFields();

                            $output['tour_category'][] = array(
                                'id' => $cat_arFields['ID'],
                                'name' => $cat_arFields['NAME']
                            );

                        }

                    }

                }

                if (isset($arProps['COUNTRY']) && count($arProps['COUNTRY']['VALUE']) > 0){

                    foreach ($arProps['COUNTRY']['VALUE'] as $country){

                        $country_arSelect = Array("ID", "IBLOCK_ID", "NAME");
                        $country_arFilter = Array("IBLOCK_ID"=>IntVal($arProps['COUNTRY']['LINK_IBLOCK_ID']),"ID"=> IntVal($country), "ACTIVE"=>"Y");

                        $res = CIBlockElement::GetList(Array(), $country_arFilter, false, Array("nPageSize"=>99), $country_arSelect);

                        while($ob = $res->GetNextElement()){

                            $coutry_arFields = $ob->GetFields();

                            $output['country'][$coutry_arFields['ID']] = array(
                                'name' => $coutry_arFields['NAME']
                            );

                        }

                    }

                }

//                if (isset($arProps['CITIES']) && count($arProps['CITIES']['VALUE']) > 0){
//
//                    $arProps['CITIES']['VALUE'][] = 129;
//
//                    foreach ($arProps['CITIES']['VALUE'] as $city){
//
//                        $city_arSelect = Array("ID", "IBLOCK_ID", "NAME");
//                        $city_arFilter = Array("IBLOCK_ID"=>IntVal($arProps['CITIES']['LINK_IBLOCK_ID']),"ID"=>IntVal($city), "ACTIVE"=>"Y");
//
//                        $res = CIBlockElement::GetList(Array(), $city_arFilter, false, Array("nPageSize"=>999), $city_arSelect);
//
//                        while($ob = $res->GetNextElement()){
//
//                            $city_arFields = $ob->GetFields();
//                            $city_arProps = $ob->GetProperties();
//
//                            $output['country'][$city_arProps['COUNTRY']['VALUE']]['city'][] = array(
//                                'id' => $city_arFields['ID'],
//                                'name' => $city_arFields['NAME']
//                            );
//
//                            $output['city'][$city_arFields['ID']] = array(
//                                'id' => $city_arFields['ID'],
//                                'name' => $city_arFields['NAME']
//                            );
//
//                        }
//
//                    }
//
//                }

                if ($days !== false && count($days) > 0){

                    foreach ($days as $day){

                        $city_arSelect = Array("ID", "IBLOCK_ID", "NAME");
                        $city_arFilter = Array("IBLOCK_ID"=>IntVal($arProps['CITIES']['LINK_IBLOCK_ID']),"ID"=>IntVal($day['city_id']), "ACTIVE"=>"Y");

                        $res = CIBlockElement::GetList(Array(), $city_arFilter, false, Array("nPageSize"=>1), $city_arSelect);

                        while($ob = $res->GetNextElement()){

                            $city_arFields = $ob->GetFields();
                            $city_arProps = $ob->GetProperties();

                            $output['country'][$city_arProps['COUNTRY']['VALUE']]['city'][] = array(
                                'id' => $city_arFields['ID'],
                                'name' => $city_arFields['NAME']
                            );

                            $output['city'][$city_arFields['ID']] = array(
                                'id' => $city_arFields['ID'],
                                'name' => $city_arFields['NAME']
                            );

                        }

                        $output['city'][129] = array(
                            'id' => 129,
                            'name' => 'Москва'
                        );


                        $hotel_arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_CITY");
                        $hotel_arFilter = Array("IBLOCK_ID"=>IntVal(5),"PROPERTY_CITY"=> $day['city_id'], "ACTIVE"=>"Y");

                        $res = CIBlockElement::GetList(Array(), $hotel_arFilter, false, Array("nPageSize"=>99), $hotel_arSelect);

                        while($ob = $res->GetNextElement()){

                            $hotel_arFields = $ob->GetFields();
                            $hotel_arProps = $ob->GetProperties();

                            $rooms = $this->getRoomByHotelID($hotel_arFields['ID']);

                            $output['hotel'][$day['city_id']][$hotel_arFields['ID']] = array(
                                'id' => $hotel_arFields['ID'],
                                'name' => $hotel_arFields['NAME'],
                                'stars' => $hotel_arProps['STARS']['VALUE'],
                                'contacts' => "",
                                'price' => $this->priceRub($hotel_arProps['PRICE_FROM']['VALUE']),
                                'rooms' => $rooms
                            );

                        }

                    }

                }

                if (isset($arProps['DATE']) && count($arProps['DATE']['VALUE']) > 0){

                    foreach ($arProps['DATE']['VALUE'] as $date){

                        $output['date'][] = $date;

                    }

                }

                if (isset($arProps['DURATION'])){

                    $output['duration'] = $arProps['DURATION']['VALUE'];

                }

                if (isset($arProps['PRICE_FROM'])){

                    $output['price'] = $this->priceRub($arProps['PRICE_FROM']['VALUE']);

                }

                return $output;



            }

        }else{

            $output = array(
                'city' => $this->getCities(),
            );

            if ($days && count($days) > 0){

                foreach ($days as $day){

                    $hotel_arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_CITY");
                    $hotel_arFilter = Array("IBLOCK_ID"=>IntVal(5),"PROPERTY_CITY"=> $day['city_id'], "ACTIVE"=>"Y");

                    $res = CIBlockElement::GetList(Array(), $hotel_arFilter, false, Array("nPageSize"=>99), $hotel_arSelect);

                    while($ob = $res->GetNextElement()){

                        $hotel_arFields = $ob->GetFields();
                        $hotel_arProps = $ob->GetProperties();

                        $rooms = $this->getRoomByHotelID($hotel_arFields['ID']);

                        $output['hotel'][$day['city_id']][$hotel_arFields['ID']] = array(
                            'id' => $hotel_arFields['ID'],
                            'name' => $hotel_arFields['NAME'],
                            'stars' => $hotel_arProps['STARS']['VALUE'],
                            'contacts' => "",
                            'price' => $this->priceRub($hotel_arProps['PRICE_FROM']['VALUE']),
                            'rooms' => $rooms
                        );

                    }

                }

            }

            return $output;

        }

    }

    public function getDayByTourID($tour_id){

        CModule::IncludeModule('iblock');

        $arSelect = Array("ID", "IBLOCK_ID", "NAME");
        $arFilter = Array("IBLOCK_ID"=>IntVal(15),"ID"=>IntVal($tour_id), "ACTIVE"=>"Y");

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);

        $output = array();

        while($ob = $res->GetNextElement()) {

            $arProps = $ob->GetProperties();

            if (isset($arProps['TOUR_DAYS']) && count($arProps['TOUR_DAYS']) > 0){

                foreach ($arProps['TOUR_DAYS']['VALUE'] as $day){

                    $daySelect = Array("ID", "IBLOCK_ID", "NAME");
                    $dayFilter = Array("IBLOCK_ID"=>IntVal(16),"ID"=>IntVal($day), "ACTIVE"=>"Y");

                    $res = CIBlockElement::GetList(Array(), $dayFilter, false, Array("nPageSize"=>1), $daySelect);

                    while ($ob = $res->GetNextElement()){

                        $arProps = $ob->GetProperties();

                        $output[] = array(
                           'city' => $arProps['CITIES']['VALUE'][count($arProps['CITIES']['VALUE']) - 1],
                           'diff' => $arProps['DURATION']['VALUE']
                        );

                    }


                }

            }

        }

        return $output;

    }

    public function getRoomByHotelID($hotel_id){

        $output = array();
        $hotel_arSelect = Array("ID", "IBLOCK_ID", "NAME");
        $hotel_arFilter = Array("IBLOCK_ID"=>IntVal(10),"PROPERTY_CML2_LINK"=> $hotel_id, "ACTIVE"=>"Y", "<=DATE_ACTIVE_FROM" => array(false, ConvertTimeStamp(false, "FULL")), ">=DATE_ACTIVE_TO" => array(false, ConvertTimeStamp(false, "FULL")));

        $res = CIBlockElement::GetList(Array(), $hotel_arFilter, false, Array("nPageSize"=>99), $hotel_arSelect);

        while($ob = $res->GetNextElement()){

            $hotel_arFields = $ob->GetFields();
            $price = CPrice::GetBasePrice($hotel_arFields['ID']);
            $hotel_arProps = $ob->GetProperties();
            $dop_services = array();

            if (count($hotel_arProps['ADDITIONAL']['VALUE']) > 0 && !empty($hotel_arProps['ADDITIONAL']['VALUE'])){

                foreach ($hotel_arProps['ADDITIONAL']['VALUE'] as $services_id){

                    $services_filter = Array("IBLOCK_ID" => IntVal(9), "ID" => $services_id);
                    $resServices = CIBlockElement::GetList(Array(), $services_filter, false, Array("nPageSize"=>99), array("ID", "IBLOCK_ID", "NAME"));

                    while($ob = $resServices->GetNextElement()){

                        $service_arFields = $ob->GetFields();
                        $priceService = CPrice::GetBasePrice($service_arFields['ID']);

                        $dop_services[$service_arFields['ID']] = array(
                            'name' => $service_arFields['NAME'],
                            'price' => $this->priceRub($priceService['PRICE']."|".$priceService['CURRENCY'])
                        );

                    }

                }

            }


            $type_filter = Array("IBLOCK_ID" => IntVal(8), "ID" => $hotel_arProps["TYPE"]["VALUE"]);
            $type = CIBlockElement::GetList(Array(), $type_filter, false, Array("nPageSize"=>99), array("ID", "IBLOCK_ID", "NAME"));

            $typeArr = array();

            while($ob = $type->GetNextElement()){
               $typeFields = $ob->GetFields();
               $typeProps = $ob->GetProperties();

                $typeArr = array(
                    "id" => $typeFields["ID"],
                    "name" => $typeFields["NAME"],
                    "description" => $typeProps["WHAT_IS"]["VALUE"],
                    "code" => $typeProps["CODE_ROOM"]["VALUE"],
                    "capacity" => $typeProps["CAPACITY"]["VALUE"]
                );
            }

            $output[$hotel_arFields['ID']] = array(
               'id' => $hotel_arFields['ID'],
               'name' => $hotel_arFields['NAME'],
               'type' => $typeArr,
               'price' => $this->priceRub($price['PRICE']."|".$price['CURRENCY']),
               'include' => $hotel_arProps['INCLUDE_']['VALUE'],
               'facilities' => $hotel_arProps['FACILITIES']['VALUE'],
               'services' => $dop_services
            );

        }

        return $output;

    }

    public function getRoomByID($room_id){

        $arrFilter = Array("IBLOCK_ID" => IntVal(10), "ID" => $room_id);

        $res = CIBlockElement::GetList(Array(), $arrFilter, false, Array("nPageSize"=>99), array("ID", "IBLOCK_ID", "NAME"));
        $dop_services = array();
        $output = array();

        while($ob = $res->GetNextElement()){

            $roomField = $ob->GetFields();
            $price = CPrice::GetBasePrice($roomField['ID']);
            $roomProps = $ob->GetProperties();

            if (count($roomProps['ADDITIONAL']['VALUE']) > 0 && !empty($roomProps['ADDITIONAL']['VALUE'])){

                foreach ($roomProps['ADDITIONAL']['VALUE'] as $services_id){

                    $services_filter = Array("IBLOCK_ID" => IntVal(9), "ID" => $services_id);
                    $resServices = CIBlockElement::GetList(Array(), $services_filter, false, Array("nPageSize"=>99), array("ID", "IBLOCK_ID", "NAME"));

                    while($ob = $resServices->GetNextElement()){

                        $service_arFields = $ob->GetFields();
                        $priceService = CPrice::GetBasePrice($service_arFields['ID']);

                        $dop_services[$service_arFields['ID']] = array(
                            'name' => $service_arFields['NAME'],
                            'price' => $this->priceRub($priceService['PRICE']."|".$priceService['CURRENCY'])
                        );

                    }

                }

            }


            $type_filter = Array("IBLOCK_ID" => IntVal(8), "ID" => $roomProps["TYPE"]["VALUE"]);

            $type = CIBlockElement::GetList(Array(), $type_filter, false, Array("nPageSize"=>99), array("ID", "IBLOCK_ID", "NAME"));

            $typeArr = array();

            while($ob = $type->GetNextElement()){
                $typeFields = $ob->GetFields();
                $typeProps = $ob->GetProperties();

                $typeArr = array(
                    "id" => $typeFields["ID"],
                    "name" => $typeFields["NAME"],
                    "description" => $typeProps["WHAT_IS"]["~VALUE"],
                    "code" => $typeProps["CODE_ROOM"]["VALUE"],
                    "capacity" => $typeProps["CAPACITY"]["VALUE"]
                );
            }

            $output[$roomField['ID']] = array(
                'id' => $roomField['ID'],
                'name' => $roomField['NAME'],
                'type' => $typeArr,
                'price' => $this->priceRub($price['PRICE']."|".$price['CURRENCY']),
                'include' => $roomProps['INCLUDE_']['VALUE'],
                'facilities' => $roomProps['FACILITIES']['VALUE'],
                'services' => $dop_services
            );
        }

        return $output;

    }

    public function getCities(){

        $output = array();
        $arrFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE"=>"");
        $res = CIBlockElement::GetList(Array(), $arrFilter, false, Array("nPageSize"=>999), array("ID", "IBLOCK_ID", "NAME"));

        while($ob = $res->GetNextElement()){

            $result = $ob->GetFields();
            $output[$result['ID']] = array(
                'name' => $result['NAME'],
                'id' => $result['ID']
            );

        }

        return $output;

    }

    public function getHotelByID($id){

        $output = array();
        $hotel_arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_CITY");
        $hotel_arFilter = Array("IBLOCK_ID"=>IntVal(5),"ID"=> $id, "ACTIVE"=>"Y");

        $res = CIBlockElement::GetList(Array(), $hotel_arFilter, false, Array("nPageSize"=>99), $hotel_arSelect);

        while($ob = $res->GetNextElement()){

            $result = $ob->GetFields();
            $output = array(
                'name' => $result['NAME'],
                'id' => $result['ID']
            );

        }

        return $output;


    }

    public function getHotels(){

        $output = array();

        $cities = $this->getCities();

        foreach ($cities as $city){

            $hotel_arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_CITY");
            $hotel_arFilter = Array("IBLOCK_ID"=>IntVal(5), "PROPERTY_CITY" => $city['id'] , "ACTIVE"=>"Y");

            $res = CIBlockElement::GetList(Array(), $hotel_arFilter, false, Array("nPageSize"=>99), $hotel_arSelect);

            while($ob = $res->GetNextElement()){

                $hotel_arFields = $ob->GetFields();
                $hotel_arProps = $ob->GetProperties();


                $rooms = $this->getRoomByHotelID($hotel_arFields['ID']);

                $output[$city['id']][$hotel_arFields['ID']] = array(
                    'id' => $hotel_arFields['ID'],
                    'name' => $hotel_arFields['NAME'],
                    'city' => $city['id'],
                    'stars' => $hotel_arProps['STARS']['VALUE'],
                    'contacts' => "",
                    'price' => $this->priceRub($hotel_arProps['PRICE_FROM']['VALUE']),
                    'rooms' => $rooms
                );

            }

        }

        return $output;


    }

    public function getHotelsList(){

        $output = array();

        $cities = $this->getCities();

        foreach ($cities as $city){

            $hotel_arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_CITY");
            $hotel_arFilter = Array("IBLOCK_ID"=>IntVal(5), "PROPERTY_CITY" => $city['id'] , "ACTIVE"=>"Y");

            $res = CIBlockElement::GetList(Array(), $hotel_arFilter, false, Array("nPageSize"=>99), $hotel_arSelect);

            while($ob = $res->GetNextElement()){

                $hotel_arFields = $ob->GetFields();

                $output[$hotel_arFields['ID']] = array(
                    'id' => $hotel_arFields['ID'],
                    'name' => $hotel_arFields['NAME'],
                    'city' => $city['id'],
                );

            }

        }

        return $output;


    }

    public function getCityByID($id){

        $output = array();
        $arrFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE"=>"", "ID" => $id);
        $res = CIBlockElement::GetList(Array(), $arrFilter, false, Array("nPageSize"=>999), array("ID", "IBLOCK_ID", "NAME"));

        while($ob = $res->GetNextElement()){

            $result = $ob->GetFields();
            $output = array(
                'name' => $result['NAME'],
                'id' => $result['ID']
            );

        }

        return $output;

    }

    public function getRooms(){

        $arrFilter = Array("IBLOCK_ID" => IntVal(10));

        $res = CIBlockElement::GetList(Array(), $arrFilter, false, Array("nPageSize"=>999), array("ID", "IBLOCK_ID", "NAME"));
        $output = array();

        while($ob = $res->GetNextElement()){

            $roomField = $ob->GetFields();


            $output[$roomField['ID']] = array(
                'id' => $roomField['ID'],
                'name' => $roomField['NAME'],
            );
        }

        return $output;

    }

    public function getTours(){

        CModule::IncludeModule('iblock');

        $arSelect = Array("ID", "IBLOCK_ID", "NAME");
        $arFilter = Array("IBLOCK_ID"=>IntVal(15), "ACTIVE"=>"Y");

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>999), $arSelect);

        $output = array();

        while($ob = $res->GetNextElement()) {

            $arFields = $ob->GetFields();

            $output[$arFields['ID']] = array(
                'id' => $arFields['ID'],
                'name' => $arFields['NAME'],
            );

        }


        return $output;

    }

    public function getManager(){

        $filter = Array("GROUPS_ID" => Array(6, 1));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        $arSpecUser = array();

        while ($arUser = $rsUsers->Fetch()) {
            $arSpecUser[$arUser['ID']] = array(
                'name' => $arUser['NAME'].' '.$arUser['LAST_NAME'],
                'email' => $arUser['EMAIL']
            );
        }

        return $arSpecUser;

    }

    public function getManagerList(){

        $filter = Array("GROUPS_ID" => Array(1));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
        $arSpecUser = array();


        $arSpecUser[0] = array(
            "name_group" => 'Администраторы',
            "group_id" => "1"
        );

        while ($arUser = $rsUsers->Fetch()) {

            $arSpecUser[0]['users'][$arUser['ID']] = array(
                'name' => $arUser['NAME'].' '.$arUser['LAST_NAME'],
                'email' => $arUser['EMAIL']
            );
        }

        $filter = Array("GROUPS_ID" => Array(6));
        $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);


        $arSpecUser[1] = array(
            "name_group" => 'Менеджеры',
            "group_id" => "6"
        );

        while ($arUser = $rsUsers->Fetch()) {

            $arSpecUser[1]['users'][$arUser['ID']] = array(
                'name' => $arUser['NAME'].' '.$arUser['LAST_NAME'],
                'email' => $arUser['EMAIL']
            );
        }

        return $arSpecUser;


    }
    
    public function AddOrderProperty($code,  $order, $value) {

        $arr = array();

        if (!strlen($code)) {
            return false;
        }
        if (CModule::IncludeModule('sale')) {

            $db_props = CSaleOrderProps::GetList(array("SORT" => "ASC"), array("CODE" => $code));

            while ($arProps = $db_props->Fetch()){

                $db_vals = CSaleOrderPropsValue::GetList(
                    array("SORT" => "ASC"),
                    array(
                        "ORDER_ID" => $order,
                        "ORDER_PROPS_ID" => $arProps["ID"]
                    )
                );



                if ($arVals = $db_vals->Fetch()){

                    if($arVals['VALUE'] != '' && $arVals['VALUE'] != '[]') {

                        if ($code == 'ARR_ORDER_INFO'){

                            $arVals['VALUE'] = (array) json_decode($arVals['VALUE']);
                            $arrUniq = array();
                            $arrIndex = array();


                            foreach ($arVals['VALUE'] as $index) {
                                $index = (array) $index;
                                $arrIndex[] = $index['productxml'];
                            }

                            foreach ($arVals['VALUE'] as $key => $arVal) {
                                $arVal = (array) $arVal;

                                if ($value['id_order'] == $arVal['id_order'] && $value['productxml'] == $arVal['productxml'] && $value['id'] == $arVal['id']){
                                    if ($value['badge'] == ''){
                                        $value['badge'] = $arVal['badge'];
                                    }
                                    $arVals['VALUE'][$key] = $value;

                                    break;
                                }else{
                                    if (!in_array($value['productxml'],$arrUniq)){
                                        $arrUniq[$value['productxml']] = $value;
                                    }
                                }
                            }

                            if (count($arrUniq) > 1){
                                $arVals['VALUE'][] = array_unique($arrUniq[$value['productxml']]);
                            }else{
                                $arVals['VALUE'][] = $arrUniq[$value['productxml']];
                            }

                            $value = json_encode($arVals['VALUE']);
//                            echo "<pre>";print_r($value);die();
                        }



                        CSaleOrderPropsValue::Update($arVals['ID'], array("ORDER_ID" => $arVals['ORDER_ID'], "VALUE" => $value));
                        return true;

                    } else {

                        if ($code == 'ARR_ORDER_INFO'){
                            $arr[] = $value;
                            $arr = json_encode($arr);

                            CSaleOrderPropsValue::Update($arVals['ID'], array("ORDER_ID" => $arVals['ORDER_ID'], "VALUE" => $arr));

                        }else{

                            CSaleOrderPropsValue::Update($arVals['ID'], array("ORDER_ID" => $arVals['ORDER_ID'], "VALUE" => $value));

                        }
                    }
                }else{

                    if ($code == 'ARR_ORDER_INFO'){
                        $value[] = $value;
                        $value = json_encode($value);
                    }

                    $arFields = array(
                        "ORDER_ID" => $order,
                        "ORDER_PROPS_ID" => $arProps['ID'],
                        "NAME" => $arProps['NAME'],
                        "CODE" => $arProps['CODE'],
                        "VALUE" => $value
                    );



                    CSaleOrderPropsValue::Add($arFields);
                }
                return true;
            }
        }
    }
    
    public function getServicesList(){
        
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
        $arFilter = Array("IBLOCK_ID"=> 11, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>999), $arSelect);

        $services = array();

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $price = CPrice::GetBasePrice($arFields['ID']);
            $priceRub = priceRub($price['PRICE']."|".$price['CURRENCY']);

            $services[] = array(
                'name' => $arFields['NAME'],
                'id' => $arFields['ID'],
                'price' => round($priceRub)
            );

        }
        
        return $services;
        
    }
    
    public function getPriceOrder($order_id){

        $order_id = (int)$order_id;
        
        $result = $this->db->query("SELECT order_bitrix_id  FROM `order` WHERE order_id  = {$order_id}; ");
        
        if (count($result->rows) <= 0){
            return false;
        }

        $orderID = $result->row['order_bitrix_id'];
        $order = Sale\Order::load($orderID);
        
        return round($order->getField('PRICE'));
    }
    
}


