<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_Excursion extends Model{

    function __construct()
    {
        parent::__construct();
    }  
    

    public function addExcursion($data){

        $this->db->query('INSERT INTO `excursion_order` SET `excursion_id`= "'.$data['excursion_id'].'", `order_id` = "'.$data['order_id'].'", `people_order_id` = "'.$data['people_order_id'].'", `date_excursion` = "'.$data['date_excursion'].'", `time_excursion` = "'.$data['time_excursion'].'", `date_created` = NOW(), `status` = "active", `price` = "'.$data['price'].'"');

        $reserve_id = $this->db->getLastId();

        return $reserve_id;

    }

    public function getExcursionBy($data, $order_by, $page, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
                'excursion_order_id' => 'DESC'
            );
        }

        $sort = 'excursion_order_id DESC';

        if (count($order_by) > 0){
            foreach ($order_by as $key => $value){
                $sort = ' '.$key.' '.$value.' ';
            }
        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $where .= " `".$key."`= '".$value."' AND";
                }else{
                    $where .= " `".$key."`= '".$value."' ";
                }
                $i++;
            }
        }


        $result = $this->db->query("SELECT * FROM `excursion_order` WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else {
            return false;
        }

    }

    public function getTotalExcursionByDate($date){

        $result = $this->db->query("SELECT COUNT(*) AS excursion_count  FROM `excursion_order` WHERE `date_excursion` = '".$date."' AND `status` = 'active';");

        if ($result->row && count($result->row) > 0){
            return $result->row;
        }else{
            return false;
        }

    }

    public function updateExcursionOrder($excursion_order_id, $data){

        $result = $this->db->query("SELECT * FROM `excursion_order` WHERE `excursion_order_id` = ".(int)$excursion_order_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `excursion_order` SET ".$set." WHERE `excursion_order_id`=".(int)$excursion_order_id." ;");

            return $this->getTransportByID($transport_id);

        }else{
            return false;
        }

    }

    public function deleteExcursion($excursion_order_id){

        $data = array(
            'status' => 'delete'
        );

        $this->updateExcursionOrder($excursion_order_id, $data);
    }

}