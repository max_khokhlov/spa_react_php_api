<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_Hotel extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    private function clean($value = "") {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }

    public function addReserveHotel($data){

//        echo 'INSERT INTO `reserve_hotel` SET `name`= "'.$data['name'].'", `city_id` = "'.$data['city_id'].'", `hotel_id` = "'.$data['hotel_id'].'", `room_id` = "'.$data['room_id'].'", `date_start` = "'.$data['date_start'].'", `date_finish` = "'.$data['date_finish'].'", `original_price` = "'.$data['original_price'].'", `client_price` = "'.$data['client_price'].'",  `status` = "active"';


        $this->db->query('INSERT INTO `reserve_hotel` SET `name`= "'.$data['name'].'", `city_id` = "'.$data['city_id'].'", `hotel_id` = "'.$data['hotel_id'].'", `room_id` = "'.$data['room_id'].'", `date_start` = "'.$data['date_start'].'", `date_finish` = "'.$data['date_finish'].'", `original_price` = "'.$data['original_price'].'", `client_price` = "'.$data['client_price'].'",  `status` = "active"');


        $reserve_id = $this->db->getLastId();

        return $reserve_id;

    }

    public function addReserveHotelPeople($data, $reserve_id){
        
        $this->db->query('INSERT INTO `reserve_hotel_people` SET `reserve_hotel_id`= "'.$data['reserve_hotel_id'].'", `reserve_id` = "'.$reserve_id.'", `order_day_id` = "'.$data['order_day_id'].'", `people_order_id` = "'.$data['people_order_id'].'",  `date_reserve_start` = "'.$data['date_reserve_start'].'", `date_reserve_finish` = "'.$data['date_reserve_finish'].'", `date_created` = NOW(),  `status` = "'.$data['status'].'"');
        
        return $reserve_id;
    }
    
    public function getMaxReserveID($reserve = false){
        
        if ($reserve){

            $count = $this->db->query("SELECT count(*) AS count_row FROM `reserve_hotel_people`");

            if ($count->row['count_row'] > 0){
                $reserve_max = $this->db->query("SELECT MAX(reserve_id) AS max_id FROM `reserve_hotel_people`;");
                $reserve_id = $reserve_max->row['max_id'] + 1;
            }else{
                $reserve_id = 1;
            }
            
        }else{

            $count = $this->db->query("SELECT count(*) AS count_row FROM `hotel_people`");

            if ($count->row['count_row'] > 0){

                $reserve_max = $this->db->query("SELECT MAX(reserve_id) AS max_id FROM `hotel_people`;");
                $reserve_id = $reserve_max->row['max_id'] + 1;
                
            }else{
                $reserve_id = 1;
            }
            
        }

        return $reserve_id;
        
    }

    public function addHotelPeople($data, $reserve_id){

        $this->db->query('INSERT INTO `hotel_people` SET  `order_day_id` = "'.$data['order_day_id'].'", `reserve_id` = "'.$reserve_id.'",  `people_order_id` = "'.$data['people_order_id'].'", `date_reserve_start` = "'.$data['date_reserve_start'].'", `date_reserve_finish` = "'.$data['date_reserve_finish'].'",`hotel_id` = "'.$data['hotel_id'].'", `room_id` = "'.$data['room_id'].'", `date_created` = NOW(),  `status` = "'.$data['status'].'"');

        return $reserve_id;

    }

    public function addServices($data, $status = 'reserve'){

        if ($status == 'reserve'){
            $id_reserve_hotel = $this->db->query("SELECT reserve_hotel_people_id FROM reserve_hotel_people WHERE reserve_id = '{$data['hotel_reserve_id']}' ORDER BY reserve_hotel_people_id DESC LIMIT 1");
            $this->db->query('INSERT INTO `hotel_service` SET `reserve_hotel_id` = "'.$id_reserve_hotel->row['reserve_hotel_people_id'].'", `hotel_reserve_id`= "'.$data['hotel_reserve_id'].'", `service_id` = "'.$data['service_id'].'", `comment` = "'.$this->clean($data['comment']).'",   `status` = "'.(string)$status.'"');
        }else{
            $id_reserve_hotel = $this->db->query("SELECT hotel_people_id FROM hotel_people WHERE reserve_id = '{$data['hotel_reserve_id']}' ORDER BY hotel_people_id DESC LIMIT 1");
            $this->db->query('INSERT INTO `hotel_service` SET `reserve_hotel_id` = "'.$id_reserve_hotel->row['hotel_people_id'].'", `hotel_reserve_id`= "'.$data['hotel_reserve_id'].'", `service_id` = "'.$data['service_id'].'", `comment` = "'.$this->clean($data['comment']).'",   `status` = "'.(string)$status.'"');
        }

        $reserve_id = $this->db->getLastId();

        return $reserve_id;

    }

    public function getReserveBy($data, $order_by, $page, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
                'reserve_hotel_id' => 'DESC'
            );
        }

        $sort = 'reserve_hotel_id DESC';

        if (count($order_by) > 0){
            foreach ($order_by as $key => $value){
                $sort = ' '.$key.' '.$value.' ';
            }
        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){

                $not = strpos($value, '!=');

                if (count($data) > $i + 1){
                    if ($not === false){
                        $where .= " `".$key."`= '".$value."' AND";
                    }else{
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' AND";
                    }
                }else{
                    if ($not === false){
                        $where .= " `".$key."`= '".$value."' ";
                    }else{
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' ";
                    }
                }
                $i++;
            }
        }
        

        $result = $this->db->query("SELECT * FROM `reserve_hotel` WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else {
            return false;
        }

    }
    
    public function getCountReserve(){
        
        $result = $this->db->query("SELECT count(reserve_hotel_id) AS count_reserve FROM  `reserve_hotel` WHERE `status` != 'delete' ");
        
        if (isset($result->row)){
            return $result->row['count_reserve'];
        }else{
            return false;
        }
        
    }

    public function getListHotelPeopleBy($data, $order_by, $page, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
                'reserve_hotel_people_id' => 'DESC'
            );
        }

        $sort = 'reserve_hotel_people_id DESC';

        if (count($order_by) > 0){
            foreach ($order_by as $key => $value){
                $sort = ' '.$key.' '.$value.' ';
            }
        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $where .= " `".$key."`= '".$value."' AND";
                }else{
                    $where .= " `".$key."`= '".$value."' ";
                }
                $i++;
            }
        }


        $result = $this->db->query("SELECT * FROM `hotel_people` WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else {
            return false;
        }

    }

    public function getReserveByHotel($hotel_reserve_id){

        $result = $this->db->query("SELECT * FROM `reserve_hotel_people` WHERE `reserve_hotel_id` =".$hotel_reserve_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getReserveByID($reserve_id){

        $result = $this->db->query("SELECT * FROM `reserve_hotel_people` WHERE `reserve_hotel_people_id` =".$reserve_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getReserveHotelByID($hotel_reserve_id){

        $result = $this->db->query("SELECT * FROM `reserve_hotel` WHERE `reserve_hotel_id` =".$hotel_reserve_id." ;");

        if ($result->row && count($result->row) > 0){
            return $result->row;
        }else{
            return false;
        }


    }

    public function getHotelPeopleByID($hotel_people_id){

        $result = $this->db->query("SELECT * FROM `hotel_people` WHERE `hotel_people_id` =".$hotel_people_id." ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }


    }

    public function getReserveByDate($date_start, $date_finish){

        $d_start = ceil(strtotime($date_start) / (60*60*24));
        $d_finish = ceil(strtotime($date_finish) / (60*60*24));

        $interval = ($d_finish - $d_start) + 1;

        $data = array();

        //Промежуток выбора 1 год
        $result = $this->db->query("SELECT * FROM `reserve_hotel` WHERE date_start >= DATE_ADD('".$date_start."', INTERVAL - 12 MONTH) AND date_finish <= DATE_ADD('".$date_finish."', INTERVAL 12 MONTH) AND date_finish >= '".$date_start."' AND `status` = 'active';");

        if ($result->rows && count($result->rows) > 0){

            foreach ($result->rows as $reserve){

                $reserveDate = $this->db->query("SELECT date_reserve_start, date_reserve_finish FROM `reserve_hotel_people` WHERE (date_reserve_start >= DATE_ADD('".$date_finish."', INTERVAL - ".$interval." DAY ) OR date_reserve_finish >= DATE_ADD('".$date_finish."', INTERVAL  - ".$interval." DAY))  AND `reserve_hotel_id` = ".$reserve['reserve_hotel_id']."; ");

                if (count($reserveDate->rows) > 0){
                    $reserve['reserve'] = $reserveDate->rows;
                }

                $data[$reserve['city_id']][$reserve['reserve_hotel_id']] = $reserve;

            }

            return $data;

        }else{
            return false;
        }

    }

    public function getListReserveHotel(){

        $result = $this->db->query("SELECT * FROM `reserve_hotel` WHERE `status` = 'active' ORDER BY reserve_hotel_id  DESC ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getServices($hotel_people_id, $status = 'reserve'){

        $result = $this->db->query("SELECT * FROM `hotel_service` WHERE `hotel_people_id` = '".$hotel_people_id."' AND `status` = '".$status."' ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function updateReserveHotel($hotel_reserve_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_hotel` WHERE `reserve_hotel_id` = ".(int)$hotel_reserve_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            $set = '';


            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set .= " `".$key."` = '".$value."' ,";
                }else{
                    $set .= " `".$key."` = '".$value."' ";
                }
                $i++;
            }


            $result = $this->db->query("UPDATE `reserve_hotel` SET ".$set." WHERE `reserve_hotel_id`=".(int)$hotel_reserve_id." ;");

            if ($result){

                return true;

            }else{

                return false;

            }


        }else{
            return false;
        }

    }

    public function updateReserveHotelPeople($reserve_hotel_people_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_hotel_people` WHERE `reserve_hotel_people_id` = ".(int)$reserve_hotel_people_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `reserve_hotel_people` SET ".$set." WHERE `reserve_hotel_people_id`=".(int)$reserve_hotel_people_id." ;");

            return $this->getReserveByID($reserve_hotel_people_id);

        }else{
            return false;
        }

    }

    public function updateHotelPeople($hotel_people_id, $data){

    $result = $this->db->query("SELECT * FROM `hotel_people` WHERE `hotel_people_id` = ".(int)$hotel_people_id.";");

    if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

        $i = 0;
        foreach ($data as $key => $value){
            if (count($data) > $i + 1){
                $set = " `".$key."` = '".$value."' ,";
            }else{
                $set = " `".$key."` = '".$value."' ";
            }
            $i++;
        }

        $this->db->query("UPDATE `hotel_people` SET ".$set." WHERE `hotel_people_id`=".(int)$hotel_people_id." ;");

        return $this->getHotelPeopleByID($hotel_people_id);

    }else{
        return false;
    }

}

    public function deleteHotelPeople($order_day_id, $data){

        $result = $this->db->query("SELECT * FROM `hotel_people` WHERE `order_day_id` = ".(int)$order_day_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $where = '';
            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $where .= " `".$key."` = '".$value."' AND ";
                }else{
                    $where .= " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $hotel_people_id = $this->db->query("SELECT reserve_id FROM `hotel_people` WHERE ".$where." ;");

            if (isset($hotel_people_id->row['reserve_id'])){
                $this->db->query("DELETE FROM `hotel_service` WHERE `hotel_reserve_id`=".$hotel_people_id->row['reserve_id']." AND `status`= 'hotel' ;");
            }

            $this->db->query("DELETE FROM `hotel_people` WHERE ".$where." ;");

            return true;

        }else{
            return false;
        }

    }

    public function deleteReserveHotelPeople($order_day_id, $data){

        $result = $this->db->query("SELECT * FROM `reserve_hotel_people` WHERE `order_day_id` = ".(int)$order_day_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $where = '';
            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $where .= " `".$key."` = '".$value."' AND ";
                }else{
                    $where .= " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $hotel_people_id = $this->db->query("SELECT reserve_id FROM `reserve_hotel_people` WHERE ".$where." ;");

            if (isset($hotel_people_id->row['reserve_id'])){
                $this->db->query("DELETE FROM `hotel_service` WHERE `hotel_reserve_id`=".$hotel_people_id->row['reserve_id']." AND `status`= 'reserve' ;");
            }

            $this->db->query("DELETE FROM `reserve_hotel_people` WHERE ".$where." ;");

            return true;

        }else{
            return false;
        }

    }

    public function deleteReserveHotel($hotel_reserve_id){

        $data = array(
            'status' => 'delete'
        );

       $result = $this->updateReserveHotel($hotel_reserve_id, $data);

       if ($result){

           return true;

       }else{

           return false;

       }

    }

    public function deleteServices($hotel_service_id){
        try{
            $this->db->query("DELETE FROM `hotel_service` WHERE `hotel_service_id` = ".$hotel_service_id." ;");
            return true;
        }catch (Exception $e){
            return "Ошибка удаления";
        }
    }
}