<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_Statistic extends Model
{

    private $tables = array();

    function __construct()
    {
        parent::__construct();

        $this->model_bitrix = new model_Bitrix();
        $this->model_transport = new model_ReserveTransport();
        $this->model_people =  new model_People();
    }

    function getTables()
    {
        $output = array();
        $result = $this->db->query("SHOW TABLES FROM ".DB." ;");

        if (count($result->rows) > 0){

            foreach ($result->rows as $row) {

                $output[] = $row['Tables_in_'.DB];

            }

            return $output;

        }else{

            return false;

        }

    }

    function checkField($table, $field){

        $result = $this->db->query("SHOW COLUMNS FROM ".DB.".".$table." where `Field` = {$field} ");

        return $result;

    }

    function getColumnTables($table)
    {
        $output = array();
        $result = $this->db->query("SHOW COLUMNS FROM ".DB.".".$table." ;");

        if (count($result->rows) > 0){

            foreach ($result->rows as $row) {

                $output[] = array(
                    'name' => $row['Field']
                );

            }

            return $output;

        }else{
            return false;
        }

    }

    function getOptionDistinct($table)
    {

        $columns = $this->getColumnTables($table);
        $output = array();

        if (count($columns) > 0){

            foreach ($columns as $column) {

                $result = $this->db->query("SELECT DISTINCT(".$column['name'].") FROM `".$table."`");

                if (count($result->rows) > 0){

                    foreach ($result->rows as $row){

                        $array_not = array(
                            'comment', 'date_update'
                        );

                        if (!in_array($column['name'], $array_not)){

                            $output[$column['name']][] = $row[$column['name']];

                        }


                    }

                }

            }

            return $output;

        }else{

            return false;

        }


    }

    function getTypeColumnTable($table){

        $output = array();
        $result = $this->db->query("SHOW COLUMNS FROM ".DB.".".$table." ;");

        if (count($result->rows) > 0){

            foreach ($result->rows as $row) {

                $type = preg_replace('/( \( )?[0-9].*(\))/x', '', $row['Type']);

                $output[$row['Field']] = $type;

            }
            return $output;

        }else{

            return false;

        }
    }

    function getTypeFieldTable($table, $column){

        $result = $this->db->query("SHOW COLUMNS FROM ".DB.".".$table." WHERE Field = '".$column."' ;");

        if (isset($result->row['Type'])){
            return $result->row['Type'];
        }else{
            return false;
        }

    }


    function getTable($table_name, $params = false){

        $fields = $this->getColumnTables($table_name);
        $communication = array();
        $fieldsBitrixArray = array();
        $output = array();

        $fieldBitrix = array(
            'service_id',
            'room_id',
            'hotel_id',
            'order_bitrix_id',
            'tour_bitrix_id',
            'manager_id',
            'city_id',
            'city_start',
            'city_finish',
            'date_created',
            'date_update',
            'type_place_id',
            'transport_type_id',
            'people_group_id',
            'type',
            'type_place_id',
            'reserve_id',
            'status'
        );

        if (count($fields) > 0){

            foreach ($fields as $field) {

                if (!in_array($field['name'], $fieldBitrix)){

                    switch ($field['name']){
                        case 'type':
                            $field['name'] = 'transport_type_id';
                            break;
                        case 'type_place_id':
                            $field['name'] = 'reserve_type_place_id';
                            break;
                        case 'reserve_id':
                            $field['name'] = 'reserve_hotel_id';
                    }

                    if (preg_match("/(\_)(?=id)\w+/", $field['name'])){

                        if (count($this->tables) == 0){
                            $this->tables = $this->getTables();
                        }

                        foreach ($this->tables as $table){

                            $columns = $this->getColumnTables($table);

                            if (is_array($columns) || is_object($columns)){

                                foreach ($columns as $column) {

                                    if ($column['name'] == $field['name']){

                                        if (!in_array($table, $communication[$field['name']])){
                                            $communication[$field['name']][] = $table;
                                        }

                                    }

                                }

                            }

                        }

                    }

                }else{

                    switch ($field['name']){
                        case 'hotel_id':
                            $fieldsBitrixArray[$field['name']] = $this->model_bitrix->getHotelsList();
                            break;
                        case 'city_start':
                        case 'city_finish':
                        case 'city_id':
                            $fieldsBitrixArray[$field['name']] = $this->model_bitrix->getCities();
                            break;
                        case 'room_id':
                            $fieldsBitrixArray[$field['name']] = $this->model_bitrix->getRooms();
                            break;
                        case 'order_bitrix_id':
                            $fieldsBitrixArray[$field['name']] =  $this->model_bitrix->getTours();
                            break;
                        case 'manager_id':
                            $fieldsBitrixArray[$field['name']] =  $this->model_bitrix->getManager();
                            break;
                        case 'type_place_id':
                            $fieldsBitrixArray[$field['name']] =  $this->model_transport->getListPlaceTypeFilter();
                            $fieldsBitrixArray["reserve_".$field['name']] =  $this->model_transport->getListReserveTransportFilter();
                            break;
                        case 'reserve_type_place_id':
                            $fieldsBitrixArray[$field['name']] =  $this->model_transport->getListPlaceTypeFilter();
                            break;
                        case 'type':
                        case 'transport_type_id':
                            $fieldsBitrixArray[$field['name']] =  $this->model_transport->getListTypeTransportFilter();
                            break;
                        case 'people_group_id':
                            $fieldsBitrixArray[$field['name']] =  $this->model_people->getGroupListFilter();
                            break;

                    }

                }

            }

        }

        if (!$params){

            $result = $this->db->query("SELECT * FROM `{$table_name}` ");

        }else{

            $where = "";
            $i = 0;



            foreach ($params as $key => $value){

                if (count($value) == 0){
                    unset($value);
                }

                if (is_array($value) && count($value) > 0){

                    foreach ($value as $key => $val){

                        if (empty($val) || $val == -1){
                            unset($value[$key]);
                        }

                    }

                }else{

                    if (empty($value) || $value == -1){
                        unset($params->$key);
                    }

                }

            }

            foreach ($params as $key => $value){

                $type = $this->getTypeFieldTable($table_name, $key);

                if (is_array($value)){

                    switch ($type){
                        case 'date':
                        case 'datetime':
                            if ($i > 0){
                                $where .= $this->whereArrayMoreLess($key,$value, $type,true);
                            }else{
                                $where .= $this->whereArrayMoreLess($key,$value,$type,false);
                            }
                            break;
                        default:
                            $where .= $this->whereArrayEqually($key,$value, $i);
                            break;
                    }

                }else{
                    if ($type == 'float'){
                        switch ($params->simbol){
                            case 'equally':
                                if ($i > 1){
                                    $where .= ' OR '.' `'.$key.'`="'.$value.'"';
                                }else{
                                    $where .= ' '.' `'.$key.'`="'.$value.'"';
                                }
                                break;
                            case 'better':
                                if ($i > 1){
                                    $where .= ' OR '.' `'.$key.'`>="'.$value.'"';
                                }else{
                                    $where .= ' '.' `'.$key.'`>="'.$value.'"';
                                }
                                break;
                            case 'less':
                                if ($i > 1){
                                    $where .= ' OR '.' `'.$key.'`<="'.$value.'"';
                                }else{
                                    $where .= ' '.' `'.$key.'`<="'.$value.'"';
                                }
                                break;
                        }

                    }else{

                        if ($key !== 'simbol'){
                            if ($i > 0){
                                $where .= ' OR '.' `'.$key.'`="'.$value.'"';
                            }else{
                                $where .= ' '.' `'.$key.'`="'.$value.'"';
                            }
                        }

                    }

                }

                $i++;

            }

//            echo "<pre>";print_r($where);die();

            if (empty($where)){
                $result = $this->db->query("SELECT * FROM `{$table_name}`");
            }else{
                $result = $this->db->query("SELECT * FROM `{$table_name}` WHERE {$where}");
            }


        }

        if (count($result->rows) > 0){

            $output['data'] = $result->rows;
            $output['field_bitrix'] = $fieldsBitrixArray;
            $output['communication'] = $communication;



        }else{

            $output['data'] = [];
            $output['field_bitrix'] = $fieldsBitrixArray;
            $output['communication'] = $communication;

        }

        return $output;

    }

    function getTableJoin($table_join, $table, $params, $keyJoin){

        $where = "";
        $i = 0;
        $communication = array();
        $fieldsBitrixArray = array();
        $output = array();
        $fields = array();

        $fieldBitrix = array(
            'service_id',
            'room_id',
            'hotel_id',
            'order_bitrix_id',
            'tour_bitrix_id',
            'manager_id',
            'city_id',
            'city_start',
            'city_finish',
            'date_created',
            'date_update',
            'type_place_id',
            'transport_type_id',
            'people_group_id',
            'type',
            'type_place_id',
            'reserve_id',
            'status'
        );

        foreach ($params as $key => $value){

            if (count($value) == 0){
                unset($value);
            }

            if (is_array($value) && count($value) > 0){

                foreach ($value as $key => $val){

                    if (empty($val) || $val == -1){
                        unset($value[$key]);
                    }

                }

            }else{

                if (empty($value) || $value == -1){
                    unset($params->$key);
                }

            }

        }

        foreach ($params as $key => $value){

            $type = $this->getTypeFieldTable($table, $key);

            if (is_array($value)){

                switch ($type){
                    case 'date':
                    case 'datetime':
                        if ($i > 0){
                            $where .= $this->whereArrayJoinMoreLess($key,$value, $type,true);
                        }else{
                            $where .= $this->whereArrayJoinMoreLess($key,$value,$type,false);
                        }
                        break;
                    default:
                        $where .= $this->whereArrayJoinEqually($key,$value, $i);
                        break;
                }

            }else{
                if ($type == 'float'){

                    switch ($params->simbol){
                        case 'equally':
                            if ($i > 1){
                                $where .= ' OR '.' t1.'.$key.'="'.$value.'"';
                            }else{
                                $where .= ' '.' t1.'.$key.'="'.$value.'"';
                            }
                            break;
                        case 'better':
                            if ($i > 1){
                                $where .= ' OR '.' t1.'.$key.'>="'.$value.'"';
                            }else{
                                $where .= ' '.' t1.'.$key.'>="'.$value.'"';
                            }
                            break;
                        case 'less':
                            if ($i > 1){
                                $where .= ' OR '.' t1.'.$key.'<="'.$value.'"';
                            }else{
                                $where .= ' '.' t1.'.$key.'<="'.$value.'"';
                            }
                            break;
                    }

                }else{

                    if ($key !== 'simbol'){
                        if ($i > 0){
                            $where .= ' OR '.' t1.'.$key.'="'.$value.'"';
                        }else{
                            $where .= ' '.' t1.'.$key.'="'.$value.'"';
                        }
                    }

                }

            }

            $i++;

        }

        $result = $this->db->query("SELECT * FROM `{$table}` t1 LEFT JOIN `{$table_join}` t2 ON t1.{$keyJoin} = t2.{$keyJoin} WHERE {$where} ;", true);

        if (count($result->row) > 0){

            foreach ($result->row as $key_field => $value){
                $fields[] = $key_field;
            }

            if (count($fields) > 0){

                foreach ($fields as $field) {

                    if (!in_array($field, $fieldBitrix)){

                        switch ($field){
                            case 'type':
                                $field = 'transport_type_id';
                                break;
                            case 'type_place_id':
                                $field = 'reserve_type_place_id';
                                break;
                            case 'reserve_id':
                                $field = 'reserve_hotel_id';
                        }

                        if (preg_match("/(\_)(?=id)\w+/", $field)){

                            if (count($this->tables) == 0){
                                $this->tables = $this->getTables();
                            }

                            foreach ($this->tables as $table){

                                $columns = $this->getColumnTables($table);

                                foreach ($columns as $column) {

                                    if ($column['name'] == $field['name']){

                                        if (!in_array($table, $communication[$field])){
                                            $communication[$field][] = $table;
                                        }

                                    }

                                }

                            }

                        }

                    }else{

                        switch ($field){
                            case 'hotel_id':
                                $fieldsBitrixArray[$field] = $this->model_bitrix->getHotelsList();
                                break;
                            case 'city_start':
                            case 'city_finish':
                            case 'city_id':
                                $fieldsBitrixArray[$field] = $this->model_bitrix->getCities();
                                break;
                            case 'room_id':
                                $fieldsBitrixArray[$field] = $this->model_bitrix->getRooms();
                                break;
                            case 'order_bitrix_id':
                                $fieldsBitrixArray[$field] =  $this->model_bitrix->getTours();
                                break;
                            case 'manager_id':
                                $fieldsBitrixArray[$field] =  $this->model_bitrix->getManager();
                                break;
                            case 'type_place_id':
                                $fieldsBitrixArray[$field] =  $this->model_transport->getListPlaceTypeFilter();
                                $fieldsBitrixArray["reserve_".$field] =  $this->model_transport->getListReserveTransportFilter();
                                break;
                            case 'reserve_type_place_id':
                                $fieldsBitrixArray[$field] =  $this->model_transport->getListPlaceTypeFilter();
                                break;
                            case 'type':
                            case 'transport_type_id':
                                $fieldsBitrixArray[$field] =  $this->model_transport->getListTypeTransportFilter();
                                break;
                            case 'people_group_id':
                                $fieldsBitrixArray[$field] =  $this->model_people->getGroupListFilter();
                                break;

                        }

                    }

                }

            }

        }

        if (count($result->rows) > 0){

            $option_fields = array();

            foreach ($fields as $field) {

                $option_fields[$field] = array();

            }

            $output['data'] = $result->rows;
            $output['fields'] = $option_fields;
            $output['field_bitrix'] = $fieldsBitrixArray;
            $output['communication'] = $communication;



        }else{

            $output['data'] = [];
            $output['fields'] = [];
            $output['field_bitrix'] = $fieldsBitrixArray;
            $output['communication'] = $communication;

        }

        return $output;


    }

    function whereArrayMoreLess($key, $value, $type ,$and = false){

        if ($type == 'date'){

            if ($and){
                $where = ' OR ('.' `'.$key.'`>="'.date("Y-m-d", $value[0]).'" AND `'.$key.'`<= "'.date("Y-m-d", $value[1]).'") ';
            }else{
                $where = ' ('.' `'.$key.'`>="'.date("Y-m-d", $value[0]).'" AND `'.$key.'`<= "'.date("Y-m-d", $value[1]).'") ';
            }

        }else if ($type == 'datetime'){

            if ($and){
                $where = ' OR ('.' `'.$key.'`>="'.date("Y-m-d H:i:s", $value[0]).'" AND `'.$key.'`<= "'.date("Y-m-d H:i:s", $value[1]).'") ';
            }else{
                $where = ' ('.' `'.$key.'`>="'.date("Y-m-d H:i:s", $value[0]).'" AND `'.$key.'`<= "'.date("Y-m-d H:i:s", $value[1]).'") ';
            }

        }


        return $where;

    }

    function whereArrayEqually($key, $value, $index){

        $where = '';
        $i = 0;

        foreach ($value as $item){

            if ($i > 0){
                $where .= ' OR `'.$key.'`= "'.$item.'" ';
            }else{

                if ($index > 0){
                    $where .= ' OR `'.$key.'`= "'.$item.'" ';
                }else{
                    $where .= ' `'.$key.'`= "'.$item.'" ';
                }

            }

            $i++;

        }

        return $where;

    }

    function whereArrayJoinMoreLess($key, $value, $type ,$and = false){

        if ($type == 'date'){

            if ($and){
                $where = ' OR ('.' t1.'.$key.'>="'.date("Y-m-d", $value[0]).'" AND t1.'.$key.'<= "'.date("Y-m-d", $value[1]).'") ';
            }else{
                $where = ' ('.' t1.'.$key.'>="'.date("Y-m-d", $value[0]).'" AND t1.'.$key.'<= "'.date("Y-m-d", $value[1]).'") ';
            }

        }else if ($type == 'datetime'){

            if ($and){
                $where = ' OR ('.' t1.'.$key.'>="'.date("Y-m-d H:i:s", $value[0]).'" AND t1.'.$key.'<= "'.date("Y-m-d H:i:s", $value[1]).'") ';
            }else{
                $where = ' ('.' t1.'.$key.'>="'.date("Y-m-d H:i:s", $value[0]).'" AND t1.'.$key.'<= "'.date("Y-m-d H:i:s", $value[1]).'") ';
            }

        }


        return $where;

    }

    function whereArrayJoinEqually($key, $value, $index){

        $where = '';
        $i = 0;

        foreach ($value as $item){

            if ($i > 0){
                $where .= ' OR t1.'.$key.'= "'.$item.'" ';
            }else{

                if ($index > 0){
                    $where .= ' OR t1.'.$key.'= "'.$item.'" ';
                }else{
                    $where .= ' t1.'.$key.'= "'.$item.'" ';
                }

            }

            $i++;

        }

        return $where;

    }

}

