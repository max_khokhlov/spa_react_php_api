<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_Document extends Model{

    function __construct()
    {
        parent::__construct();
    }

    public function addTypeDocument($data){

        $this->db->query('INSERT INTO `document_type` SET `name`= "'.$data['name'].'", `hotel_active` = "'.$data['hotel_active'].'", `tour_active` = "'.$data['tour_active'].'", `url_default` = "'.$data['url_default'].'", `status` = "active"');

        $orderId = $this->db->getLastId();

        return $orderId;

    }

    public function addDocumentPeople($data){

        $this->db->query('INSERT INTO `document_order` SET `name`= "'.$data['name'].'", `document_type_id` = "'.$data['document_type_id'].'", `order_id` = "'.$data['order_id'].'", `people_order_id` = "'.$data['people_order_id'].'", `status` = "'.$data['status'].'", `url`= "'.$data['url'].'"; ');

        $orderId = $this->db->getLastId();

        return $orderId;

    }

    public function getListDocumentType(){

        $result = $this->db->query("SELECT * FROM `document_type` WHERE `status` = 'active' ");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getDocumentTypeByID($document_type_id){

        $result = $this->db->query("SELECT * FROM `document_type` WHERE `document_type_id` = ".(int)$document_type_id." ");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getDocumentByPeopleID($peopleId){

        $result = $this->db->query("SELECT * FROM `document_order` WHERE `people_order_id`= '".$peopleId."' ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }


    }

    public function getDocumentByOrderID($order_id){

        $result = $this->db->query("SELECT * FROM `document_order` WHERE `order_id`= '".$order_id."' ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getDocumentByID($document_order_id){

        $result = $this->db->query("SELECT * FROM `document_order` WHERE `document_order_id`= '".$document_order_id."' ;");

        if ($result->rows && count($result->rows) > 0){
            return $result->rows;
        }else{
            return false;
        }

    }

    public function getDocumentBy($data, $order_by, $page, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
                'order_id' => 'DESC'
            );
        }

        $sort = 'order_id DESC';

        if (count($order_by) > 0){
            foreach ($order_by as $key => $value){
                $sort = ' '.$key.' '.$value.' ';
            }
        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $where .= " `".$key."`= '".$value."' AND";
                }else{
                    $where .= " `".$key."`= '".$value."' ";
                }
                $i++;
            }
        }


        $result = $this->db->query("SELECT * FROM `document_order` WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else {
            return false;
        }

    }

    public function updateDocumentType($document_type_id, $data){

        $result = $this->db->query("SELECT * FROM `document_type` WHERE `document_type_id` = ".(int)$document_type_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `document_type` SET ".$set." WHERE `document_type_id`=".(int)$document_type_id." ;");

            return $this->getDocumentTypeById($document_type_id);

        }else{
            return false;
        }

    }

    public function updateDocumentOrder($document_order_id, $data){

        $result = $this->db->query("SELECT * FROM `document_order` WHERE `document_order_id` = ".(int)$document_order_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `document_order` SET ".$set." WHERE `document_order_id`=".(int)$document_order_id." ;");

            return $this->getDocumentByID($document_order_id);

        }else{
            return false;
        }

    }

    public function deleteDocumentType($document_type_id){

        $data = array(
            'status' => 'delete'
        );

        $this->updateDocumentType($document_type_id, $data);

        return true;

    }

    public function deleteDocumentOrder($document_order_id){

        $data = array(
            'status' => 'delete'
        );

        $this->updateDocumentOrder($document_order_id, $data);

        return true;

    }


}