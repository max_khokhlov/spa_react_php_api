<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_People extends Model{

    function __construct()
    {
        parent::__construct();
    }

    public function addPeople($data){

        $this->db->query('INSERT INTO `people_order` SET  `name` = "'.$data['name'].'" , `email` = "'.$data['email'].'" , `phone` = "'.$data['phone'].'" , `people_group_id` = "'.$data['group_id'].'" , `comment` = "'.$data['comment'].'" , `status` = "'.$data['status'].'" , `date_created` = NOW(), `order_id` = "'.$data['order_id'].'" ;');

        $peopleId = $this->db->getLastId();

        $this->db->query('INSERT INTO `people_data_order` SET `bith_date` = "'.$data['bith_date'].'", `sex` = "'.$data['sex'].'", `citizenship` = "'.$data['citizenship'].'", `passport` = "'.$data['passport'].'", `date_of_issue` = "'.$data['date_of_issue'].'", `date_expires`=  "'.$data['date_expires'].'", `people_order_id` =  "'.$peopleId.'";');

        return $peopleId;

    }

    public function addPeopleGroup($data){

        $this->db->query('INSERT INTO `people_group` SET  `name` = "'.$data['name'].'" , `type` = "'.$data['type'].'", `sale` = "'.$data['sale'].'" active = 1;');

        $groupId = $this->db->getLastId();

        return $groupId;

    }

    public function getPeople($people_order_id){

        $result = $this->db->query("SELECT * FROM  `people_order` po LEFT JOIN  `people_data_order` pdo ON po.people_order_id = pdo.people_order_id WHERE po.people_order_id = ".$people_order_id." ;");



        if (isset($result->row) && count($result->row) > 0){

            $order_status = $this->db->query("SELECT `status` FROM `order` WHERE `order_id` = ".$result->row['order_id']);

            $result->row['order_status'] = $order_status->row['status'];

            return $result->row;

        }else{
            return false;
        }

    }

    public function getPeopleByOrderId($order_id){

        $output = array();
        $result = $this->db->query("SELECT * FROM  `people_order` po LEFT JOIN  `people_data_order` pdo ON po.people_order_id = pdo.people_order_id WHERE po.order_id = ".$order_id." AND po.status != 'delete' ;");

        if (isset($result->rows) && count($result->rows) > 0){

            foreach ($result->rows as $row){
                $output[$row['people_order_id']] = $row;
            }

            return $output;

        }else{
            return false;
        }

    }

    public function getGroupByID($group_id){

        $result = $this->db->query("SELECT * FROM `people_group` WHERE `people_group_id` = ".$group_id." ;");

        if (isset($result->rows) && count($result->rows) > 0){

            return $result->rows;

        }else{
            return false;
        }

    }

    public function getGroupList(){

        $result = $this->db->query("SELECT * FROM `people_group`;");

        if (isset($result->rows) && count($result->rows) > 0){

            return $result->rows;

        }else{
            return false;
        }

    }


    public function getGroupListFilter(){

        $output = array();
        $result = $this->db->query("SELECT * FROM `people_group`;");

        if (isset($result->rows) && count($result->rows) > 0){

            foreach ($result->rows as $row){

                $output[$row['people_group_id']] = array(
                    'name'=> $row['name'],
                    'id' => $row['people_group_id']
                );

            }


            return $output;

        }else{
            return false;
        }

    }

    public function getPeopleByGroupID($group_id){

        $result = $this->db->query("SELECT * FROM `people_order` po LEFT JOIN  `people_data_order` pdo ON po.people_order_id = pdo.people_order_id WHERE po.people_group_id = ".$group_id." ;");

        if (isset($result->rows) && count($result->rows) > 0){

            return $result->rows;

        }else{
            return false;
        }

    }

    public function getPeopleBy($data, $order_by = array(), $page = 0, $count = 20){

        if (count($order_by) <= 0){
            $order_by = array(
                'order_id' => 'DESC'
            );
        }

        $sort = 'order_id DESC';

        if (count($order_by) > 0){

            foreach ($order_by as $key => $value){

                $sort = ' '.$key.' '.$value.' ';

            }

        }

        if ($count > 0 && $page > 0) {
            $count = " ORDER BY ".$sort." LIMIT " . $page * $count . "," . $count . " ;";
        }else{
            $count = " ORDER BY ".$sort." LIMIT 0,".$count." ;";
        }

        $where = '';
        $i = 0;

        if (is_array($data) && count($data) > 0){
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $not = strpos('!=', $value);
                    if ($not === false){
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`= '".$value."' AND";
                    }else{
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' AND";
                    }
                }else{
                    $not = strpos('!=', $value);
                    if ($not === false){
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' ";
                    }else{
                        $value = str_replace('!=', '', $value);
                        $where .= " `".$key."`!= '".$value."' ";
                    }
                }
                $i++;
            }
        }

        $result = $this->db->query("SELECT * FROM `people_order` po LEFT JOIN `people_data_order` pdo ON po.people_order_id = pdo.people_order_id WHERE ".$where." ".$count);

        if (isset($result->rows) && count($result->rows) > 0){

            $output = $result->rows;

            return $output;

        } else {

            return false;

        }

    }

    public function updateGroup($group_id, $data){

        $result = $this->db->query("SELECT * FROM `people_group` WHERE `people_group_id` = ".(int)$group_id.";");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            foreach ($data as $key => $value){
                if (count($data) > $i + 1){
                    $set = " `".$key."` = '".$value."' ,";
                }else{
                    $set = " `".$key."` = '".$value."' ";
                }
                $i++;
            }

            $this->db->query("UPDATE `people_group` SET ".$set." WHERE `people_group_id`=".(int)$group_id." ;");

            return $this->getGroupByID($group_id);

        }else{
            return false;
        }
    }

    public function updatePeople($people_order_id, $data){

        $result = $this->db->query("SELECT * FROM `people_order` po LEFT JOIN  `people_data_order` pdo ON po.people_order_id = pdo.people_order_id WHERE po.people_order_id = ".(int)$people_order_id." ;");

        if (isset($result->row) && count($result->row) > 0 && count($data) > 0){

            $i = 0;
            $set = '';

            foreach ($data as $key => $value){
                if (count($data) > ($i + 1)){
                    $set .= " `".$key."` = '".$value."' ,";
                }else{
                    $set .= " `".$key."` = '".$value."' ";
                }
                $i++;
            }

//            echo $people_order_id." ".$set;die();
            
            $this->db->query("UPDATE `people_order` po LEFT OUTER JOIN `people_data_order` pdo ON po.people_order_id = pdo.people_order_id SET ".$set." WHERE po.people_order_id =".(int)$people_order_id." ;");

            return true;

        }else{
            return false;
        }

    }

    public function deletePeopleToOrder($people_order_id, $order_id){

        $result = $this->db->query("SELECT * FROM `people_order` po LEFT JOIN  `people_data_order` pdo ON po.people_order_id = pdo.people_order_id WHERE po.people_order_id = ".(int)$people_order_id." ;");

        if (isset($result->row) && count($result->row) > 0 ){

            $this->db->query("UPDATE `people_order` SET `order_id` = 0 WHERE people_order_id =".(int)$people_order_id." AND `order_id` = ".(int)$order_id." ;");

            return true;

        }else{
            return false;
        }

    }

    public function deletePeople($people_order_id){

        $data = array(
          'status' => 'delete'
        );

        $this->updatePeople($people_order_id, $data);

        return true;
    }

    public function deleteGroup($group_id){

        $data = array(
            'active' => 0
        );

        $this->updateGroup($group_id, $data);

        return true;
    }

}