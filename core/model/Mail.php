<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class model_Mail {

    private $email = array();
    private $mess;
    private $from_name;
    private $type =  "text/html";
    private $charset = 'UTF-8';
    private $from_email;
    private $notify;

    public function __construct($from_email)
    {
        $this->from_email = $from_email;
    }

    public function setNotify($notify){
        $this->notify = $notify;
    }

    public function setFrom($from_email){
        $this->from_email = $from_email;
    }

    public function setType($type){
        $this->type = $type;
    }

    public function setName($name){
        $this->from_name = $name;
    }

    private function mime_header_encode($str, $data_charset, $send_charset) {
        if ($data_charset != $send_charset) {
            $str = iconv($data_charset, $send_charset, $str);
        }
        return '=?' . $send_charset . '?B?' . base64_encode($str) . '?=';
    }

    private function send_mime_mail($name_from, // имя отправителя
                                    $email_from, // email отправителя
                                    $name_to, // имя получателя
                                    $email_to, // email получателя
                                    $data_charset, // кодировка переданных данных
                                    $send_charset, // кодировка письма
                                    $subject, // тема письма
                                    $body, // текст письма
                                    $html = true, // письмо в виде html или обычного текста
                                    $reply_to = FALSE) {
        $to      =  $this->mime_header_encode($name_to, $data_charset, $send_charset) . ' <' . $email_to . '>';
        $subject =  $this->mime_header_encode($subject, $data_charset, $send_charset);
        $from    =  $this->mime_header_encode($name_from, $data_charset, $send_charset) . ' <' . $email_from . '>';

        if ($data_charset != $send_charset) {
            $body = iconv($data_charset, $send_charset, $body);
        }

        $headers = "From: $from\r\n";
        $type    = ($html) ? 'html' : 'plain';
        $headers .= "Content-type: $this->type; charset=$send_charset\r\n";
        $headers .= "Mime-Version: 1.0\r\n";

        if ($reply_to) {
            $headers .= "Reply-To: $reply_to";
        }
        if ($this->notify){
            $headers .= "Disposition-Notification-To: ".$this->from_email."\r\n";
        }

        return mail($to, $subject, $body, $headers);
    }
    
    function getTemplates($file, $fields = array()){
        
        if (!file_exists($file)){
            return "Нет файла";
        }
        
        $template = file_get_contents($file);
        
        foreach ($fields as $key => $value){
            
            $template = str_replace('{{'.$key.'}}', $value, $template);
            
        }
        
        return $template;
    }

    function Send($tos, $subject, $mess) {

        if (is_array($tos)){
            foreach ($tos as $to) {
                $this->send_mime_mail($this->from_name, $this->from_email, '', $to, $this->charset, $this->charset, $subject, $mess);
            }
        }else{
            $this->send_mime_mail($this->from_name, $this->from_email, '', $tos, $this->charset, $this->charset, $subject, $mess);

        }

        return true;
    }


}