<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class Hotel{

    private $params;
    public $model_order;
    public $model_people;
    public $model_bitrix;
    private $db;
    public $model_hotel;

    public function __construct($params)
    {
       
        $this->params = $params;
        $this->model_order = new model_Order();
        $this->model_people = new model_People();
        $this->model_bitrix = new model_Bitrix();
        $this->model_hotel = new model_Hotel();
    }

    public function getHotelInfoAction(){

        if (isset($this->params['status'])){

            $return = array();
            $return['city'] = $this->model_bitrix->getCities();
            $return['hotels'] = $this->model_bitrix->getHotels();
            $return['count_reserve'] = $this->model_hotel->getCountReserve();

            return $return;

        }else{

            return false;

        }

    }

    public function getReserveListAction(){

        $return = array();
        $order_by = array();
        $page = 0;
        $pageSize = 10;

        if (!isset($this->params['status'])){
            $this->params['status'] = '!=delete';
        }
        
        if (isset($this->params['page'])){
            $page = (int)$this->params['page'] - 1;
        }
        
        if (isset($this->params['pageSize'])){
            $pageSize = $this->params['pageSize'];
        }

        $data = array(
            'status' => $this->params['status']
        );
        
        $result = $this->model_hotel->getReserveBy($data, $order_by, $page, $pageSize);
        
        //Список всех резервов
//        $result = $this->model_hotel->getListReserveHotel();

        if ($result){

            foreach ($result as $item) {

                $return[] = array(
                    'name' => $item['name'],
                    'reserve_hotel_id' => $item['reserve_hotel_id'],
                    'city' => $this->model_bitrix->getCityByID($item['city_id']),
                    'hotel' => $this->model_bitrix->getHotelByID($item['hotel_id']),
                    'room_id' => $item['room_id'],
                    'room' => $this->model_bitrix->getRoomByID($item['room_id']),
                    'date_start' => $item['date_start'],
                    'date_finish' => $item['date_finish']
                );

            }

            return $return;

        }else{
            return false;
        }

    }

    public function getReserveAction(){

        if (isset($this->params['reserve_hotel_id'])){

            $result = $this->model_hotel->getReserveHotelByID($this->params['reserve_hotel_id']);
            $reserve = $this->model_hotel->getReserveByHotel($this->params['reserve_hotel_id']);

            if (count($reserve) > 0 && (is_array($reserve)||is_object($reserve))){
                
                foreach ($reserve as $item){

                    $item['people'] = $this->model_people->getPeople($item['people_order_id']);
                    $result['reserve'][] = $item;

                }

            }

            if ($result){

                return $result;

            }else{

                return false;

            }

        }else{

            return false;

        }

    }

    public function deleteReserveHotelforDayAction(){

        if (isset($this->params['reserve_hotel_id']) && isset($this->params['order_day_id'])){

            $order_day_id = $this->params['order_day_id'];

            $data = array(
                'reserve_hotel_id' => $this->params['reserve_hotel_id'],
                'order_day_id' => $this->params['order_day_id'],
                'date_reserve_start' => $this->params['date_reserve_start'],
                'date_reserve_finish' => $this->params['date_reserve_finish'],
            );

            foreach ($this->params['peoples'] as $people){

                $data['people_order_id'] = $people;
                
                $this->model_bitrix->deleteProductBasketToDay($order_day_id, $data);
                $this->model_hotel->deleteReserveHotelPeople($order_day_id, $data);

            }


            return true;

        }else{

            return false;

        }




    }

    public function deleteHotelforDayAction(){


        if (isset($this->params['hotel_people_id']) && isset($this->params['order_day_id'])){

            $order_day_id = $this->params['order_day_id'];

            $data = array(
               'order_day_id' => $this->params['order_day_id'],
               'date_reserve_start' => $this->params['date_reserve_start'],
               'date_reserve_finish' => $this->params['date_reserve_finish'],
               'hotel_id' => $this->params['hotel_id'],
               'room_id' => $this->params['room_id']
            );

            foreach ($this->params['peoples'] as $people){

                $data['people_order_id'] = $people;

                $this->model_bitrix->deleteProductBasketToDay($order_day_id, $data);
                $this->model_hotel->deleteHotelPeople($order_day_id, $data);

            }


        }

        return true;

    }


    public function addReserveHotelAction(){
        
        $array_product_id = array();
        $array_product_id['rooms'][] = array(
            'id' => $this->params['room_id']
        );
        
        if (count($this->params['services']) > 0){
            foreach ($this->params['services'] as $value){
                $array_product_id['services'][] = array(
                    'id' => $value->id,
                    'comments' => $value->comments
                );
            }
        }
        
        $this->model_bitrix->updateBasketOrder($this->params['order_day_id'], $array_product_id);
        
        if ($this->params['reserve'] === false){

            $reserve_id = $this->model_hotel->getMaxReserveID();

            $data = array(
                'hotel_id' => $this->params['hotelId'],
                'room_id' => $this->params['room_id'],
                'order_day_id' => $this->params['order_day_id'],
                'date_reserve_start' => $this->params['date_reserve_start'],
                'date_reserve_finish' => $this->params['date_reserve_finish'],
                'status' => 'active'
            );

            foreach ($this->params['peoples'] as $people) {

                $data['people_order_id'] = $people;

                $this->model_hotel->addHotelPeople($data, $reserve_id);
                
            }

            if (count($this->params['services']) > 0){
                foreach ($this->params['services'] as $value){

                    $data = array(
                       'hotel_reserve_id' => $reserve_id,
                       'service_id' => $value->id,
                       'comment' => $value->comments
                    );

                    $this->model_hotel->addServices($data, 'hotel');

                }
            }

            return true;

        }else{

            $reserve_id = $this->model_hotel->getMaxReserveID(true);

            $data = array(
                'reserve_hotel_id' => $this->params['hotelId'],
                'order_day_id' => $this->params['order_day_id'],
                'date_reserve_start' => $this->params['date_reserve_start'],
                'date_reserve_finish' => $this->params['date_reserve_finish'],
                'status' => 'active'
            );

            foreach ($this->params['peoples'] as $people) {

                $data['people_order_id'] = $people;

                $this->model_hotel->addReserveHotelPeople($data, $reserve_id);
            }

            if (count($this->params['services']) > 0){

                foreach ($this->params['services'] as $value){

                    $data = array(
                        'hotel_reserve_id' => $reserve_id,
                        'service_id' => $value->id,
                        'comment' => $value->comments
                    );

                    $this->model_hotel->addServices($data);

                }
            }

            return true;

        }


    }



    public function addReserveAction(){


        if (count($this->params) && isset($this->params['room_id'])){

            $data = array(

                'name' => $this->params['name'],
                'city_id' => $this->params['city'],
                'date_start' => $this->params['date_start'],
                'date_finish' => $this->params['date_finish'],
                'hotel_id' => $this->params['hotel_id'],
                'room_id' => $this->params['room_id'],
                'original_price' => $this->params['original_price'],
                'client_price' => $this->params['client_price'],


            );

            $result = $this->model_hotel->addReserveHotel($data);

            if ($result){

                return $this->getReserveListAction();

            }

        }

    }


    public function deleteReserveAction(){

        if (isset($this->params['reserve_hotel_id'])){

            $result = $this->model_hotel->deleteReserveHotel((int)$this->params['reserve_hotel_id']);

            if ($result){

                return $this->getReserveListAction();

            }

        }

    }


    public function updateReserveAction(){

        if (isset($this->params['reserve_hotel_id'])){

            $data = array(
                'reserve_hotel_id' => $this->params['reserve_hotel_id'],
                'name' => $this->params['name']
            );

            $result = $this->model_hotel->updateReserveHotel($this->params['reserve_hotel_id'], $data);

            if ($result){

              return $this->getReserveAction();

            }

        }

    }

}