<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/manager/core/config.php';

class Order
{

    private $params;
    public $model_order;
    public $model_people;
    public $model_bitrix;
    public $model_hotel;
    public $model_services;
    public $diff_number;

    public function __construct($params)
    {
        
        $this->params = $params;
        $this->model_order = new model_Order();
        $this->model_people = new model_People();
        $this->model_bitrix = new model_Bitrix();
        $this->model_hotel = new model_Hotel();
        $this->model_services = new model_Services();
        $this->model_transport = new model_ReserveTransport();
    }

    public function addOrderAction()
    {

        $data = array();

        if (!isset($this->params['user'])) {
            return false;
        }

        $this->params['user'] = (array)$this->params['user'];

        if (!isset($this->params['user']['bitrix_id'])) {

            $check_user = $this->model_bitrix->checkUser($this->params['user']);

            if ($check_user['status'] == 'error') {
                $user_id = $this->model_bitrix->addUser($this->params['user']);
            } else {
                $user_id = $check_user['user_bitrix_id'];
            }

        } else {
            $user_id = $this->params['user']['bitrix_id'];
        }

        if (!$user_id) {
            return false;
        }

        $bitrix_order_id = $this->model_bitrix->addOrder($user_id);

        if ($this->params['mode'] == 'tour') {

            $data = array(
                'orderBitrixId' => $bitrix_order_id,
                'orderName' => $this->params['name'],
                'tour_bitrix_id' => $this->params['tour'],
                'is_tour' => 1,
                'orderDate' => date('Y-m-d H:i:s'),
                'orderDateStart' => $this->params['date_start'],
                'orderDateFinish' => $this->params['date_start'],
                'managerId' => $this->params['manager_id'],
                'orderStatus' => 'new',
                'orderTotal' => 0,
                'comment' => 'Напишите комментарий к новому заказу'
            );

            if ($this->params['tour'] != 0) {

                $days = $this->model_bitrix->getDayByTourID($this->params['tour']);
                $dataDays = array();
                $i = 0;

                foreach ($days as $day) {

                    if ($i <= 0) {
                        $day_start = $this->params['date_start'];
                        $day_finish = date('Y-m-d', strtotime($this->params['date_start']) + (($day['diff'] - 1) * 24 * 3600));
                        $number = '1-' . $day['diff'];
                        $this->diff_number = $day['diff'] + 1;
                    } else {
                        $day_start = date('Y-m-d', strtotime($dataDays[$i - 1]['date_finish']) + (24 * 3600));
                        $day_finish = date('Y-m-d', strtotime($day_start) + (($day['diff'] - 1) * 24 * 3600));
                        $number = $this->diff_number . "-" . ($this->diff_number + $day['diff'] - 1);
                        $this->diff_number += $day['diff'] + 1;
                    }

                    $dataDays[] = array(
                        'date_start' => $day_start,
                        'date_finish' => $day_finish,
                        'city_id' => $day['city'],
                        'number' => $number
                    );

                    $i++;

                }

                $data = array(
                    'orderBitrixId' => $bitrix_order_id,
                    'orderName' => $this->params['name'],
                    'tour_bitrix_id' => $this->params['tour'],
                    'is_tour' => 1,
                    'orderDate' => date('Y-m-d H:i:s'),
                    'orderDateStart' => $this->params['date_start'],
                    'orderDateFinish' => $dataDays[count($dataDays) - 1]['date_finish'],
                    'managerId' => $this->params['manager_id'],
                    'orderStatus' => 'new',
                    'orderTotal' => 0,
                    'comment' => 'Напишите комментарий к новому заказу',
                    'days' => $dataDays
                );


            }

        }

        if ($this->params['mode'] == 'hotel') {

            $dataDays[] = array(
                'date_start' => $this->params['date_start'],
                'date_finish' => $this->params['date_finish'],
                'city_id' => $this->params['city'],
                'number' => 1
            );

            $data = array(
                'orderBitrixId' => $bitrix_order_id,
                'orderName' => $this->params['name'],
                'tour_bitrix_id' => -1,
                'is_tour' => 0,
                'orderDate' => date('Y-m-d H:i:s'),
                'orderDateStart' => $this->params['date_start'],
                'orderDateFinish' => $this->params['date_finish'],
                'managerId' => $this->params['manager_id'],
                'orderStatus' => 'new',
                'orderTotal' => 0,
                'comment' => 'Напишите комментарий к бронированию отеля',
                'days' => $dataDays
            );

        }

        if (count($data) > 0) {

            $order_id = $this->model_order->addOrder($data);

            if ($order_id) {

                $date_default = date('Y-m-d');

                $data = array(
                    'name' => $this->params['user']['surname'] . " " . $this->params['user']['firstname'],
                    'email' => $this->params['user']['email'],
                    'phone' => $this->params['user']['phone'],
                    'group_id' => 1,
                    'comment' => "ВНИМАНИЕ! ОБНОВИТЕ ДАННЫЕ ПО УМОЛЧАНИЮ",
                    'status' => "active",
                    'order_id' => $order_id,
                    'bith_date' => $date_default,
                    'sex' => 'male',
                    'citizenship' => 1,
                    'passport' => '',
                    'date_of_issue' => $date_default,
                    'date_expires' => $date_default,
                );

                $this->model_people->addPeople($data);
            }

        } else {
            return false;
        }


        return $this->getOrdersAction();

    }

    public function addOrderFromSiteAction(){

        if (count($this->params['data']) > 0) {
            
            $data = (array)$this->params['data'];
            
            $order_id = $this->model_order->addOrder($data);

            if ($order_id) {

                $date_default = date('Y-m-d');
                $this->params['user'] = (array)$this->params['user'];

                $data = array(
                    'name' => $this->params['user']['surname'] . " " . $this->params['user']['firstname'],
                    'email' => $this->params['user']['email'],
                    'phone' => $this->params['user']['phone'],
                    'group_id' => 1,
                    'comment' => "ВНИМАНИЕ! ОБНОВИТЕ ДАННЫЕ ПО УМОЛЧАНИЮ",
                    'status' => "active",
                    'order_id' => $order_id,
                    'bith_date' => $date_default,
                    'sex' => 'male',
                    'citizenship' => 1,
                    'passport' => '',
                    'date_of_issue' => $date_default,
                    'date_expires' => $date_default,
                );

                $this->model_people->addPeople($data);
                
                return true;
            }

        } else {
            return false;
        }
        
    }
    
    public function getOrdersAction()
    {

        $page = 0;
        $pageSize = 10;
        $data = array();

        if (!isset($this->params['status'])) {
            $this->params['status'] = '!=delete';
        }

        if (isset($this->params['page']) && (int)$this->params['page'] > 0) {
            $page = (int)$this->params['page'] - 1;
        }

        if (isset($this->params['pageSize']) && (int)$this->params['pageSize'] > 0 && (int)$this->params['pageSize'] < 100) {
            $pageSize = (int)$this->params['pageSize'];
        }

        //filter WHERE
        $this->params['where'] = (array)$this->params['where'];

        if (!isset($this->params['where']['status'])) $this->params['where']['status'] = '!=delete';

        foreach ($this->params['where'] as $key => $value) {

            if ($key == 'duration') {
                $data['date_start'] = $value->date_start;
                $data['date_finish'] = $value->date_finish;
            } else {
                $data[$key] = $value;
            }

        }

        if (in_array("1", $this->params['group'])) {

            if (count($this->params['where']) <= 0) {
                $data = array(
                    'status' => $this->params['status']
                );
            }

        } else {

            if (count($this->params['where']) > 0) {
                $data['manager_id'] = $this->params['manager_id'];
            } else {
                $data = array(
                    'status' => $this->params['status'],
                    'manager_id' => $this->params['manager_id']
                );
            }

        }

        $result = $this->model_order->getListOrderBy($data, array(), $page, $pageSize);


        return $result;

    }
    
    public function getOrderByBitrixOrderIDAction(){
        
        if (isset($this->params['order_bitrix_id'])){
            
            $orderId = $this->model_order->getOrderIDByBitrixOrderID($this->params['order_bitrix_id']);
            
            $this->params['orderId'] = $orderId;
            
            return $this->getOrderDataAction();
            
        }
        
    }

    public function getOrderDataAction()
    {

        if (isset($this->params['orderId'])) {
            
            $this->updateTotalOrder($this->params['orderId']);
            
            $orderId = (int)$this->params['orderId'];
            $result = $this->model_order->getOrder($orderId);
            $result['people'] = $this->model_people->getPeopleByOrderId($orderId);

            if (count($result['people']) > 0){
                foreach ($result['people'] as $key => $value){
                    $servicesOrder = $this->model_services->getServices($orderId, $key);
                    $value['services'] = $servicesOrder;
                    $result['people'][$key] = $value;
                }
            }

            $peopleGroup = array();

            $groups = $this->model_people->getGroupList();

            foreach ($groups as $group) {
                $peopleGroup[] = array(
                    'value' => $group['people_group_id'],
                    'label' => $group['name']
                );
            }

            $result['people_group'] = $peopleGroup;

            //BitrixTour
            if (count($result['days']) > 0) {
                $bitrixTour = $this->model_bitrix->getTourByID($result['tour_bitrix_id'], $result['days']);
            } else {
                $bitrixTour = $this->model_bitrix->getTourByID($result['tour_bitrix_id']);
            }


            $reserveHotel = $this->model_hotel->getReserveByDate($result['date_start'], $result['date_finish']);

            
            if (count($reserveHotel) > 0 && (is_array($reserveHotel) || is_object($reserveHotel))) {
                
                foreach ($reserveHotel as $key => $reserve) {
                    
                    foreach ($reserve as $i => $item) {

                        $reserveHotel[$key][$i]['rooms'] = $this->model_bitrix->getRoomByID($item['room_id']);

                    }

                }
            }



            $orderTransferReserve = $this->model_transport->getReserveTransportByOrderID($orderId);
            $listPlaceType = $this->model_transport->getListPlaceType();
            $transportType = $this->model_transport->getListTypeTransport();
            $servicesList = $this->model_bitrix->getServicesList();

            $result['transfer'] = $orderTransferReserve;

            $result['tour_info'] = $bitrixTour;
            $result['tour_info']['transport_type'] = $transportType;
            $result['tour_info']['list_place_type'] = $listPlaceType;
            $result['tour_info']['reserve_hotel'] = $reserveHotel;
            $result['tour_info']['services_list'] = $servicesList;

            return $result;

        } else {
            return false;
        }

    }

    public function updateOrderAction()
    {

        if (isset($this->params['order_id'])) {

            $data = array();

            if (isset($this->params['peoples'])) {

                foreach ($this->params['peoples'] as $id => $people) {

                    $people = (array)$people;

                    $this->model_people->updatePeople($id, $people);

                }

                unset($this->params['peoples']);

            }

            foreach ($this->params as $key => $param) {

                if ($key !== 'order_id') {

                    $data[$key] = $param;

                    $this->model_order->orderUpdate($this->params['order_id'], $data);

                }

            }

            $this->params['orderId'] = $this->params['order_id'];

            return $this->getOrderDataAction();

        }

    }

    public function addDayAction()
    {

        if (isset($this->params['order_id'])) {

            $number_day = $this->params['last_number'] + 1;

            $this->params['orderId'] = $this->params['order_id'];

            $data['days'][] = array(
                'date_start' => $this->params['date_start'],
                'date_finish' => $this->params['date_finish'],
                'city_id' => $this->params['city'],
                'number' => $this->params['diff'] > 1 ? ($number_day) . '-' . ($number_day + $this->params['diff'] - 1) : ($number_day)
            );

            $this->model_order->addOrderDay($this->params['order_id'], $data);

            return $this->getOrderDataAction();

        } else {

            return false;

        }

    }

    public function deleteDayAction()
    {

        if (isset($this->params['order_day_id'])) {

            $this->model_bitrix->deleteProductBasketToDay($this->params['order_day_id'], array('all' => true));
            $this->model_order->removeOrderDay($this->params['order_day_id']);

            return $this->getOrderDataAction();

        } else {

            return false;

        }

    }

    public function getInfoAction()
    {

        $output = array();

        $output['tours'] = $this->model_bitrix->getTours();
        $output['city'] = $this->model_bitrix->getCities();

        if (isset($this->params['manager_id']) && !in_array("1", $this->params['group'])) {
            $output['count_orders'] = $this->model_order->getCountOrders($this->params['manager_id']);
        } else {
            $output['count_orders'] = $this->model_order->getCountOrders();
        }


        return $output;

    }
    
    public function updateTotalOrder($order_id){
        
        $price = $this->model_bitrix->getPriceOrder($order_id);
        
        $data = array(
            'total' => $price
        );
        
        $this->model_order->orderUpdate($order_id, $data);
        
        return true;
        
    }
    
    public function updateStatusOrderAction(){
        
        if (isset($this->params['order_id']) && isset($this->params['status'])){
            
            $order_id = $this->model_order->getOrderIDByBitrixOrderID((int)$this->params['order_id']);
            $status = '';
            
            switch ($this->params['status']){
                case 'N':
                    $status = 'new';
                    break;
                case 'NW':
                    $status = 'register';
                    break;
                case 'CN':
                    $status = 'canceled';
                    break;
                case 'PY':
                    $status = 'payed';
                    break;
                case 'F':
                    $status = 'finish';
                    break;
                default: 
                    $status = 'no-status';
                
            }

            $data = array(
                'status' => $status
            );

            $this->model_order->orderUpdate($order_id, $data);

            return true;


        }else{
            return false;
        }
        
        
    }
    
    public function deleteOrderAction()
    {

        if (isset($this->params['orderId'])) {

            $bitrix_id = $this->model_order->getBitrixOrderIDByOrderID($this->params['orderId']);

            if ($bitrix_id) {
                $this->model_bitrix->canсelOrder($bitrix_id, "DL");
                $this->model_order->deleteOrder($this->params['orderId']);
            }

            return $this->getOrdersAction();

        } else {

            return false;

        }

    }
}