<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class People{

    private $params;
    private $model_user;

    public function __construct($params)
    {
        $this->params = $params;
        $this->model_user = new model_People();
    }

    public function addPeopleAction(){

        if (count($this->params) > 0){

            $data = array();

            if (isset($this->params['name'])){
                 $data['name'] = $this->params['name'];
            }else{
                $data['name'] = "";
            }

            if (isset($this->params['email']) && filter_var($this->params['email'], FILTER_VALIDATE_EMAIL)){
                $data['email'] = $this->params['email'];
            }else{
                return false;
            }

            if (isset($this->params['phone'])){
                $data['phone'] = $this->params['phone'];
            }else{
                $data['phone'] = "";
            }

            if (isset($this->params['people_group_id']) && !empty($this->params['people_group_id']) ){
                $data['group_id'] = $this->params['people_group_id'];
            }else{
                return false;
            }

            if (isset($this->params['comment'])){
                $data['comment'] = $this->params['comment'];
            }else{
                $data['comment'] = "";
            }

            if (isset($this->params['order_id'])){
                $data['order_id'] = $this->params['order_id'];
            }else{
                $data['order_id'] = 0;
            }

            if (isset($this->params['bith_date'])){
                $date = strtotime($this->params['bith_date']);
                $data['bith_date'] = date('Y-m-d', $date);
            }else{
                $data['bith_date'] = date('Y-m-d');
            }

            if (isset($this->params['sex'])){
                $data['sex'] = $this->params['sex'];
            }else{
                $data['sex'] = '';
            }

            if (isset($this->params['citizenship'])){
                $data['citizenship'] = $this->params['citizenship'];
            }else{
                $data['citizenship'] = '';
            }

            if (isset($this->params['passport'])){
                $data['passport'] = $this->params['passport'];
            }else{
                $data['passport'] = '';
            }

            if (isset($this->params['date_of_issue'])){
                $date = strtotime($this->params['date_of_issue']);
                $data['date_of_issue'] = date('Y-m-d', $date);
            }else{
                $data['date_of_issue'] = '';
            }

            if (isset($this->params['date_expires'])){
                $date = strtotime($this->params['date_expires']);
                $data['date_expires'] = date('Y-m-d', $date);
            }else{
                $data['date_expires'] = '';
            }

        }


        return $this->model_user->addPeople($data);

    }

    public function updatePeopleAction(){

        if (!isset($this->params['people_id'])){
            return false;
        }
        
        $peopleId = $this->params['people_id'];
        unset($this->params['people_id']);
        
        $data = $this->params;
        
        return $this->model_user->updatePeople($peopleId, $data);

    }


    public function deletePeopleToOrderAction(){

        if (count($this->params) > 0){

            if (isset($this->params['peopleId']) && isset($this->params['orderId'])){

                $this->model_user->deletePeopleToOrder($this->params['peopleId'], $this->params['orderId']);

                return true;

            }

        }else{

            return false;

        }

    }

}