<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class Transfer
{
    private $params;
    public $model_order;
    public $model_people;
    public $model_bitrix;
    public $model_hotel;

    public function __construct($params)
    {
        $this->params = $params;
        $this->model_transport = new model_ReserveTransport();
        $this->model_bitrix = new model_Bitrix();
    }

    public function getTransfersListAction(){

        $order_by = array();
        $page = 0;
        $pageSize = 10;

        if (!isset($this->params['status'])){
            $this->params['status'] = '!=delete';
        }

        if (isset($this->params['page'])){
            $page = (int)$this->params['page'] - 1;
        }

        if (isset($this->params['pageSize'])){
            $pageSize = $this->params['pageSize'];
        }

        $data = array(
            'status' => $this->params['status']
        );
        
        $result = $this->model_transport->getReserveOrderBy($data, $order_by, $page, $pageSize);
        //Все резервы
//        $result = $this->model_transport->getReserveTransportList();
        
        $output = array();

        foreach ($result as $item){

            $type = $this->model_transport->getTypeTransportByID($item['type']);
            $item['type'] = $type[0];

            $output[] = $item;

        }

        return $output;

    }

    public function getTransferByCityIDAction(){

        $output = array();

        if (isset($this->params['city_start']) && isset($this->params['city_finish']) && isset($this->params['date_reserve'])){

            if ($this->params['city_finish']){
                $result = $this->model_transport->getTransportByCityAndDate($this->params['city_start'], $this->params['city_finish'], $this->params['date_reserve']);

                $output['reserve_transport'] = $result;
                $output['transport_types'] = array();
                $index = 0;

                if (count($result) > 0){

                    foreach ($result as $item) {

                        $type = $this->model_transport->getTypeTransportByID($item['type']);

                        if (count($type) > 0){
                            $output['reserve_transport'][$index]['type_name'] = $type[0]['transport_type_name'];

                            if (!in_array($type[0]['transport_type_name'], $output['transport_types'])){
                                $output['transport_types'][] = $type[0]['transport_type_name'];
                            }

                        }else{
                            $output['reserve_transport'][$index]['type_name'] = 'Неизвестно';
                        }

                        $place = $this->model_transport->getPlaceByID($item['reserve_transport_id']);

                        if (count($place) > 0){

                            foreach ($place as $val){

                                $reserve = $this->model_transport->checkReservePlace($val['reserve_place_id'], $this->params['date_reserve']);
                                $type = $this->model_transport->getTypePlaceByID($val['reserve_type_place_id']);

                                if (count($type) > 0){
                                    $val['type_place_name'] = $type[0]['place_name'];
                                }

                                if ($reserve){
                                    $val['reserved'] = true;
                                }else{
                                    $val['reserved'] = false;
                                }

                                $output['reserve_transport'][$index]['place'][] = $val;

                            }

                        }else{
                            $output['reserve_transport'][$index]['place'] = false;
                        }


                        $index++;

                    }

                }

                return $output;

            }else{
                return false;
            }

        }

    }

    public function addReserveTransferAction(){

        if (isset($this->params['is_reserve'])){

            if ($this->params['is_reserve']){

                $data = array(
                    'order_id' => $this->params['order_id'],
                    'people_order_id' => $this->params['people'],
                    'reserve_place_id' => $this->params['reserve_place_id'],
                    'is_reserve' => 1,
                    'city_start' => $this->params['city_start'],
                    'city_finish' => $this->params['city_finish'],
                    'transport_type_id' => $this->params['reserve_transport_id'],
                    'type_place_id' => $this->params['reserve_type_place_id'],
                    'number_place' => $this->params['number_place'],
                    'date_reserve' => $this->params['date_reserve'],
                    'comment' => $this->params['comments'],
                    'price' => $this->params['price']
                );

            }else{

                $data = array(
                    'order_id' => $this->params['order_id'],
                    'people_order_id' => $this->params['people'],
                    'reserve_place_id' => $this->params['reserve_place_id'],
                    'is_reserve' => 0,
                    'city_start' => $this->params['city_start'],
                    'city_finish' => $this->params['city_finish'],
                    'transport_type_id' => $this->params['type_transport_id'],
                    'type_place_id' => $this->params['type_place_id'],
                    'number_place' => $this->params['number_place'],
                    'date_reserve' => $this->params['date_reserve'],
                    'comment' => $this->params['comments'],
                    'price' => $this->params['price']
                );

            }

            $result = $this->model_transport->addReserveTransferPeople($data);

            return $result;

        }else{
            return false;
        }

    }

    public function addReserveAction(){

        if (isset($this->params['transport_name'])){

            $data = array(
                'transport_name' => $this->params['transport_name'],
                'date_start' => $this->params['date_start'],
                'type' => $this->params['type'],
                'date_finish' => $this->params['date_finish'],
                'city_finish' => $this->params['city_finish'],
                'city_start' => $this->params['city_start'],
            );

            $offer_id = $this->model_transport->addTransport($data);

            if (count($this->params['place']) > 0 && $offer_id){

                foreach ($this->params['place'] as $place){

                    $data = array(
                        'reserve_transport_id' => $offer_id,
                        'reserve_type_place_id' => $place->reserve_type_place_id,
                        'number_place' => $place->number_place,
                        'status' => $place->status
                    );

                    $this->model_transport->addReservePlace($data);

                }

            }

            return $this->getTransfersListAction();

        }

    }

    public function getTypeTransportAction(){

       $result = array();

       $result['type_transport'] = $this->model_transport->getListTypeTransport();
       $result['city'] = $this->model_bitrix->getCities();
       $result['type_place'] = $this->model_transport->getListPlaceType();
       $result['count_reserve'] = $this->model_transport->getCountReserve();


       if ($result){
          return $result;
       }else{
          return false;
       }

    }

    public function deleteReserveTransferAction(){

        if (isset($this->params['reserve_transport_people_id'])){
            $result = $this->model_transport->deleteReserveOrder($this->params['reserve_transport_people_id']);

            return $result;

        }else{
            return false;
        }

    }

    public function getTransferAction(){

        if (isset($this->params['reserve_transport_id'])){

            $return = array();

            $reserve_id = $this->params['reserve_transport_id'];

            $return['reserve'] = $this->model_transport->getTransportByID($reserve_id);

            $return['reserve']['reserve_place'] = $this->model_transport->getPlaceByTransportId($return['reserve']['reserve_transport_id']);

            $return['city'] = $this->model_bitrix->getCities();

            $return['type_place'] = $this->model_transport->getListPlaceType();

            $return['type_transport'] = $this->model_transport->getListTypeTransport();

            return $return;

        }else{

            return false;

        }

    }

    public function updateReserveTransferAction(){

        $params = $this->params;
        $dataUpdate = array();

        foreach ($params as $key => $value){

            if ($value !== false && $key != 'place'){
                $dataUpdate[$key] = $value;
            }

        }

        if ($params['place'] && count($params['place']) > 0){

            foreach ($params['place'] as $param) {

                if ($param->status == 'delete'){

                    $data = array(
                      'number_place' =>  $param->number_place,
                      'reserve_transport_id' => $param->reserve_transport_id
                    );


                    $this->model_transport->deletePlaceReserve($data);

                }else{

                    $data = array(
                        'number_place' =>  $param->number_place,
                        'reserve_transport_id' => $param->reserve_transport_id,
                        'reserve_type_place_id' => 7,
                        'status' => 'active'
                    );

                    $this->model_transport->addReservePlace($data);

                }

            }

        }

        $result = $this->model_transport->updateTransport($params['reserve_transport_id'], $dataUpdate);

        if ($result){
            return $this->getTransferAction();
        }else{
            return false;
        }

    }

    public function deleteReserveAction(){

        if (isset($this->params['id'])){

            $deleteTransport = $this->model_transport->deleteReserveTransport($this->params['id']);
//            $deletePlace = $this->model_transport->deletePlaceReserve($data);

            if ($deleteTransport){

                return $this->getTransfersListAction();

            }else{

                return false;

            }


        }else{

            return false;

        }

    }

}