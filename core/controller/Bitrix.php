<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

CModule::IncludeModule('main');
CModule::IncludeModule('iblock');
global $USER;
session_start();
?>

<?php

class Bitrix{

    private $user;
    private $params;
    private $model_bitrix;

    public function __construct($params)
    {
        global $USER;
        global $APPLICATION;

        $this->params = $params;
        $this->user = $USER;
        $this->model_bitrix = new model_Bitrix();
        $this->application = $APPLICATION;
    }

    public function addOrderAction(){

        if (!isset($this->params['user_id']) || !isset($this->params['basket'])){
            return false;
        }
        
       
        $order_id = $this->model_bitrix->addOrder($this->params['user_id'], $this->params['basket']);

        return $order_id;
        
    }

    public function addServicesToOrderAction(){

        if (!isset($this->params['services']) || !isset($this->params['order_id'])){
            return false;
        }

        $data = array(
            'services' => $this->params['services']
        );

        $this->model_bitrix->addServicesToOrder($data, $this->params['order_id']);

    }

    public function deleteServiceToOrderAction(){

        if (!isset($this->params['order_id'])){
            return false;
        }

        $this->model_bitrix->deleteServiceToOrder($this->params['order_id']);

    }

    public function loginAction(){

        $output = array();

        $this->user->Logout();

        if (isset($this->params['login']) && isset($this->params['password'])){

                $this->user->Login($this->params['login'], $this->params['password'], "Y");

                if ($this->user->IsAuthorized()){

                    $group = $this->user->GetUserGroup($this->user->GetId());

                    if (in_array("1", $group) || in_array("6", $group)){

                        $this->user->SavePasswordHash();
                        $hash = $this->user->GetParam("PASSWORD_HASH");

                        $output = array(
                            'status' => 'success',
                            'manager_id' => $this->user->GetId(),
                            'user_login' => $this->user->GetLogin(),
                            'group' => $this->user->GetUserGroup($this->user->GetId()),
                            'user_name' => $this->user->GetParam("NAME"),
                            'login_hash' => $_SESSION['SESS_AUTH']['SESSION_HASH'],
                            'redirect' => $this->params['redirect']
                        );

                    } else {

                        $output = array(
                            'status' => 'error',
                            'errorText' => 'У вас нет доступа к этому разделу'
                        );

                    }

                }else{

                    $output = array(
                        'status' => 'error',
                        'errorText' => 'Проверьте логин и пароль'
                    );

                }

                return $output;
        }else{

           $output = array(
              'status' => 'error',
              'errorText' => 'Проверьте логин и пароль'
           );

           return $output;

        }

    }

    public function isAuthAction(){

        if (isset($this->params['login_hash']) && isset($this->params['login'])){

            $_SESSION["SESS_AUTH"]["SESSION_HASH"] = $this->params['login_hash'];

                if ($this->user->LoginByHash($this->params['login'], $this->params['login_hash'])){

                    $group = $this->user->GetUserGroup($this->user->GetId());

                    if (in_array("1", $group) || in_array("6", $group)){

                        $output = array(
                            'status' => 'success',
                            'manager_id' => $this->user->GetId(),
                            'user_login' => $this->user->GetLogin(),
                            'group' => $this->user->GetUserGroup($this->user->GetId()),
                            'user_name' => $this->user->GetParam("NAME"),
                            'login_hash' => $this->params['login_hash']
                        );

                    }else{

                        unset($_SESSION["SESS_AUTH"]["SESSION_HASH"]);

                        $output = array(
                            'status' => 'error'
                        );

                    }

                }

        }else{

            unset($_SESSION["SESS_AUTH"]["SESSION_HASH"]);

            $output = array(
                'status' => 'error'
            );

        }

        return $output;

    }

    public function logoutAction(){

        $output = array(
           'status' => 'success',
        );

        $this->user->Logout();

        unset($_SESSION["SESS_AUTH"]["SESSION_HASH"]);

        return $output;

    }

}


