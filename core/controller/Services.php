<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class Services
{
    private $params;
    private $model_services;
    private $model_bitrix;

    public function __construct($params)
    {
        $this->params = $params;
        $this->model_services = new model_Services();
        $this->model_bitrix = new model_Bitrix();
    }
    
    public function addServiceAction(){
        
       if (!isset($this->params['order_id']) || !isset($this->params['service_id']) || !isset($this->params['people'])){
           return false;
       }
       
       $this->model_bitrix->addProductInOrder($this->params['order_id'], $this->params['service_id'], 1);
       
       return $this->model_services->addServices($this->params);
       
    }
    
    public function deleteServiceAction(){
        
       if (!isset($this->params['order_id']) || !isset($this->params['service_id']) || !isset($this->params['people'])){
           return false;
       }

        $this->model_bitrix->deleteProductInOrder($this->params['order_id'], $this->params['service_id'], 1);
       
       return $this->model_services->deleteServices($this->params['order_id'], $this->params['service_id'], $this->params['people']);
       
    }
    
}