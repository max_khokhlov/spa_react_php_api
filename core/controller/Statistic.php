<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

class Statistic{

    private $params;
    public $model_order;
    public $model_people;
    public $model_bitrix;
    public $model_hotel;
    public $model_statistic;

    public function __construct($params)
    {
        $this->params = $params;
        $this->model_order = new model_Order();
        $this->model_people = new model_People();
        $this->model_bitrix = new model_Bitrix();
        $this->model_hotel = new model_Hotel();
        $this->model_statistic = new model_Statistic();
    }


    public function getTablesAction(){

        if (isset($this->params['status'])){

            $tables = $this->model_statistic->getTables();

            $output = array();
            $output['table_list'] = $tables;

            foreach ($tables as $table){

                $columns = $this->model_statistic->getColumnTables($table);
                $output['table'][$table] = $columns;

            }

            return $output;

        }

    }

    public function getDataAction(){

        if (isset($this->params['table_name'])){

            $table = $this->params['table_name'];

            if ($this->params['params'] === false){

                $optionFilter = $this->model_statistic->getOptionDistinct($table);
                $data = $this->model_statistic->getTable($table);
                $type_column = $this->model_statistic->getTypeColumnTable($table);

                $output = array(
                    'data' => $data['data'],
                    'communication' => $data['communication'],
                    'field_bitrix' => $data['field_bitrix'],
                    'option_filter' => $optionFilter,
                    'type_column' => $type_column
                );

                return $output;

            }else{

                $params = $this->params['params'];
                $optionFilter = $this->model_statistic->getOptionDistinct($table);
                $data = $this->model_statistic->getTable($table, $params);
                $type_column = $this->model_statistic->getTypeColumnTable($table);

                $output = array(
                    'data' => $data['data'],
                    'communication' => $data['communication'],
                    'field_bitrix' => $data['field_bitrix'],
                    'option_filter' => $optionFilter,
                    'type_column' => $type_column
                );

                return $output;

            }

        }else{

            return false;

        }

    }


    public function getFilterJoinAction(){

        if (isset($this->params['table_join']) && isset($this->params['table']) && isset($this->params['params'])){

            $table_join = $this->params['table_join'];
            $table = $this->params['table'];
            $params = $this->params['params'];
            $key = $this->params['key'];

            $data = $this->model_statistic->getTableJoin($table_join, $table, $params, $key);

            $output = array(
                'data' => $data['data'],
                'communication' => $data['communication'],
                'field_bitrix' => $data['field_bitrix'],
                'option_filter' => $data['fields'],
                'type_column' => array()
            );

            return $output;

        }

    }

}