<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/manager/core/config.php';

class Manager
{

    private $params;
    public $model_order;
    public $model_people;
    public $model_bitrix;
    public $model_hotel;

    public function __construct($params)
    {
        $this->params = $params;
        $this->model_order = new model_Order();
        $this->model_people = new model_People();
        $this->model_bitrix = new model_Bitrix();
        $this->model_hotel = new model_Hotel();
        $this->model_transport = new model_ReserveTransport();
    }

    public function getManagerListAction(){

        return $this->model_bitrix->getManagerList();

    }

}