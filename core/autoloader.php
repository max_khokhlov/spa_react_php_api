<?php

if(!function_exists('autoload')) {
    function autoload($class)
    {


        $baseDir = __DIR__ . '/';
        $path = $baseDir . str_replace('_', '/', $class) . '.php';

        if (file_exists($path)) {
            require_once($path);
            return true;
        }

        return false;
    }
}

spl_autoload_register('autoload');