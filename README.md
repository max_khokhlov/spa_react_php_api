# SPA приложение на React (Redux) + PHP + REST API

![screenshot](./screenshot.png)

##[Back-end](https://bitbucket.org/max_khokhlov/spa_react_php_api/src/c5286a1f0a17/core/?at=master)  

**model** -  Модель БД

**controller** - Контроллер для работы с запросами 

##[API](https://bitbucket.org/max_khokhlov/spa_react_php_api/src/c5286a1f0a17d7fb6cd260b69392da1949ebb6eb/api/v1/?at=master)

Все запросы к контроллерам идут через REST API

##[Front-end](https://bitbucket.org/max_khokhlov/spa_react_php_api/src/c5286a1f0a17d7fb6cd260b69392da1949ebb6eb/src/?at=master)

Стандартная схема работы с Redux

**Action / Reducer / Store / State**

Использовал как UI фреймворк [AntDesign](https://ant.design/)
