<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php";
require_once $_SERVER['DOCUMENT_ROOT'].'/manager/core/config.php';

CModule::IncludeModule('main');
CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

$applications = array(
    'APP001' => API_KEYS,
);

try{

    $request = $_REQUEST['fields'];
    $app_id = $_REQUEST['app_id'];
    
//    if( !isset($applications[$app_id]) ) {
//        throw new Exception('Application does not exist!');
//    }

    $params = json_decode($request);

    $params = (array) $params;
    
    if ($_GET){
        foreach ($_GET as $key => $item) {
            $controller = ucfirst(strtolower($key));
            $action = strtolower($item).'Action';
            break;
       } 
    }

    if( !$controller || empty($controller) || !$action || empty($action) ) {
        throw new Exception('Request is not valid');
    }

    if(file_exists($_SERVER['DOCUMENT_ROOT']."/manager/core/controller/{$controller}.php") ) {
        require_once $_SERVER['DOCUMENT_ROOT']."/manager/core/controller/{$controller}.php";
    } else {
        throw new Exception('Controller is invalid.');
    }

//    $params = array(
//      'login' => 'admin',
//      'password' => 355113
//    );

    $controller = new $controller($params);

    if( method_exists($controller, $action) === false ) {
        throw new Exception('Action is invalid.');
    }else{
        $result = array();
        $result['success'] = true;
        $result['result'] = $controller->$action();
    }

} catch (Exception $e){
    $result = array();
    $result['success'] = false;
    $result['error'] = $e->getMessage();
}

echo json_encode($result);
exit();