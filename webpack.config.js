var path = require('path')
var webpack = require('webpack')
const fs  = require('fs');

const lessToJs = require('less-vars-to-js');
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, './src/less/ant-stylistic.less'), 'utf8'));
process.env.NODE_ENV = 'production';
// themeVariables["@icon-url"] = "'//localhost:8080/fonts/iconfont'";

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  node: {
    fs: "empty"
  },
  devServer: {
     index: './index.php'
  },
  entry: [
    'webpack-hot-middleware/client',
    'babel-polyfill',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.EnvironmentPlugin(['NODE_ENV', 'USER'])
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ["es2015", "stage-0", "react"],
            plugins: ['transform-runtime',"react-hot-loader/babel", ['import', { libraryName: "antd", style: true }]]
          }
        }
      },
      {
      test: /\.less$/,
      use: [
          {loader: "style-loader"},
          {loader: "css-loader"},
          {loader: "less-loader",
            options: {
              javascriptEnabled: true,
              modifyVars: themeVariables
            }
          }
        ]
      },
      {
      test: /\.scss$/,
      use: [
          {loader: "style-loader"},
          {loader: "css-loader"},
          {loader: "sass-loader"}
        ]
      },
      {
        test:/\.css$/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader"}
        ]
      }
    ]
  },
  devServer: { historyApiFallback: true }
}

