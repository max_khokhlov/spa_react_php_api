var gulp = require('gulp'),
    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    postcss    = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync'),
    pump = require('pump'),
    imagemin = require('imagemin'),
    concat = require('gulp-concat'),
    pngquant = require('imagemin-pngquant'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    reload = browserSync.reload;



gulp.task('img', function () {
    imagemin(
        ['./_build/img/**/*'],
        './img/',
        {
            plugins: [
                pngquant(),
                imageminJpegRecompress({
                    target:1,
                    max:80
                })
            ]
        }
    );
});

gulp.task('browserSync', function() {
    browserSync.init({
        proxy: 'localhost'
    });
});

gulp.task('reload', function(){
    gulp.src('./*html')
        .pipe(reload({stream: true}));
});

gulp.task('sass', function(){
    gulp.src('./_build/sass/*scss')
        .pipe(sass())
        .pipe(postcss([
            autoprefixer({
                browsers: ['last 100 versions'],
                cascade: false
            })
        ]))
        .pipe(csso())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./src/css'))
        .pipe(reload({stream: true}));
});

gulp.task('watch', function(){
    gulp.watch('./_build/sass/*scss', ['sass']);
    gulp.watch('./_build/img/*', ['img']);
    // gulp.watch('./*html', ['reload']);
});

//gulp.task('build', ['browserSync', 'watch']);
gulp.task('build', ['watch']);
gulp.task('default', ['sass']);
